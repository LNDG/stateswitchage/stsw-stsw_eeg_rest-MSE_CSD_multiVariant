Scripts for the analysis of modified Multi-scale Sample Entropy [MSE] in resting state data from younger and older adults
-
**Description of scripts**
-
A:  
- prepare resting state data for input to MSE function

B:   
- run MSE function (set to tardis cluster version)

C:
- merge data from all younger and older adults
- perform spline interpolation for high-pass implementation

D:
- visualize differences between age MSE spectra (not reported)

E:
- calculate cluster-based permutation analysis (CBPA) for age contrast
- E2: plot results from CBPA
- E3: compare effect sizes for 'Original' effect clusters with different implementations

F:
- visualize PSD slopes and multi-scale sample entropy spectra
- Figure 6A
- [requires empirical/fft]


G:
- plot summary figure of CBPA for MSE, PSD and PSD slopes
- Figure 6B-D
- [requires empirical/fft]

H:
- plot entropy and similarity criterion traces by age and age contrast results
- Figure 7

I:
- highlight association between coarse-scale entropy and high-frequency power
- Figure 8
- [requires empirical/fft]

J:
- highlight association between fine-scale entropy and PSD slopes
- Figure 9
- [requires empirical/fft]

K:
- highlight association between bandpass MSE and rhythmic events
- Figure 10
- [requires empirical/ebosc_nodur]

L:
- scale-wise relation between mMSE (lowpass/band-pass) & spectral event rate
- [requires empirical/ebosc_nodur]

Z1:
- extracts schematic spectra for Figure 2

Z2:
- extracts schematic spectra for Figure 3

Z3:
- extracts schematic spectra for Figure 11
