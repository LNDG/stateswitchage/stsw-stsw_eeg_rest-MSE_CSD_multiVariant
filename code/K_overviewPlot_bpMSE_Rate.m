% create overview plot of bandpass MSE-rhythm results

% requires R2018a for local minimum detection

% add convertPtoExponential
addpath('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/A_MSE_CSD_multiVariant/T_tools/')

%% plot bandpassed versions in overview figure: alpha, beta

  h = figure('units','normalized','position',[.1 .1 .8 .8]);
  set(gcf,'renderer','Painters')
        subplot(3,4,3); cla; hold on;
        load('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/D_eBOSC_noDur/B_data/C_RhythmCharacteristics/D_TimeDomainRhythmicity_Alpha_lowpass.mat')
            for indID = 1:32
               curData = cat(1, TimeEps{indID,:});
               ampData = curData(:,4);
               curDataPlot = curData(:,3);
               curData = curData(:,1);
               tempSeries = NaN(size(curData,1),3000);
               for indEp = 1:size(curData,1)
                     % find minima clostest to TFR maximum 
                    [~, sortIndTFR] = max(ampData{indEp});
                    TF = islocalmin(curData{indEp});
                    TF = find(TF);
                    [~, minInd_tmp] = min(abs(sortIndTFR-TF));
                    sortInd = TF(minInd_tmp);
                    tempSeries(indEp, 1500-sortInd+1:1500+numel(curData{indEp})-sortInd) = curDataPlot{indEp};
                    tempSeriesDur(indEp) = numel(curData{indEp});
                    tempSeriesDurPre(indEp) = 500+sortInd;
                    tempSeriesDurPost(indEp) = numel(curData{indEp})-500-sortInd;
               end
               AvgSeries_alpha(indID,:) = nanmedian(tempSeries,1);
               time = -1500*1/250:1/250:1500*1/250;
               plot(time(2:end)*1000,nanmean(tempSeries,1), 'Color', [.8 .8 .8], 'LineWidth', 1)
               xlabel('Time in ms'); xlim([-800, 800]); ylabel('Amplitude (z-scored)')
            end
            hold on; plot(time(2:end)*1000,nanmean(AvgSeries_alpha,1), 'Color', 'k', 'LineWidth', 4)
            title('Alpha events (8-12 Hz; POz)')
            ylim([-2.5 2.5])
       subplot(3,4,4); cla; hold on;
        load('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/D_eBOSC_noDur/B_data/C_RhythmCharacteristics/D_TimeDomainRhythmicity_Beta_lowpass.mat')
            for indID = 1:32
                try
                   curData = cat(1, TimeEps{indID,2});
                   ampData = curData(:,4);
                   curDataPlot = curData(:,3);
                   curData = curData(:,1);
                   tempSeries = NaN(size(curData,1),3000);
                   for indEp = 1:size(curData,1)
                         % find minima clostest to TFR maximum 
                        [~, sortIndTFR] = max(ampData{indEp});
                        TF = islocalmin(curData{indEp});
                        TF = find(TF);
                        [~, minInd_tmp] = min(abs(sortIndTFR-TF));
                        sortInd = TF(minInd_tmp);
                        tempSeries(indEp, 1500-sortInd+1:1500+numel(curData{indEp})-sortInd) = curDataPlot{indEp};
                        tempSeriesDur(indEp) = numel(curData{indEp});
                        tempSeriesDurPre(indEp) = 500+sortInd;
                        tempSeriesDurPost(indEp) = numel(curData{indEp})-500-sortInd;
                   end
                   time = -1500*1/250:1/250:1500*1/250;
                   plot(time(2:end)*1000,nanmean(tempSeries,1), 'Color', [.8 .8 .8], 'LineWidth', 1)
                   AvgSeries_beta(indID,:) = nanmedian(tempSeries,1);
                   xlabel('Time in ms'); xlim([-800, 800]); ylabel('Amplitude (z-scored)')
                catch
                    continue;
                end
            end
            hold on; plot(time(2:end)*1000,nanmean(AvgSeries_beta,1), 'Color', 'k', 'LineWidth', 4)
            title('Beta events (14-20 Hz; Cz)')
            ylim([-1.75 1.75]) 
       set(findall(gcf,'-property','FontSize'),'FontSize',16)

   %% add correlations between MSE and PSD
   
   % load MSE

    load('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/A_MSE_CSD_multiVariant/B_data/E_mseMerged.mat', 'mseMerged', 'IDs')

    % load PSD

    load('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/B_FFT/B_data/B_FFT_grandavg_individual.mat')

    commonYA = info.IDs_all(ismember(info.IDs_all, IDs{1}));
    commonOA = info.IDs_all(ismember(info.IDs_all, IDs{2}));

    idx_YA_fft = ismember(info.IDs_all, commonYA);
    idx_OA_fft = ismember(info.IDs_all, commonOA);
    idx_YA_mse = ismember(IDs{1}, commonYA);
    idx_OA_mse = ismember(IDs{2}, commonOA);

    % load stats cluster

    pn.root = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/A_MSE_CSD_multiVariant/';
    load([pn.root, 'B_data/E2_CBPA_Age_Condition_v2.mat'], 'stat', 'methodLabels')

    % correlate PSD with MSE

    betaCluster = max(stat{4,5}.mask(:,stat{4,5}.freq>14 & stat{4,5}.freq<20),[],2);
    alphaCluster = max(stat{4,5}.mask(:,stat{4,5}.freq>8 & stat{4,5}.freq<12),[],2);

    BetaPSD_YA = squeeze(nanmean(nanmean(log10(grandavg_EO.powspctrm(idx_YA_fft,betaCluster,grandavg_EC.freq > 14 & grandavg_EO.freq<20)),3),2));
    AlphaPSD_YA = squeeze(nanmean(nanmean(log10(grandavg_EO.powspctrm(idx_YA_fft,alphaCluster,grandavg_EC.freq > 8 & grandavg_EO.freq<12)),3),2));

    BetaMSE_YA = squeeze(nanmean(nanmean(mseMerged{1,2}.MSEbp(:,betaCluster,mseMerged{1,1}.freq> 14 & mseMerged{1,1}.freq< 20),3),2));
    AlphaMSE_YA = squeeze(nanmean(nanmean(mseMerged{1,2}.MSEbp(:,alphaCluster,mseMerged{1,1}.freq> 8 & mseMerged{1,1}.freq< 12),3),2));

    BetaPSD_OA = squeeze(nanmean(nanmean(log10(grandavg_EO.powspctrm(idx_OA_fft,betaCluster,grandavg_EO.freq > 14 & grandavg_EO.freq<20)),3),2));
    AlphaPSD_OA = squeeze(nanmean(nanmean(log10(grandavg_EO.powspctrm(idx_OA_fft,alphaCluster,grandavg_EO.freq > 8 & grandavg_EO.freq<12)),3),2));

    BetaMSE_OA = squeeze(nanmean(nanmean(mseMerged{2,2}.MSEbp(:,betaCluster,mseMerged{1,1}.freq> 14 & mseMerged{1,1}.freq< 20),3),2));
    AlphaMSE_OA = squeeze(nanmean(nanmean(mseMerged{2,2}.MSEbp(:,alphaCluster,mseMerged{1,1}.freq> 8 & mseMerged{1,1}.freq< 12),3),2));

    subplot(3,4,5); cla; hold on;
        x = AlphaPSD_YA; y = AlphaMSE_YA; 
        l1 = scatter(x,y, 50,'filled', 'r'); y1_ls = polyval(polyfit(x,y,1),x); y1_ls = plot(x, y1_ls, 'Color', 'r', 'LineWidth', 2);
        [r1, p1] = corrcoef(x, y); p1 = convertPtoExponential(p1(2));
        x = AlphaPSD_OA; y = AlphaMSE_OA; 
        l3 = scatter(x,y, 50,'filled', 'k'); y3_ls = polyval(polyfit(x,y,1),x); y3_ls = plot(x, y3_ls, 'Color', 'k', 'LineWidth', 2);
        [r3, p3] = corrcoef(x, y); p3 = convertPtoExponential(p3(2));
        legend([y1_ls, y3_ls], {['YA Alpha: r = ', num2str(round(r1(2),2)), ', p = ' p1{1}], ...
            ['OA Alpha: r = ', num2str(round(r3(2),2)), ', p = ' p3{1}]}, 'location', 'NorthWest'); legend('boxoff');
        xlabel('Power (log10)'); ylabel('Entropy');
        ylim([.15 .4])
        title('Parietooccipital Alpha')
    subplot(3,4,6); cla; hold on;
        x = BetaPSD_YA; y = BetaMSE_YA; 
        l2 = scatter(x,y, 50,'filled', 'r'); y2_ls = polyval(polyfit(x,y,1),x); y2_ls = plot(x, y2_ls, 'Color', 'r', 'LineWidth', 2);
        [r2, p2] = corrcoef(x, y); p2 = convertPtoExponential(p2(2));
        x = BetaPSD_OA; y = BetaMSE_OA; 
        l4 = scatter(x,y, 50,'filled', 'k'); y4_ls = polyval(polyfit(x,y,1),x); y4_ls = plot(x, y4_ls, 'Color', 'k', 'LineWidth', 2);
        [r4, p4] = corrcoef(x, y); p4 = convertPtoExponential(p4(2));
        legend([y2_ls, y4_ls], {['YA Beta: r = ', num2str(round(r2(2),2)), ', p = ' p2{1}],...
            ['OA Beta: r = ', num2str(round(r4(2),2)), ', p = ' p4{1}]}, 'location', 'NorthWest'); legend('boxoff');
        xlabel('Power (log10)'); ylabel('Entropy');
        ylim([.25 .4])
        title('Central Beta')

    %% add topographies of MSE and Variance criterion
    
    pn.root = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/A_MSE_CSD_multiVariant/';
    addpath([pn.root, 'T_tools/fieldtrip-20170904']); ft_defaults;
    load([pn.root, 'B_data/E2_CBPA_Age_Condition_v2.mat'], 'stat', 'methodLabels')
    contrastLabels = {'YA: EC minus EO'; 'OA: EC minus EO'; 'EC: OA minus YA'; 'EO: OA minus YA'};

    addpath('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/A_MSE_CSD_multiVariant/T_tools/brewermap')

    cBrew = brewermap(500,'RdBu');
    cBrew = flipud(cBrew);

    cfg = [];
    cfg.layout = 'acticap-64ch-standard2.mat';
    cfg.parameter = 'powspctrm';
    cfg.comment = 'no';
    cfg.style = 'both';
    cfg.colormap = cBrew;
    cfg.zlim = [-5 5];
    cfg.marker = 'off';

    subplot(3,4,1)
        cfg.highlight = 'on';
        cfg.highlightchannel = stat{1,1}.label(max(stat{4,5}.mask(:,15:20),[],2));
        plotData = [];
        plotData.label = stat{1,1}.label; % {1 x N}
        plotData.dimord = 'chan';
        plotData.powspctrm = squeeze(nanmedian(stat{4,5}.stat(:,15:20),2));
        ft_topoplotER(cfg,plotData);
        % cb = colorbar('SouthOutside'); set(get(cb,'title'),'string','t values');
        pval = []; pval = convertPtoExponential(stat{4,5}.posclusters(1).prob);
        title({'Alpha entropy OA minus YA', ['p = ', pval{1}]});
    subplot(3,4,2)
        cfg.highlight = 'on';
        cfg.highlightchannel = stat{1,1}.label(max(stat{4,5}.mask(:,23:27),[],2));
        plotData = [];
        plotData.label = stat{1,1}.label; % {1 x N}
        plotData.dimord = 'chan';
        plotData.powspctrm = squeeze(nanmedian(stat{4,5}.stat(:,23:27),2));
        ft_topoplotER(cfg,plotData);
        % cb = colorbar('SouthOutside'); set(get(cb,'title'),'string','t values');
        title('Beta entropy OA minus YA')
        pval = []; pval = convertPtoExponential(stat{4,5}.negclusters(1).prob);
        title({'Beta entropy OA minus YA', ['p = ', pval{1}]});
    subplot(3,4,9)
        cfg.highlight = 'on';
        cfg.highlightchannel = stat{1,1}.label(max(stat{4,10}.mask(:,15:20),[],2));
        plotData = [];
        plotData.label = stat{1,1}.label; % {1 x N}
        plotData.dimord = 'chan';
        plotData.powspctrm = squeeze(nanmedian(stat{4,10}.stat(:,15:20),2));
        ft_topoplotER(cfg,plotData);
        % cb = colorbar('SouthOutside'); set(get(cb,'title'),'string','t values');
        pval = []; pval = convertPtoExponential(stat{4,10}.negclusters(1).prob);
        title({'Alpha similarity bound OA minus YA', ['p = ', pval{1}]});
    subplot(3,4,10)
        cfg.highlight = 'on';
        cfg.highlightchannel = stat{1,1}.label(max(stat{4,10}.mask(:,23:27),[],2));
        plotData = [];
        plotData.label = stat{1,1}.label; % {1 x N}
        plotData.dimord = 'chan';
        plotData.powspctrm = squeeze(nanmedian(stat{4,10}.stat(:,23:27),2));
        ft_topoplotER(cfg,plotData);
        % cb = colorbar('SouthOutside'); set(get(cb,'title'),'string','t values');
        title('Beta similarity bound OA minus YA')
        pval = []; pval = convertPtoExponential(stat{4,10}.posclusters(1).prob);
        title({'Beta similarity bound OA minus YA', ['p = ', pval{1}]});

set(findall(gcf,'-property','FontSize'),'FontSize',18)

%% add rhythm-MSE coupling

pn.data = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/D_eBOSC_noDur/B_data/C_RhythmCharacteristics/';
load([pn.data, 'C2_EventsMerged.mat'], 'Events_Merged', 'EventsNonRhythm_Merged', 'EventsRhythm_Merged');

freq = 2.^[1:.125:6];
freq = freq(4:end-3);
 
% load stats cluster of bandpass difference

pn.root = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/A_MSE_CSD_multiVariant/';
load([pn.root, 'B_data/E2_CBPA_Age_Condition_v2.mat'], 'stat', 'methodLabels')

% correlate with individual MSE values

load('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/A_MSE_CSD_multiVariant/B_data/E_mseMerged.mat')

betaCluster = max(stat{4,5}.mask(:,23:27),[],2);
alphaCluster = max(stat{4,5}.mask(:,15:20),[],2);

BetaMSE_YA = squeeze(nanmean(nanmedian(mseMerged{1,2}.MSEbp(:,betaCluster,mseMerged{1,1}.freq> 14 & mseMerged{1,1}.freq< 20),3),2));
BetaMSE_OA = squeeze(nanmean(nanmedian(mseMerged{2,2}.MSEbp(:,betaCluster,mseMerged{1,1}.freq> 14 & mseMerged{1,1}.freq< 20),3),2));

AlphaMSE_YA = squeeze(nanmean(nanmedian(mseMerged{1,2}.MSEbp(:,alphaCluster,mseMerged{1,1}.freq> 8 & mseMerged{1,1}.freq< 12),3),2));
AlphaMSE_OA = squeeze(nanmean(nanmedian(mseMerged{2,2}.MSEbp(:,alphaCluster,mseMerged{1,1}.freq> 8 & mseMerged{1,1}.freq< 12),3),2));

BetaRate_YA = squeeze(nanmean(max(EventsRhythm_Merged(1:47, betaCluster, freq > 14 & freq< 20,2),[],3),2));
BetaRate_OA = squeeze(nanmean(max(EventsRhythm_Merged(48:end, betaCluster, freq > 14 & freq< 20,2),[],3),2));

AlphaRate_YA = squeeze(nanmean(max(EventsRhythm_Merged(1:47, alphaCluster, freq > 8 & freq< 12,2),[],3),2));
AlphaRate_OA = squeeze(nanmean(max(EventsRhythm_Merged(48:end, alphaCluster, freq > 8 & freq< 12,2),[],3),2));

subplot(3,4,7); cla; hold on;
    x = AlphaRate_YA; y = AlphaMSE_YA; 
    l1 = scatter(x,y, 50,'filled', 'r'); y1_ls = polyval(polyfit(x,y,1),x); y1_ls = plot(x, y1_ls,'Color', 'r', 'LineWidth', 2);
    [r1, p1] = corrcoef(x, y); p1 = convertPtoExponential(p1(2));
    x = AlphaRate_OA; y = AlphaMSE_OA; 
    l3 = scatter(x,y, 50,'filled', 'k'); y3_ls = polyval(polyfit(x,y,1),x); y3_ls = plot(x, y3_ls,'Color', 'k', 'LineWidth', 2);
    [r3, p3] = corrcoef(x, y); p3 = convertPtoExponential(p3(2));
    legend([y1_ls, y3_ls], {['YA Alpha: r = ', num2str(round(r1(2),2)), ', p = ' p1{1}], ...
        ['OA Alpha: r = ', num2str(round(r3(2),2)), ', p = ' p3{1}]}); legend('boxoff');
    xlabel('Number of Alpha events'); ylabel('Entropy');
    ylim([.15 .4])
    title('Parietooccipital Alpha')
subplot(3,4,8); cla; hold on;
    x = BetaRate_YA; y = BetaMSE_YA; 
    l2 = scatter(x,y, 50,'filled', 'r'); y2_ls = polyval(polyfit(x,y,1),x); y2_ls = plot(x, y2_ls,'Color', 'r', 'LineWidth', 2);
    [r2, p2] = corrcoef(x, y);  p2 = convertPtoExponential(p2(2));
    x = BetaRate_OA; y = BetaMSE_OA; 
    l4 = scatter(x,y, 50,'filled', 'k'); y4_ls = polyval(polyfit(x,y,1),x); y4_ls = plot(x, y4_ls,'Color', 'k', 'LineWidth', 2);
    [r4, p4] = corrcoef(x, y);  p4 = convertPtoExponential(p4(2));
    legend([y2_ls, y4_ls], {['YA Beta: r = ', num2str(round(r2(2),2)), ', p = ' p2{1}],...
        ['OA Beta: r = ', num2str(round(r4(2),2)), ', p = ' p4{1}]}); legend('boxoff');
    xlabel('Number of Beta events'); ylabel('Entropy');
    ylim([.25 .4])
    title('Central Beta')
set(findall(gcf,'-property','FontSize'),'FontSize',18)

% get partial correlations

[r,p] = partialcorr(AlphaRate_YA, AlphaMSE_YA, [BetaRate_YA, BetaMSE_YA])
[r,p] = partialcorr(AlphaRate_OA, AlphaMSE_OA, [BetaRate_OA, BetaMSE_OA])
[r,p] = partialcorr(BetaRate_YA, BetaMSE_YA, [AlphaRate_YA, AlphaMSE_YA])
[r,p] = partialcorr(BetaRate_OA, BetaMSE_OA, [AlphaRate_OA, AlphaMSE_OA])

% load stats matrix of rhythm rate age difference

load([pn.data, 'AgeDifferenceRhythmRate.mat'], 'stat')
load('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/A_MSE_CSD_multiVariant/B_data/elec.mat')

cfg = [];
cfg.layout = 'acticap-64ch-standard2.mat';
cfg.parameter = 'powspctrm';
cfg.comment = 'no';
cfg.zlim = [-5 5];
cfg.style = 'both';
cfg.colormap = cBrew;
cfg.marker = 'off';

subplot(3,4,11)
    cfg.highlight = 'yes';
    cfg.highlightchannel = elec.label(max(stat.mask(:,15:19),[],2)); % 8 to 12 Hz
    plotData = [];
    plotData.label = elec.label(1:60); % {1 x N}
    plotData.dimord = 'chan';
    plotData.powspctrm = squeeze(median(stat.stat(:,15:19),2));
    ft_topoplotER(cfg,plotData);
    % cb = colorbar('SouthOutside'); set(get(cb,'title'),'string','t values');
    pval = []; pval = convertPtoExponential(stat.negclusters(1).prob);
    title({'Alpha event rate: OA minus YA', ['p = ', pval{1}]});
subplot(3,4,12)
    cfg.highlight = 'yes';
    cfg.highlightchannel = elec.label(max(stat.mask(:,21:24),[],2)); % 14 to 20 Hz
    plotData = [];
    plotData.label = elec.label(1:60); % {1 x N}
    plotData.dimord = 'chan';
    plotData.powspctrm = squeeze(median(stat.stat(:,21:24),2));
    topo2 = ft_topoplotER(cfg,plotData);
    % cb = colorbar('SouthOutside'); set(get(cb,'title'),'string','t values');
    pval = []; pval = convertPtoExponential(stat.posclusters(1).prob);
    title({'Beta event rate: OA minus YA', ['p = ', pval{1}]});

    set(findall(gcf,'-property','FontSize'),'FontSize',18)

%% save Figure
    
pn.plotFolder = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/A_MSE_CSD_multiVariant/C_figures/';
figureName = 'M_overviewPlot_BPMSE_Rate';
saveas(h, [pn.plotFolder, figureName], 'epsc');
saveas(h, [pn.plotFolder, figureName], 'png');
