%% Create a summary figure of the age-dependent spectra and their reflection in 'Original' MSE scales

load('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/A_MSE_CSD_multiVariant/B_data/E_mseMerged.mat', 'mseMerged', 'IDs')

% add convertPtoExponential
addpath('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/A_MSE_CSD_multiVariant/T_tools/')

pn.shadedError = ['/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/A_MSE_CSD_multiVariant/T_tools/shadedErrorBar']; addpath(pn.shadedError);

indCond = 2; channels = 1:60;

%% Plot PSD spectra including slope fits

load('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/B_FFT/B_data/B_FFT_grandavg_individual.mat')

% figure; 
% subplot(1,2,1); imagesc(squeeze(nanmean(grandavg_EC.powspctrm,2))); title('Eyes Closed');
% subplot(1,2,2); imagesc(squeeze(nanmean(grandavg_EO.powspctrm,2))); title('Eyes Open');

commonYA = info.IDs_all(ismember(info.IDs_all, IDs{1}));
commonOA = info.IDs_all(ismember(info.IDs_all, IDs{2}));

idx_YA_fft = ismember(info.IDs_all, commonYA);
idx_OA_fft = ismember(info.IDs_all, commonOA);
idx_YA_mse = ismember(IDs{1}, commonYA);
idx_OA_mse = ismember(IDs{2}, commonOA);

pn.shadedError = ['/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/A_MSE_CSD_multiVariant/T_tools/shadedErrorBar']; addpath(pn.shadedError);

h = figure('units','normalized','position',[.1 .1 .15 .4]);
set(gcf,'renderer','Painters')
subplot(2,1,1); cla; hold on;
    curAverage = nanmean(log10(grandavg_EO.powspctrm(idx_YA_fft,:,:)),2);
    l1 = plot(log10(grandavg_EO.freq),squeeze(nanmean(curAverage,1)), 'Color', 'r', 'LineWidth', 5);
    curAverage = nanmean(log10(grandavg_EO.powspctrm(idx_OA_fft,:,:)),2);
    l2 = plot(log10(grandavg_EO.freq),squeeze(nanmean(curAverage,1)), 'Color', 'k', 'LineWidth', 5);
    %set(gca, 'XScale', 'log')
    xlim([.3, 1.8])
    set(gca, 'XTick',log10(grandavg_EC.freq(3:8:end)));    
    set(gca, 'XTickLabels',round(grandavg_EC.freq(3:8:end)));
    %set(gca, 'XTickLabels',[]);
    xlabel('Frequency [Hz, log-10]'); ylabel({'Power [log-scaled]'; ''})
    set(gca, 'YTick', []);
    set(gca, 'XAxisLocation', 'top')
    set(gca, 'YAxisLocation', 'right')
    set(gca,'XColor','none');
    set(gca,'YColor','none');
    
%% superimpose mean slope fits

load('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/B_FFT/B_data/D_PSDslopes_loglog.mat')

y1 = squeeze(nanmean(linFit_2_30_EO(:, channels),2));
y2 = squeeze(nanmean(linFit_2_30_EO_int(:, channels),2));
y = [y1,y2];
x = log10(grandavg_EC.freq);

%figure; hold on;
y2_ls = [];
for indID = 1:size(y,1)
    y2_ls(indID,:) = polyval(y(indID,:),x); %y2_ls = plot(x, y2_ls, 'Color', 'k', 'LineWidth', 3);
end

%figure;
curAverage = y2_ls(idx_YA_fft,:);
standError = nanstd(curAverage,1)./sqrt(size(curAverage,1));
l3 = shadedErrorBar(x,nanmean(curAverage,1),standError, 'lineprops', {'LineStyle', '--', 'Color', 'r', 'LineWidth', 2,}, 'patchSaturation', .09);

curAverage = y2_ls(idx_OA_fft,:);
standError = nanstd(curAverage,1)./sqrt(size(curAverage,1));
l4 = shadedErrorBar(x,nanmean(curAverage,1),standError, 'lineprops', {'LineStyle', '--', 'Color', 'k', 'LineWidth', 2,}, 'patchSaturation', .09);

%% Plot 'Original' MSE spectrum

subplot(2,1,2); cla; hold on;
    curAverage = nanmean(mseMerged{1,indCond}.MSEVanilla(:, channels,:),2);
%     standError = nanstd(curAverage,1)./sqrt(size(curAverage,1));
%     l1 = shadedErrorBar([],nanmean(curAverage,1),standError, 'lineprops', {'Color', 'r', 'LineWidth', 3,}, 'patchSaturation', .09);
    l1 = plot(squeeze(nanmean(curAverage,1)), 'Color', 'r', 'LineWidth', 5);
    curAverage = nanmean(mseMerged{2,indCond}.MSEVanilla(:, channels,:),2);
%     standError = nanstd(curAverage,1)./sqrt(size(curAverage,1));
%     l2 = shadedErrorBar([],nanmean(curAverage,1),standError, 'lineprops', {'Color', 'k', 'LineWidth', 3,}, 'patchSaturation', .09);
    l2 = plot(squeeze(nanmean(curAverage,1)), 'Color', 'k', 'LineWidth', 5);
    % mseMerged{1,indCond}.freq
    xlim([1 41]);
    ylim([.2 .85]);
    %set(gca, 'XScale', 'log')
    scales = 1:41;
    %set(gca, 'XTick',round(logspace(log10(1), log10(41), 6)));
    set(gca, 'XTick',round(linspace(1,41, 4)));
    tmp_meas = [round(mseMerged{1,indCond}.freq(get(gca, 'XTick')),0); round(scales(get(gca, 'XTick')),0)];
    xLabels = [];
    for indScale = 1:size(tmp_meas,2)
        xLabels{indScale} = [num2str(tmp_meas(1,indScale)), '; ',num2str(tmp_meas(2,indScale))];
    end; clear tmp_meas;
    set(gca, 'XTickLabels', xLabels);
    legend([l1, l2],{'Younger adults'; 'Older adults'}, 'orientation', 'vertical', 'location', 'South'); legend('boxoff');
    %set(gca, 'XDir','reverse')
    %legend([l1.mainLine, l2.mainLine],{'Young adults'; 'Older adults'}, 'location', 'NorthWest'); legend('boxoff')
    %title({'Eyes Open rest: Entropy spectrum'; ''}); 
    set(gca, 'YTick', []);
    xlabel('Time scale [Hz; scale]'); ylabel({'Sample Entropy'; ''})
    set(gca, 'YAxisLocation', 'right')
    set(findall(gcf,'-property','FontSize'),'FontSize',20);
    set(gca,'XColor','none');
    set(gca,'YColor','none');
    
%% save Figure
    
pn.plotFolder = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/A_MSE_CSD_multiVariant/C_figures/';
figureName = 'Z_SummarySchematic';

saveas(h, [pn.plotFolder, figureName], 'fig');
saveas(h, [pn.plotFolder, figureName], 'epsc');
saveas(h, [pn.plotFolder, figureName], 'png');