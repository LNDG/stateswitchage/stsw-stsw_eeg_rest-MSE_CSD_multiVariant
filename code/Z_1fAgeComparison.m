%% Plot difference between filter implementations and MSE variants

    clear all; clc; restoredefaultpath;

    pn.root = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/A_MSE_CSD_multiVariant/';

    load([pn.root, 'B_data/E_mseMerged.mat'], 'mseMerged', 'IDs')

    addpath([pn.root, 'T_tools/']); % add convertPtoExponential
    addpath([pn.root, 'T_tools/shadedErrorBar']); % add shadedErrorBar

    % add fieldtrip for topoplots
    
    pn.root     = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/';
    pn.tools	= [pn.root, 'B_analyses/S2_TFR/T_tools/']; addpath(pn.tools);
    addpath([pn.tools, 'fieldtrip-20170904/']); ft_defaults;

    load('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S2_TFR/B_data/elec.mat')
    
    indEC = 1; indEO = 2; channels = 1:60;
    
%% Supplementary: load PSD & compare power spectra

    load('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/B_FFT/B_data/B_FFT_grandavg_individual.mat')

    % figure; 
    % subplot(1,2,1); imagesc(squeeze(nanmean(grandavg_EC.powspctrm,2))); title('Eyes Closed');
    % subplot(1,2,2); imagesc(squeeze(nanmean(grandavg_EO.powspctrm,2))); title('Eyes Open');

    commonYA = info.IDs_all(ismember(info.IDs_all, IDs{1}));
    commonOA = info.IDs_all(ismember(info.IDs_all, IDs{2}));

    idx_YA_fft = ismember(info.IDs_all, commonYA);
    idx_OA_fft = ismember(info.IDs_all, commonOA);
    idx_YA_mse = ismember(IDs{1}, commonYA);
    idx_OA_mse = ismember(IDs{2}, commonOA);

    indEO = 2; channels = 1:60;

    h = figure('units','normalized','position',[.1 .1 .15 .2]);
    set(h, 'renderer', 'Painters')
        %subplot(2,1,2); 
        cla; hold on;
        curAverage = nanmean(log10(grandavg_EO.powspctrm(idx_YA_fft,:,:)),2);
        standError = nanstd(curAverage,1)./sqrt(size(curAverage,1));
        l1 = shadedErrorBar(grandavg_EC.freq,nanmean(curAverage,1),standError, 'lineprops', {'Color', 'r', 'LineWidth', 3,}, 'patchSaturation', .09);
        curAverage = nanmean(log10(grandavg_EO.powspctrm(idx_OA_fft,:,:)),2);
        standError = nanstd(curAverage,1)./sqrt(size(curAverage,1));
        l2 = shadedErrorBar(grandavg_EC.freq,nanmean(curAverage,1),standError, 'lineprops', {'Color', 'k', 'LineWidth', 3,}, 'patchSaturation', .09);
        %title({'Power spectrum'});
        set(gca, 'XScale', 'log')
        set(gca, 'YScale', 'log')
        %set(gca, 'XDir','reverse')
        xlim([2, 64])
        set(gca, 'XTick',grandavg_EC.freq(3:8:end));    
        %legend([l1.mainLine, l2.mainLine],{'Young adults'; 'Older adults'}); legend('boxoff');
        xlabel('Frequency [Hz, log10-scaled]'); ylabel('log10 Power (a.u.)')
        set(findall(gcf,'-property','FontSize'),'FontSize',23);
        set(xAX,'FontSize', 18); set(xl, 'FontSize', 23);
    pn.plotFolder = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/A_MSE_CSD_multiVariant/C_figures/';
    figureName = 'Z_PSD_normal1f';

    saveas(h, [pn.plotFolder, figureName], 'fig');
    saveas(h, [pn.plotFolder, figureName], 'epsc');
    saveas(h, [pn.plotFolder, figureName], 'png');
