% Statistically compare effect sizes between different methods.

%% load MSE topography with different filter settings

pn.root = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/A_MSE_CSD_multiVariant/';
load([pn.root, 'B_data/E2_CBPA_Age_Condition_v2.mat'])

% Note that time scales are inverted between statistical output matrix and
% original data matrices!

% extract individual values from statistical cluster (Vanilla version)

FastMSEVanillaCluster = find(max(stat{4,1}.mask(:,39:41),[],2));
SlowMSEVanillaCluster = find(max(stat{4,1}.mask(:,1:3),[],2));

load([pn.root, 'B_data/E_mseMerged.mat'])

%% calculate R squared : t squared divided by (t squared +df)

% identical to correlation: corrcoef([x1;x2], [repmat(1,numel(x1),1); repmat(2,numel(x2),1)])

x1 = nanmean(nanmean(mseMerged{1,2}.MSEVanilla(:,FastMSEVanillaCluster,1:3),3),2);
x2 = nanmean(nanmean(mseMerged{2,2}.MSEVanilla(:,FastMSEVanillaCluster,1:3),3),2);
[h,p,ci,stats] = ttest2(x1,x2);
rsquare = (stats.tstat^2)./(stats.tstat^2 + stats.df);
rVanillaFast = sqrt(rsquare);

x1 = nanmean(nanmean(mseMerged{1,2}.MSEVanilla(:,SlowMSEVanillaCluster,39:41),3),2);
x2 = nanmean(nanmean(mseMerged{2,2}.MSEVanilla(:,SlowMSEVanillaCluster,39:41),3),2);
[h,p,ci,stats] = ttest2(x1,x2);
rsquare = (stats.tstat^2)./(stats.tstat^2 + stats.df);
rVanillaSlow = sqrt(rsquare);

x1 = nanmean(nanmean(mseMerged{1,2}.MSEhp(:,FastMSEVanillaCluster,1:3),3),2);
x2 = nanmean(nanmean(mseMerged{2,2}.MSEhp(:,FastMSEVanillaCluster,1:3),3),2);
[h,p,ci,stats] = ttest2(x1,x2);
rsquare = (stats.tstat^2)./(stats.tstat^2 + stats.df);
rHPFast = sqrt(rsquare);

x1 = nanmean(nanmean(mseMerged{1,2}.MSEPointavg(:,SlowMSEVanillaCluster,39:41),3),2);
x2 = nanmean(nanmean(mseMerged{2,2}.MSEPointavg(:,SlowMSEVanillaCluster,39:41),3),2);
[h,p,ci,stats] = ttest2(x1,x2);
rsquare = (stats.tstat^2)./(stats.tstat^2 + stats.df);
rRrescaleSlow = sqrt(rsquare);

x1 = nanmean(nanmean(mseMerged{1,2}.MSEPointavg(:,FastMSEVanillaCluster,1:3),3),2);
x2 = nanmean(nanmean(mseMerged{2,2}.MSEPointavg(:,FastMSEVanillaCluster,1:3),3),2);
[h,p,ci,stats] = ttest2(x1,x2);
rsquare = (stats.tstat^2)./(stats.tstat^2 + stats.df);
rRrescaleFast = sqrt(rsquare);

%figure; bar([nanmean(x1,1),nanmean(x2,1)])

%% calculate r-to-z transform; compare correlation coefficients

% https://www.statisticssolutions.com/comparing-correlation-coefficients/
% https://de.mathworks.com/matlabcentral/fileexchange/44658-compare_correlation_coefficients
%Zobserved = (z1-z2) / (sqrt([(1/N1-3) + (1/N2-3)]));

% The following function already performs the calculation for us:

addpath([pn.root, 'T_tools/compare_correlation_coefficients'])

compare_correlation_coefficients(rVanillaFast, rHPFast, 97,97)
compare_correlation_coefficients(rVanillaFast, rRrescaleFast, 97,97)
compare_correlation_coefficients(rVanillaSlow, rRrescaleSlow, 97,97)

