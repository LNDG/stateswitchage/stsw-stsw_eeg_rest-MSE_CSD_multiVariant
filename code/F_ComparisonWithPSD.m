%% Plot difference between filter implementations and MSE variants

    clear all; clc; restoredefaultpath;

    pn.root = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/A_MSE_CSD_multiVariant/';

    load([pn.root, 'B_data/E_mseMerged.mat'], 'mseMerged', 'IDs')

    addpath([pn.root, 'T_tools/']); % add convertPtoExponential
    addpath([pn.root, 'T_tools/shadedErrorBar']); % add shadedErrorBar

    % add fieldtrip for topoplots
    
    pn.root     = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/';
    pn.tools	= [pn.root, 'B_analyses/S2_TFR/T_tools/']; addpath(pn.tools);
    addpath([pn.tools, 'fieldtrip-20170904/']); ft_defaults;

    load('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S2_TFR/B_data/elec.mat')
    
    indEC = 1; indEO = 2; channels = 1:60;
    
%% Supplementary: load PSD & compare power spectra

    load('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/B_FFT/B_data/B_FFT_grandavg_individual.mat')

    % figure; 
    % subplot(1,2,1); imagesc(squeeze(nanmean(grandavg_EC.powspctrm,2))); title('Eyes Closed');
    % subplot(1,2,2); imagesc(squeeze(nanmean(grandavg_EO.powspctrm,2))); title('Eyes Open');

    commonYA = info.IDs_all(ismember(info.IDs_all, IDs{1}));
    commonOA = info.IDs_all(ismember(info.IDs_all, IDs{2}));

    idx_YA_fft = ismember(info.IDs_all, commonYA);
    idx_OA_fft = ismember(info.IDs_all, commonOA);
    idx_YA_mse = ismember(IDs{1}, commonYA);
    idx_OA_mse = ismember(IDs{2}, commonOA);

    h = figure('units','normalized','position',[.1 .1 .5 .7]);
    set(gcf,'renderer','Painters')
    subplot(2,1,1); hold on;
        curAverage = nanmean(log10(grandavg_EC.powspctrm(idx_YA_fft,:,:)),2);
        standError = nanstd(curAverage,1)./sqrt(size(curAverage,1));
        shadedErrorBar(log10(grandavg_EC.freq),nanmean(curAverage,1),standError, 'lineprops', 'r', 'patchSaturation', .05);
        curAverage = nanmean(log10(grandavg_EC.powspctrm(idx_OA_fft,:,:)),2);
        standError = nanstd(curAverage,1)./sqrt(size(curAverage,1));
        shadedErrorBar(log10(grandavg_EC.freq),nanmean(curAverage,1),standError, 'lineprops', 'k', 'patchSaturation', .05);
        title('Eyes Closed');
        xlabel('Frequency (log10 Hz)'); ylabel('Power (log10)')
    subplot(2,1,2); hold on;
        curAverage = nanmean(log10(grandavg_EO.powspctrm(idx_YA_fft,:,:)),2);
        standError = nanstd(curAverage,1)./sqrt(size(curAverage,1));
        l1 = shadedErrorBar(log10(grandavg_EC.freq),nanmean(curAverage,1),standError, 'lineprops', 'r', 'patchSaturation', .05);
        curAverage = nanmean(log10(grandavg_EO.powspctrm(idx_OA_fft,:,:)),2);
        standError = nanstd(curAverage,1)./sqrt(size(curAverage,1));
        l2 = shadedErrorBar(log10(grandavg_EC.freq),nanmean(curAverage,1),standError, 'lineprops', 'k', 'patchSaturation', .05);
        title('Eyes Open');
        legend([l1.mainLine, l2.mainLine],{'Young adults'; 'Older adults'});
        set(findall(gcf,'-property','FontSize'),'FontSize',18); legend('boxoff');
        xlabel('Frequency (log10 Hz)'); ylabel('Power (log10)')
    pn.plotFolder = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/A_MSE_CSD_multiVariant/C_figures/';
    figureName = 'F1_PSD';

    saveas(h, [pn.plotFolder, figureName], 'fig');
    saveas(h, [pn.plotFolder, figureName], 'epsc');
    saveas(h, [pn.plotFolder, figureName], 'png');

%% Figure 4A: contrast Vanilla MSE spectrum with PSD

    indEO = 2; channels = 1:60;

    h = figure('units','normalized','position',[.1 .1 .15 .5]);
    set(h, 'renderer', 'Painters')
    subplot(2,1,1); cla; hold on;
        curAverage = nanmean(mseMerged{1,indEO}.MSEVanilla(:, channels,:),2);
        standError = nanstd(curAverage,1)./sqrt(size(curAverage,1));
        l1 = shadedErrorBar([],nanmean(curAverage,1),standError, 'lineprops', {'Color', 'r', 'LineWidth', 3,}, 'patchSaturation', .09);
        curAverage = nanmean(mseMerged{2,indEO}.MSEVanilla(:, channels,:),2);
        standError = nanstd(curAverage,1)./sqrt(size(curAverage,1));
        l2 = shadedErrorBar([],nanmean(curAverage,1),standError, 'lineprops', {'Color', 'k', 'LineWidth', 3,}, 'patchSaturation', .09);
        % mseMerged{1,indEO}.freq
        xlim([1 41]); 
        set(gca, 'XScale', 'log')
        scales = 1:41;
        set(gca, 'YTick',[.4:.2:.9]);
        set(gca, 'XTick',round(logspace(log10(1), log10(41), 4)));
        tmp_meas = [round(mseMerged{1,indEO}.freq(get(gca, 'XTick')),0); round(scales(get(gca, 'XTick')),0)];
        xLabels = [];
        for indScale = 1:size(tmp_meas,2)
            xLabels{indScale} = [num2str(tmp_meas(1,indScale)), ' Hz\newline(scale ',num2str(tmp_meas(2,indScale)), ')'];
        end; clear tmp_meas;
        set(gca, 'XTickLabels', xLabels);
        xl = get(gca,'XLabel'); xlFontSize = get(xl,'FontSize'); xAX = get(gca,'XAxis');
        legend([l1.mainLine, l2.mainLine],{'Young adults'; 'Older adults'}, 'location', 'South'); legend('boxoff')
        title({'Eyes Open rest:';'Original Entropy'}); xlabel('Time scale [Hz, log-scaled]'); ylabel('Sample Entropy')
    subplot(2,1,2); cla; hold on;
        curAverage = nanmean(grandavg_EO.powspctrm(idx_YA_fft,:,:),2);
        standError = nanstd(curAverage,1)./sqrt(size(curAverage,1));
        l1 = shadedErrorBar(grandavg_EC.freq,nanmean(curAverage,1),standError, 'lineprops', {'Color', 'r', 'LineWidth', 3,}, 'patchSaturation', .09);
        curAverage = nanmean(grandavg_EO.powspctrm(idx_OA_fft,:,:),2);
        standError = nanstd(curAverage,1)./sqrt(size(curAverage,1));
        l2 = shadedErrorBar(grandavg_EC.freq,nanmean(curAverage,1),standError, 'lineprops', {'Color', 'k', 'LineWidth', 3,}, 'patchSaturation', .09);
        title({'Power spectrum'});
        set(gca, 'XScale', 'log')
        set(gca, 'XDir','reverse')
        xlim([2, 64])
        set(gca, 'XTick',grandavg_EC.freq(3:8:end));    
        %legend([l1.mainLine, l2.mainLine],{'Young adults'; 'Older adults'}); legend('boxoff');
        xlabel('Frequency [Hz, log-scaled]'); ylabel('Power')
        set(findall(gcf,'-property','FontSize'),'FontSize',23);
        set(xAX,'FontSize', 18); set(xl, 'FontSize', 23);
    pn.plotFolder = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/A_MSE_CSD_multiVariant/C_figures/';
    figureName = 'F1_MSE_PSD';
    saveas(h, [pn.plotFolder, figureName], 'epsc');
    saveas(h, [pn.plotFolder, figureName], 'fig');
    saveas(h, [pn.plotFolder, figureName], 'png');

%% load PSD slopes

    load('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/B_FFT/B_data/D_PSDslopes.mat')

%% Supplementary Figure: Scatterplots of PSD correlations across methods
    % PSD slope is positively coupled with high freq entropy, negatively
    % with low freq entropy with 'Original' settings

    metrics = {'MSEVanilla'; 'MSEPointavg'; 'MSEfiltskip'; 'MSEhp'; 'MSEbp'};
    channels = 1:60;
    metricLabels = {'Vanilla'; 'Vanilla + scale-wise R'; 'Lowpass + scale-wise R'; 'Highpass + scale-wise R'; 'Bandpass + scale-wise R'};
    indEO = 2;
    
    h = figure('units','normalized','position',[0 0 1 .4]);
    for indMetric = 1:5
    subplot(2,5,indMetric); hold on;
        x = squeeze(nanmedian(nanmean(mseMerged{1,indEO}.(metrics{indMetric})(idx_YA_mse,channels,1:3),3),2));
        y = squeeze(nanmedian(linFit_2_30_EO(idx_YA_fft, channels),2));
        l1 = scatter(x,y, 'filled', 'k'); y1_ls = polyval(polyfit(x,y,1),x); y1_ls = plot(x, y1_ls, 'Color', 'k');
        [rl1,pl1] = corrcoef(x,y); pval1 = []; pval1 = convertPtoExponential(pl1(2));
        % add OA
        x = squeeze(nanmedian(nanmean(mseMerged{2,indEO}.(metrics{indMetric})(idx_OA_mse,channels,1:3),3),2));
        y = squeeze(nanmedian(linFit_2_30_EO(idx_OA_fft, channels),2));
        l2 = scatter(x,y, 'filled', 'r'); y2_ls = polyval(polyfit(x,y,1),x); y2_ls = plot(x, y2_ls, 'Color', 'r');
        [rl2,pl2] = corrcoef(x,y); pval2 = []; pval2 = convertPtoExponential(pl2(2));
        legend([y1_ls, y2_ls], {['Younger Adults: r=', num2str(round(rl1(2),2)), ', p=',pval1{1}], ...
            ['Older Adults: r=', num2str(round(rl2(2),2)), ', p=',pval2{1}]}, 'Location', 'SouthEast'); legend('boxoff');
        xlabel('MSE, Scales 1:3'); ylabel('PSD Slope 2-64 Hz (excl. 7-13 Hz)');
        title([metricLabels{indMetric}, ': fast MSE'])
        ylim([-.06, 0.01]);
    subplot(2,5,5+indMetric); hold on;
        x = squeeze(nanmedian(nanmean(mseMerged{1,indEO}.(metrics{indMetric})(idx_YA_mse,channels,35:41),3),2));
        y = squeeze(nanmedian(linFit_2_30_EO(idx_YA_fft, channels),2));
        l1 = scatter(x,y, 'filled', 'k'); y1_ls = polyval(polyfit(x,y,1),x); y1_ls = plot(x, y1_ls, 'Color', 'k');
        [rl1,pl1] = corrcoef(x,y); pval1 = []; pval1 = convertPtoExponential(pl1(2));
        % add OA
        x = squeeze(nanmedian(nanmean(mseMerged{2,indEO}.(metrics{indMetric})(idx_OA_mse,channels,35:41),3),2));
        y = squeeze(nanmedian(linFit_2_30_EO(idx_OA_fft, channels),2));
        l2 = scatter(x,y, 'filled', 'r'); y2_ls = polyval(polyfit(x,y,1),x); y2_ls = plot(x, y2_ls, 'Color', 'r');
        [rl2,pl2] = corrcoef(x,y); pval2 = []; pval2 = convertPtoExponential(pl2(2));
        legend([y1_ls, y2_ls], {['Younger Adults: r=', num2str(round(rl1(2),2)), ', p=',pval1{1}], ...
            ['Older Adults: r=', num2str(round(rl2(2),2)), ', p=',pval2{1}]}, 'Location', 'SouthEast'); legend('boxoff');
        xlabel('MSE, Scales 35-41'); ylabel('PSD Slope 2-64 Hz (excl. 7-13 Hz)');
        title([metricLabels{indMetric}, ': coarse MSE'])
        ylim([-.06, 0.01]);
    end
    set(findall(gcf,'-property','FontSize'),'FontSize',15)
    pn.plotFolder = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/A_MSE_CSD_multiVariant/C_figures/';
    figureName = 'F2_PSDLinkAcrossMethods';
    saveas(h, [pn.plotFolder, figureName], 'epsc');
    saveas(h, [pn.plotFolder, figureName], 'png');
    
    %% Supplementary: correlations using low-frequency power
        
    metrics = {'MSEVanilla'; 'MSEPointavg'; 'MSEfiltskip'; 'MSEhp'; 'MSEbp'};
    channels = 1:60;
    metricLabels = {'Vanilla'; 'Vanilla + scale-wise R'; 'Lowpass + scale-wise R'; 'Highpass + scale-wise R'; 'Bandpass + scale-wise R'};
    indEO = 2;
    
    h = figure('units','normalized','position',[0 0 1 .4]);
    set(gcf,'renderer','Painters')
    for indMetric = 1:5
    subplot(2,5,indMetric); hold on;
        x = squeeze(nanmedian(nanmean(mseMerged{1,indEO}.(metrics{indMetric})(idx_YA_mse,channels,1:3),3),2));
        y = squeeze(nanmedian(nanmean(grandavg_EO.powspctrm(idx_YA_fft,channels,1:16),3),2));
        l1 = scatter(x,y, 'filled', 'k'); y1_ls = polyval(polyfit(x,y,1),x); y1_ls = plot(x, y1_ls, 'Color', 'k');
        [rl1,pl1] = corrcoef(x,y); pval1 = []; pval1 = convertPtoExponential(pl1(2));
        % add OA
        x = squeeze(nanmedian(nanmean(mseMerged{2,indEO}.(metrics{indMetric})(idx_OA_mse,channels,1:3),3),2));
        y = squeeze(nanmedian(nanmean(grandavg_EO.powspctrm(idx_OA_fft,channels,1:16),3),2));
        l2 = scatter(x,y, 'filled', 'r'); y2_ls = polyval(polyfit(x,y,1),x); y2_ls = plot(x, y2_ls, 'Color', 'r');
        [rl2,pl2] = corrcoef(x,y); pval2 = []; pval2 = convertPtoExponential(pl2(2));
        legend([y1_ls, y2_ls], {['Younger Adults: r=', num2str(round(rl1(2),2)), ', p=',pval1{1}], ...
            ['Older Adults: r=', num2str(round(rl2(2),2)), ', p=',pval2{1}]}, 'Location', 'SouthEast'); legend('boxoff');
        xlabel('MSE, Scales 1:3'); ylabel('Power 2-8 Hz');
        title([metricLabels{indMetric}, ': fast MSE'])
        ylim([-2.5*10^-8, 2.5*10^-8]);
    subplot(2,5,5+indMetric); hold on;
        x = squeeze(nanmedian(nanmean(mseMerged{1,indEO}.(metrics{indMetric})(idx_YA_mse,channels,35:41),3),2));
        y = squeeze(nanmedian(nanmean(grandavg_EO.powspctrm(idx_YA_fft,channels,1:16),3),2));
        l1 = scatter(x,y, 'filled', 'k'); y1_ls = polyval(polyfit(x,y,1),x); y1_ls = plot(x, y1_ls, 'Color', 'k');
        [rl1,pl1] = corrcoef(x,y); pval1 = []; pval1 = convertPtoExponential(pl1(2));
        % add OA
        x = squeeze(nanmedian(nanmean(mseMerged{2,indEO}.(metrics{indMetric})(idx_OA_mse,channels,35:41),3),2));
        y = squeeze(nanmedian(nanmean(grandavg_EO.powspctrm(idx_OA_fft,channels,1:16),3),2));
        l2 = scatter(x,y, 'filled', 'r'); y2_ls = polyval(polyfit(x,y,1),x); y2_ls = plot(x, y2_ls, 'Color', 'r');
        [rl2,pl2] = corrcoef(x,y); pval2 = []; pval2 = convertPtoExponential(pl2(2));
        legend([y1_ls, y2_ls], {['Younger Adults: r=', num2str(round(rl1(2),2)), ', p=',pval1{1}], ...
            ['Older Adults: r=', num2str(round(rl2(2),2)), ', p=',pval2{1}]}, 'Location', 'SouthEast'); legend('boxoff');
        xlabel('MSE, Scales 35-41'); ylabel('Power 2-8 Hz');
        title([metricLabels{indMetric}, ': coarse MSE'])
        ylim([-2.5*10^-8, 2.5*10^-8]);
    end
    set(findall(gcf,'-property','FontSize'),'FontSize',15)

    pn.plotFolder = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/A_MSE_CSD_multiVariant/C_figures/';
    figureName = 'F2_LowFreqPowerCorrelation';
    saveas(h, [pn.plotFolder, figureName], 'epsc');
    saveas(h, [pn.plotFolder, figureName], 'png');
   
    %% Supplementary: Correlations with high-frequency power
    
    h = figure('units','normalized','position',[0 0 1 .4]);
    for indMetric = 1:5
    subplot(2,5,indMetric); hold on;
        x = squeeze(nanmedian(nanmean(mseMerged{1,indEO}.(metrics{indMetric})(idx_YA_mse,channels,1:3),3),2));
        y = squeeze(nanmedian(nanmean(grandavg_EO.powspctrm(idx_YA_fft,channels,33:41),3),2));
        l1 = scatter(x,y, 'filled', 'k'); y1_ls = polyval(polyfit(x,y,1),x); y1_ls = plot(x, y1_ls, 'Color', 'k');
        [rl1,pl1] = corrcoef(x,y); pval1 = []; pval1 = convertPtoExponential(pl1(2));
        % add OA
        x = squeeze(nanmedian(nanmean(mseMerged{2,indEO}.(metrics{indMetric})(idx_OA_mse,channels,1:3),3),2));
        y = squeeze(nanmedian(nanmean(grandavg_EO.powspctrm(idx_OA_fft,channels,33:41),3),2));
        l2 = scatter(x,y, 'filled', 'r'); y2_ls = polyval(polyfit(x,y,1),x); y2_ls = plot(x, y2_ls, 'Color', 'r');
        [rl2,pl2] = corrcoef(x,y); pval2 = []; pval2 = convertPtoExponential(pl2(2));
        legend([y1_ls, y2_ls], {['Younger Adults: r=', num2str(round(rl1(2),2)), ', p=',pval1{1}], ...
            ['Older Adults: r=', num2str(round(rl2(2),2)), ', p=',pval2{1}]}, 'Location', 'SouthEast'); legend('boxoff');
        xlabel('MSE, Scales 1:3'); ylabel('Power 32-64 Hz');
        title([metricLabels{indMetric}, ': fast MSE'])
        ylim([-.5*10^-8, .5*10^-8]);
    subplot(2,5,5+indMetric); hold on;
        x = squeeze(nanmedian(nanmean(mseMerged{1,indEO}.(metrics{indMetric})(idx_YA_mse,channels,35:41),3),2));
        y = squeeze(nanmedian(nanmean(grandavg_EO.powspctrm(idx_YA_fft,channels,33:41),3),2));
        l1 = scatter(x,y, 'filled', 'k'); y1_ls = polyval(polyfit(x,y,1),x); y1_ls = plot(x, y1_ls, 'Color', 'k');
        [rl1,pl1] = corrcoef(x,y); pval1 = []; pval1 = convertPtoExponential(pl1(2));
        % add OA
        x = squeeze(nanmedian(nanmean(mseMerged{2,indEO}.(metrics{indMetric})(idx_OA_mse,channels,35:41),3),2));
        y = squeeze(nanmedian(nanmean(grandavg_EO.powspctrm(idx_OA_fft,channels,33:41),3),2));
        l2 = scatter(x,y, 'filled', 'r'); y2_ls = polyval(polyfit(x,y,1),x); y2_ls = plot(x, y2_ls, 'Color', 'r');
        [rl2,pl2] = corrcoef(x,y); pval2 = []; pval2 = convertPtoExponential(pl2(2));
        legend([y1_ls, y2_ls], {['Younger Adults: r=', num2str(round(rl1(2),2)), ', p=',pval1{1}], ...
            ['Older Adults: r=', num2str(round(rl2(2),2)), ', p=',pval2{1}]}, 'Location', 'SouthEast'); legend('boxoff');
        xlabel('MSE, Scales 35-41'); ylabel('Power 32-64 Hz');
        title([metricLabels{indMetric}, ': coarse MSE'])
        ylim([-.5*10^-8, .5*10^-8]);
        set(findall(gcf,'-property','FontSize'),'FontSize',13)
    end
    pn.plotFolder = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/A_MSE_CSD_multiVariant/C_figures/';
    figureName = 'F2_HighFreqPowerCorrelation';
    saveas(h, [pn.plotFolder, figureName], 'epsc');
    saveas(h, [pn.plotFolder, figureName], 'png');
    
    %% Supplementary: plot Vanilla MSE association with R parameter ratio (inverted)
   
    h = figure('units','normalized','position',[.1 .1 .7 .4]);
    scales = 1:3; indEO = 2;
    subplot(1,2,1); hold on;
        x = squeeze(nanmedian(nanmean(mseMerged{1,indEO}.Rhp(idx_YA_mse,channels,scales),3),2))./...
            squeeze(nanmedian(nanmean(mseMerged{1,indEO}.Rvanilla(idx_YA_mse,channels,scales),3),2));
        y = squeeze(nanmedian(nanmean(mseMerged{1,indEO}.MSEVanilla(idx_YA_mse,channels,scales),3),2));
        l1 = scatter(x,y, 'filled', 'k'); lsline;
        [rl1,pl1] = corrcoef(x,y);
        % add OA
        x = squeeze(nanmedian(nanmean(mseMerged{2,indEO}.Rhp(idx_OA_mse,channels,scales),3),2))./...
            squeeze(nanmedian(nanmean(mseMerged{2,indEO}.Rvanilla(idx_OA_mse,channels, scales),3),2));
        y = squeeze(nanmedian(nanmean(mseMerged{2,indEO}.MSEVanilla(idx_OA_mse,channels,scales),3),2));
        l2 = scatter(x,y, 'filled', 'r'); lsline;
        [rl2,pl2] = corrcoef(x,y);
        title('Eyes open: Scales 1-3'); 
        legend([l1, l2], {['Younger Adults: r=', num2str(round(rl1(2),2))], ['Older Adults: r=', num2str(round(rl2(2),2))]}, 'Location', 'NorthWest'); legend('boxoff');
        xlabel('(highpass R)/Vanilla(lowpass) R'); ylabel('MSE Vanilla')
   subplot(1,2,2); hold on;
        indEC = 1;
        x = squeeze(nanmedian(nanmean(mseMerged{1,indEC}.Rhp(idx_YA_mse,channels,scales),3),2))./...
            squeeze(nanmedian(nanmean(mseMerged{1,indEC}.Rvanilla(idx_YA_mse,channels,scales),3),2));
        y = squeeze(nanmedian(nanmean(mseMerged{1,indEC}.MSEVanilla(idx_YA_mse,channels,scales),3),2));
        l1 = scatter(x,y, 'filled', 'k'); lsline;
        [rl1,pl1] = corrcoef(x,y);
        % add OA
        x = squeeze(nanmedian(nanmean(mseMerged{2,indEC}.Rhp(idx_OA_mse,channels,scales),3),2))./...
            squeeze(nanmedian(nanmean(mseMerged{2,indEC}.Rvanilla(idx_OA_mse,channels,scales),3),2));
        y = squeeze(nanmedian(nanmean(mseMerged{2,indEC}.MSEVanilla(idx_OA_mse,channels,scales),3),2));
        l2 = scatter(x,y, 'filled', 'r'); lsline;
        [rl2,pl2] = corrcoef(x,y);
        title('Eyes closed: Scales 1-3');
        legend([l1, l2], {['Younger Adults: r=', num2str(round(rl1(2),2))], ['Older Adults: r=', num2str(round(rl2(2),2))]}, 'Location', 'NorthWest'); legend('boxoff');
        xlabel('(highpass R)/Vanilla(lowpass) R'); ylabel('MSE Vanilla')
   set(findall(gcf,'-property','FontSize'),'FontSize',18)
   pn.plotFolder = '/Users/kosciessa/Desktop/mntTardis/STSWD_Rest/WIP_eeg/A_MSE_CSD_multiVariant/C_figures/';
   figureName = 'F3_FastMSEreflectsPowerRatio';

    saveas(h, [pn.plotFolder, figureName], 'epsc');
    saveas(h, [pn.plotFolder, figureName], 'png');
     
    %% Supplementary: scale-wise link to PSD slopes co-occurs at scales of age differences
    
    RMat = [];
    for scales = 1:41
        x = squeeze(nanmedian(nanmean(mseMerged{1,indEO}.Rhp(idx_YA_mse,channels,scales),3),2))./...
            squeeze(nanmedian(nanmean(mseMerged{1,indEO}.Rvanilla(idx_YA_mse,channels,scales),3),2));
        y = squeeze(nanmedian(nanmean(mseMerged{1,indEO}.MSEVanilla(idx_YA_mse,channels,scales),3),2));
        [rl1,pl1] = corrcoef(x,y);
        RMat(scales) = rl1(2);
    end

    h = figure('units','normalized','position',[.1 .1 .3 .4]);
    hold on;
    plot(squeeze(nanmedian(nanmedian(mseMerged{2,indEO}.MSEVanilla(:, channels,:),2),1))-...
        squeeze(nanmedian(nanmedian(mseMerged{1,indEO}.MSEVanilla(:, channels,:),2),1)), 'r', 'LineWidth', 5)
    plot(RMat, 'k', 'LineWidth', 5)
    xlim([1 41]);
    title('R parameter bias covers scales of OA > YA MSE increase')
    set(gca, 'XTick', 1:4:41); set(gca, 'XTickLabel', round(mseMerged{2,indEO}.freq(1:4:41))); xlabel('Scale [Hz]'); ylabel('Estimate magnitude');
    legend({'MSE Difference OA-YA'; 'R bias correlation with MSE'}); legend('boxoff');
    set(findall(gcf,'-property','FontSize'),'FontSize',18)
    pn.plotFolder = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/A_MSE_CSD_multiVariant/C_figures/';
    figureName = 'F4_RBias_ageContrast';
    saveas(h, [pn.plotFolder, figureName], 'fig');
    saveas(h, [pn.plotFolder, figureName], 'epsc');
    saveas(h, [pn.plotFolder, figureName], 'png');

    %% Supplementary: cumulative power plots in both directions
    % This may aid the intuition of scale-wise variance reduction in the fixed r case.    
    
    figure;
    subplot(3,2,1); imagesc(squeeze(nanmean(grandavg_EO.powspctrm(idx_OA_fft,channels,:),2))); title('Power')
    subplot(3,2,3); imagesc(squeeze(nanmean(cumsum(grandavg_EO.powspctrm(idx_OA_fft,channels,:),3),2))); title('Cumulative power, lowpass case')
    subplot(3,2,4); imagesc(squeeze(nanmean(cumsum(grandavg_EO.powspctrm(idx_OA_fft,channels,:),3, 'reverse'),2))); title('Cumulative power, highpass case')
    subplot(3,2,5); imagesc(squeeze(nanmean(grandavg_EO.powspctrm(idx_OA_fft,channels,:),2))./...
        squeeze(nanmean(cumsum(grandavg_EO.powspctrm(idx_OA_fft,channels,:),3),2))); title('Relative power to cumulate, lowpass case')
    subplot(3,2,6); imagesc(squeeze(nanmean(grandavg_EO.powspctrm(idx_OA_fft,channels,:),2))./...
        squeeze(nanmean(cumsum(grandavg_EO.powspctrm(idx_OA_fft,channels,:),3, 'reverse'),2))); title('Relative power to cumulate, highpass case')
    
    figure;
    subplot(3,2,1); imagesc(squeeze(nanmean(grandavg_EO.powspctrm(idx_OA_fft,channels,:),2))); title('Power')
    subplot(3,2,3); imagesc(zscore(squeeze(nanmean(cumsum(grandavg_EO.powspctrm(idx_OA_fft,channels,:),3),2)),[],2)); title('Cumulative power, lowpass case')
    subplot(3,2,4); imagesc(zscore(squeeze(nanmean(cumsum(grandavg_EO.powspctrm(idx_OA_fft,channels,:),3, 'reverse'),2)),[],2)); title('Cumulative power, highpass case')
    subplot(3,2,5); imagesc(squeeze(nanmean(grandavg_EO.powspctrm(idx_OA_fft,channels,:),2))./...
        squeeze(nanmean(cumsum(grandavg_EO.powspctrm(idx_OA_fft,channels,:),3),2))); title('Relative power to cumulate, lowpass case')
    subplot(3,2,6); imagesc(squeeze(nanmean(grandavg_EO.powspctrm(idx_OA_fft,channels,:),2))./...
        squeeze(nanmean(cumsum(grandavg_EO.powspctrm(idx_OA_fft,channels,:),3, 'reverse'),2))); title('Relative power to cumulate, highpass case')

    %% Supplemental: probe correlation of PSD slope with continuous age 

    % load demographics overview

    load('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/demographics/B_data/Questionnaire_STSWD_NoPilot_Clean.mat')

    idx_YA_demo = find(ismember(cell2mat(C(:,1)), str2double(IDs{1})));
    idx_OA_demo = find(ismember(cell2mat(C(:,1)), str2double(IDs{2})));

    channels = 44:60;

    h = figure('units','normalized','position',[.1 .1 .35 .4]);
    hold on;
        x = squeeze(nanmedian(nanmean(mseMerged{1,2}.MSEVanilla(:,channels,1),3),2));
        y = Age(cat(1,idx_YA_demo));
        l1 = scatter(x,y, 'filled', 'k'); lsline;
        [rl1,pl1] = corrcoef(x,y);
        x = squeeze(nanmedian(nanmean(mseMerged{2,2}.MSEVanilla(:,channels,1),3),2));
        y = Age(cat(1,idx_OA_demo));
        l2 = scatter(x,y, 'filled', 'r'); lsline;
        [rl2,pl2] = corrcoef(x,y);
        legend([l1, l2], {['Younger Adults: r=', num2str(round(rl1(2),2))], ...
            ['Older Adults: r=', num2str(round(rl2(2),2))]}, 'Location', 'NorthWest'); legend('boxoff');
        xlabel('Vanilla MSE, Scale 1'); ylabel('PSD Slope 2-64 Hz (excl. 7-13 Hz)');
        title('Fine-scale entropy is not associcated with continuous age')
        set(findall(gcf,'-property','FontSize'),'FontSize',18)
    