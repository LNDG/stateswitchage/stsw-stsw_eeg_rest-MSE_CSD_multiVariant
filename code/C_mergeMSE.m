%% merge MSE and R estimates for all versions
 
pn.dataPath = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/A_MSE_CSD_multiVariant/B_data/C_MSE_Output_v1/';

IDs{1} = {'1117';'1118';'1120';'1124';'1126';'1131';'1132';'1135';'1136';'1138';...
    '1144';'1151';'1158';'1160';'1163';'1164';'1167';'1169';'1172';'1173';...
    '1178';'1182';'1215';'1216';'1219';'1221';'1223';'1227';'1228';...
    '1233';'1234';'1237';'1239';'1240';'1243';'1245';'1247';'1250';'1252';...
    '1257';'1261';'1265';'1266';'1268';'1270';'1276';'1281'};

IDs{2} = {'2104';'2107';'2108';'2112';'2118';'2120';'2121';'2123';'2125';'2129';...
    '2130';'2131';'2132';'2133';'2134';'2135';'2139';'2140';'2145';'2147';...
    '2149';'2157';'2160';'2202';'2203';'2205';'2206';'2209';'2210';...
    '2211';'2213';'2214';'2215';'2216';'2217';'2219';'2222';'2224';'2226';...
    '2227';'2236';'2237';'2238';'2241';'2244';'2246';'2248';'2250';'2251';...
    '2252';'2258';'2261'};

conds = {'EC'; 'EO'};

mseMerged = cell(1,1);
for indGroup = 1:2
    for indID = 1:numel(IDs{indGroup})
        for indCond = 1:2
            try
            curID = IDs{indGroup}{indID};
            % vanilla
            load([pn.dataPath, curID, '_',conds{indCond},'_MSE_OUT_vanilla.mat']);
            mseMerged{indGroup,indCond}.MSEVanilla(indID,:,:) = mse.sampen(1:60,:);
            mseMerged{indGroup,indCond}.Rvanilla(indID,:,:) = squeeze(mse.r(1:60,:));
            mseMerged{indGroup,indCond}.Rlog10Vanilla(indID,:,:) = log10(squeeze(mse.r(1:60,:)));
            % pointavg
            load([pn.dataPath, curID, '_',conds{indCond},'_MSE_OUT_pointavg.mat']);
            mseMerged{indGroup,indCond}.MSEPointavg(indID,:,:) = mse.sampen(1:60,:);
            mseMerged{indGroup,indCond}.Rpointavg(indID,:,:) = squeeze(mse.r(1:60,:));
            mseMerged{indGroup,indCond}.Rlog10Pointavg(indID,:,:) = log10(squeeze(mse.r(1:60,:)));
            % filtskip (LP)
            load([pn.dataPath, curID, '_',conds{indCond},'_MSE_OUT_filtskip.mat']);
            mseMerged{indGroup,indCond}.MSEfiltskip(indID,:,:) = mse.sampen(1:60,:);
            mseMerged{indGroup,indCond}.Rfiltskip(indID,:,:) = squeeze(mse.r(1:60,:));
            mseMerged{indGroup,indCond}.Rlog10filtskip(indID,:,:) = log10(squeeze(mse.r(1:60,:)));
            % filtskip (HP)
            load([pn.dataPath, curID, '_',conds{indCond},'_MSE_OUT_hp_v2.mat']);
            % spline interpolate values to 42 scales
            for indChan = 1:60
                x = 1:3:42;
                y = mse.sampen(indChan,:);
                xq = 1:1:42;
                yq = interp1(x,y,xq,'spline');
                mseMerged{indGroup,indCond}.MSEhp(indID,indChan,:) = yq;
                y = mse.r(indChan,:);
                yq = interp1(x,y,xq,'spline');
                mseMerged{indGroup,indCond}.Rhp(indID,indChan,:) = yq;
                mseMerged{indGroup,indCond}.Rlog10hp(indID,indChan,:) = log10(yq);
            end
            % filtskip (BP)
            load([pn.dataPath, curID, '_',conds{indCond},'_MSE_OUT_bp_v3.mat']);
            mseMerged{indGroup,indCond}.MSEbp(indID,:,:) = mse.sampen(1:60,:);
            mseMerged{indGroup,indCond}.Rbp(indID,:,:) = squeeze(mse.r(1:60,:));
            mseMerged{indGroup,indCond}.Rlog10bp(indID,:,:) = log10(squeeze(mse.r(1:60,:)));
            % convert into powerRatios
            mseMerged{indGroup,indCond}.LPRatio(indID,:,:) = mseMerged{indGroup,indCond}.Rfiltskip(indID,:,:)./mseMerged{indGroup,indCond}.Rvanilla(indID,:,:);
            mseMerged{indGroup,indCond}.HPRatio(indID,:,:) = mseMerged{indGroup,indCond}.Rhp(indID,:,:)./mseMerged{indGroup,indCond}.Rvanilla(indID,:,:);
            % add info
            mseMerged{indGroup,indCond}.dimord = 'subj_chan_freq';
            mseMerged{indGroup,indCond}.label = mse.label(1:60);
            mseMerged{indGroup,indCond}.timescales = mse.timescales;
            mseMerged{indGroup,indCond}.freq = (1./[1:42])*250;
            mseMerged{indGroup,indCond}.time = mse.time;
            catch
                disp([curID, ' ',conds{indCond}, ' skipped']);
                continue
            end
        end
    end
end

save('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/A_MSE_CSD_multiVariant/B_data/E_mseMerged.mat', 'mseMerged', 'IDs')
