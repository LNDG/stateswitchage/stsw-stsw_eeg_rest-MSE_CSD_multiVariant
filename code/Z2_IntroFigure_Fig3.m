%% Create a summary figure of the age-dependent spectra and their reflection in 'Original' MSE scales

load('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/A_MSE_CSD_multiVariant/B_data/E_mseMerged.mat', 'mseMerged', 'IDs')

% add convertPtoExponential
addpath('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/A_MSE_CSD_multiVariant/T_tools/')

pn.shadedError = ['/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/A_MSE_CSD_multiVariant/T_tools/shadedErrorBar']; addpath(pn.shadedError);

indCond = 2; channels = 1:60;

%% Plot PSD spectra including slope fits

load('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/B_FFT/B_data/B_FFT_grandavg_individual.mat')

commonYA = info.IDs_all(ismember(info.IDs_all, IDs{1}));
commonOA = info.IDs_all(ismember(info.IDs_all, IDs{2}));

idx_YA_fft = ismember(info.IDs_all, commonYA);
idx_OA_fft = ismember(info.IDs_all, commonOA);
idx_YA_mse = ismember(IDs{1}, commonYA);
idx_OA_mse = ismember(IDs{2}, commonOA);

pn.shadedError = ['/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/A_MSE_CSD_multiVariant/T_tools/shadedErrorBar']; addpath(pn.shadedError);

h = figure('units','normalized','position',[.1 .1 .15 .35]);
set(gcf,'renderer','Painters')
subplot(3,1,[1,2]); cla; hold on;
    % plot area under spectrum
    x = log10(grandavg_EO.freq);
    curAverage = nanmean(log10(grandavg_EO.powspctrm(idx_YA_fft,:,:)),2);
    y1=squeeze(nanmean(curAverage,1));
    hold on
    H=area(x(grandavg_EO.freq<= 10),y1(grandavg_EO.freq<= 10),-9.5, 'FaceColor',[48/255, 111/255, 186/255]);
    H1=area(x(grandavg_EO.freq>= 10),y1(grandavg_EO.freq>= 10),-9.5, 'FaceColor',[175/255, 36/255, 24/255]);
    H2=area([x(grandavg_EO.freq> 9 & grandavg_EO.freq< 11)],[y1(grandavg_EO.freq> 9 & grandavg_EO.freq< 11)],-9.5, 'FaceColor',[1 .7 0]);
    % plot actual spectrum
    l1 = plot(log10(grandavg_EO.freq),squeeze(nanmean(curAverage,1)), 'Color', 'k', 'LineWidth', 5);
    %set(gca, 'XScale', 'log')
    xlim([.3, 1.8])
    set(gca, 'XTick',log10(grandavg_EC.freq(3:8:end)));    
    set(gca, 'XTickLabels',round(grandavg_EC.freq(3:8:end)));
    %set(gca, 'XTickLabels',[]);
    xlabel('Frequency [Hz, log-scaled]'); ylabel({'Power ~ variance [log-scaled]'})
    set(gca, 'YTick', []);
    set(gca, 'XAxisLocation', 'top')
    set(gca, 'YAxisLocation', 'left')
    set(gca, 'XDir','reverse')
    
    
%% second plot of schematic influence on MSE estimates

Fs = 250;
scaleFactors = 1:42;
LP = (1./scaleFactors).*(Fs/2);
HP = (1./(scaleFactors+1)).*(Fs/2);

subplot(3,1,3); cla; hold on;

    plot(scaleFactors, LP, 'Color', 'w');
    H=area(12:14,repmat(1.2,1,3),-9.5, 'FaceColor',[.8 .8 .8]);
    H.EdgeAlpha = 0;

    line([13 13], [0 1.2], 'LineWidth', 5, 'LineStyle', ':', 'Color', [0 0 0])

    ylim([0 1.2])
    xlim([1 23])
    
    %set(gca, 'XScale', 'log')

        scales = 1:42;
        set(gca, 'XTick',[1, 13, 23]);
        tmp_meas = [round(LP(get(gca, 'XTick')),0); round(scales(get(gca, 'XTick')),0)];
        xLabels = [];
        for indScale = 1:size(tmp_meas,2)
            xLabels{indScale} = [num2str(tmp_meas(1,indScale)), ' Hz\newline(scale ',num2str(tmp_meas(2,indScale)), ')'];
        end; clear tmp_meas;
        set(gca, 'XTickLabels', xLabels);
        set(gca, 'XDir','normal')
        xlabel({'Time scale [Hz; log-scaled]'}); ylabel({'Sample Entropy'})

set(findall(gcf,'-property','FontSize'),'FontSize',20);


%% save Figure
    
pn.plotFolder = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/A_MSE_CSD_multiVariant/C_figures/';
figureName = 'Z_IntroSchematic_Fig3';

saveas(h, [pn.plotFolder, figureName], 'fig');
saveas(h, [pn.plotFolder, figureName], 'epsc');
saveas(h, [pn.plotFolder, figureName], 'png');