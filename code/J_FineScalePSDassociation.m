%% Highlight the association between fast-scale entropy and PSD slope

    pn.root = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/A_MSE_CSD_multiVariant/';
    load([pn.root, 'B_data/E_mseMerged.mat'], 'mseMerged', 'IDs')
    addpath([pn.root, 'T_tools/']); % add convertPtoExponential
    addpath([pn.root, 'T_tools/shadedErrorBar']);
    
    indCond = 2; channels = 1:60;
    load('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/B_FFT/B_data/B_FFT_grandavg_individual.mat')
    commonYA = info.IDs_all(ismember(info.IDs_all, IDs{1}));
    commonOA = info.IDs_all(ismember(info.IDs_all, IDs{2}));
    idx_YA_fft = ismember(info.IDs_all, commonYA);
    idx_OA_fft = ismember(info.IDs_all, commonOA);
    idx_YA_mse = ismember(IDs{1}, commonYA);
    idx_OA_mse = ismember(IDs{2}, commonOA);
    
    %load('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/B_FFT/B_data/D_PSDslopes.mat')
    load('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/B_FFT/B_data/D_PSDslopes_loglog.mat')

%% FIGURE 7A: fine-scale MSE-PSD slope correlation: scatter

    h = figure('units','normalized','position',[.1 .1 .2 .3]);
    channels = 43:60; % parieto-occipital channels

    cla; hold on;
    x = squeeze(nanmean(nanmean(mseMerged{1,2}.MSEVanilla(idx_YA_mse,channels,1:3),3),2));
    y = squeeze(nanmean(linFit_2_30_EO(idx_YA_fft, channels),2));
    l1 = scatter(x,y, 50,'filled', 'r'); y1_ls = polyval(polyfit(x,y,1),x); y1_ls = plot(x, y1_ls, 'Color', 'r', 'LineWidth', 3);
    [rl1,pl1] = corrcoef(x,y); pval1 = []; pval1 = convertPtoExponential(pl1(2));
    % add OA
    x = squeeze(nanmean(nanmean(mseMerged{2,2}.MSEVanilla(idx_OA_mse,channels,1:3),3),2));
    y = squeeze(nanmean(linFit_2_30_EO(idx_OA_fft, channels),2));
    l2 = scatter(x,y, 50,'filled', 'k'); y2_ls = polyval(polyfit(x,y,1),x); y2_ls = plot(x, y2_ls, 'Color', 'k', 'LineWidth', 3);
    [rl2,pl2] = corrcoef(x,y); pval2 = []; pval2 = convertPtoExponential(pl2(2));
    lgd1 = legend([y1_ls, y2_ls], {['Younger Adults: r=', num2str(round(rl1(2),2)), ', p=',pval1{1}], ...
            ['Older Adults: r=', num2str(round(rl2(2),2)), ', p=',pval2{1}]}, 'Location', 'SouthWest'); legend('boxoff');
       xlabel('Vanilla MSE, Scales 1:3 (>50 Hz)'); ylabel('PSD Slope 2-64 Hz (excl. 7-13 Hz)');
    title({'Individual fine-scale Original entropy','captures PSD slopes'})
    xlim([0 1]); ylim([-2, .4])
    set(findall(gcf,'-property','FontSize'),'FontSize',22)

    % reset legend Font sizes
    set(lgd1,'FontSize',16);

    pn.plotFolder = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/A_MSE_CSD_multiVariant/C_figures/';
    figureName = 'F3_FastScalePSDassociation';
    saveas(h, [pn.plotFolder, figureName], 'fig');
    saveas(h, [pn.plotFolder, figureName], 'epsc');
    saveas(h, [pn.plotFolder, figureName], 'png');
    
%% FIGURE 7B: fine-scale MSE-PSD slope correlation; superimpose correlation coefficients by time scale

    h = figure('units','normalized','position',[.1 .1 .3 .3]);

    channels = 43:60; % parieto-occipital channels

    labels = {'MSEVanilla'; 'MSEfiltskip'; 'MSEhp'; 'MSEbp'};
    scales = {[1]; [5:10]};

    labelTitle = {'Original'; 'Low-pass'; 'High-pass'; 'Band-pass'};
    scaleTitle = {'1'; '5-10'};

    for indLabel = 1:3
        %subplot(1,3,indLabel); cla; hold on;
            hold on;
            for indScale = 1:41
                %cla; hold on;
                x = squeeze(nanmean(nanmean(mseMerged{1,indCond}.(labels{indLabel})(idx_YA_mse,channels,indScale),3),2));
                y = squeeze(nanmean(linFit_2_30_EO(idx_YA_fft, channels),2));
                [rl1,pl1] = corrcoef(x,y);
                Rmat(1,indScale) = rl1(2);
                Pmat(1,indScale) = pl1(2);
                % add OA
                x = squeeze(nanmean(nanmean(mseMerged{2,indCond}.(labels{indLabel})(idx_OA_mse,channels,indScale),3),2));
                y = squeeze(nanmean(linFit_2_30_EO(idx_OA_fft, channels),2));
                [rl2,pl2] = corrcoef(x,y);
                Rmat(2,indScale) = rl2(2);
                Pmat(2,indScale) = pl2(2);
            end
            if indLabel == 1
                lineStyle = '-';
            elseif indLabel == 2
                lineStyle = '--';
            elseif indLabel == 3
                lineStyle = ':';
            end
            ll{indLabel,1} = plot(Rmat(1,:), 'LineWidth', 3, 'Color', 'k', 'LineStyle', lineStyle)
            ll{indLabel,2} = plot(Rmat(2,:), 'LineWidth', 3, 'Color', 'r', 'LineStyle', lineStyle)
            xlim([1 41]); ylim([-1 1])
            set(gca, 'XScale', 'log')
            scales = 1:41;
            set(gca, 'XTick',round(logspace(log10(1), log10(41), 5)));
            tmp_meas = [round(mseMerged{1,indCond}.freq(get(gca, 'XTick')),0); round(scales(get(gca, 'XTick')),0)];
            xLabels = [];
            for indScale = 1:size(tmp_meas,2)
                xLabels{indScale} = [num2str(tmp_meas(1,indScale)), ' Hz\newline(scale ',num2str(tmp_meas(2,indScale)), ')'];
            end; clear tmp_meas;
            set(gca, 'XTickLabels', xLabels);
    %             set(gca, 'XDir','reverse')
    end
    xlabel('Time scale [Hz; log-scaled]'); ylabel('Correlation coefficient (r)')
    title({'Correlation with PSD slope depends on'; 'similarity bounds & spectral content'})
    lgd1 = legend([ll{1,1}, ll{2,1}, ll{3,1}], ...
        {'YA: Original', 'YA: low-pass', 'YA: high-pass'}, ...
        'location', 'SouthWest'); legend('boxoff');
    ah1=axes('position',get(gca,'position'),'visible','off');

    lgd2 = legend(ah1,[ll{1,2},ll{2,2},ll{3,2}], ...
        {'OA: Original', 'OA: low-pass', 'OA: high-pass'}, ...
        'location', 'South'); legend('boxoff');
    set(findall(gcf,'-property','FontSize'),'FontSize',22)

    set(lgd1,'FontSize',18)
    set(lgd2,'FontSize',18)

    pn.plotFolder = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/A_MSE_CSD_multiVariant/C_figures/';
    figureName = 'F4_FastScalePSDassociation_byMethod_CorrelationOnly';
    saveas(h, [pn.plotFolder, figureName], 'fig');
    saveas(h, [pn.plotFolder, figureName], 'epsc');
    saveas(h, [pn.plotFolder, figureName], 'png');
    
%% partial correlation of fine-scale entropy to age, while controlling for PSD slopes

    % load MSE topography with different filter settings

    load([pn.root, 'B_data/E2_CBPA_Age_Condition_v2.mat'])

    % Note that time scales are inverted between statistical output matrix and
    % original data matrices!

    % extract individual values from statistical cluster (Vanilla version)

    FastMSEVanillaCluster = find(max(stat{4,1}.mask(:,39:41),[],2));
    SlowMSEVanillaCluster = find(max(stat{4,1}.mask(:,1:3),[],2));

    load([pn.root, 'B_data/E_mseMerged.mat'])

    % calculate R squared : t squared divided by (t squared +df)
    % identical to correlation: corrcoef([x1;x2], [repmat(1,numel(x1),1); repmat(2,numel(x2),1)])

    x1 = nanmean(nanmean(mseMerged{1,2}.MSEVanilla(:,FastMSEVanillaCluster,1:3),3),2);
    x2 = nanmean(nanmean(mseMerged{2,2}.MSEVanilla(:,FastMSEVanillaCluster,1:3),3),2);
    [h,p,ci,stats] = ttest2(x1,x2);
    rsquare = (stats.tstat^2)./(stats.tstat^2 + stats.df);
    rVanillaFast = sqrt(rsquare);

    % partial PSD slopes from MSE across younger and older adults

    y1 = squeeze(nanmean(linFit_2_30_EO(idx_YA_fft, FastMSEVanillaCluster),2));
    y2 = squeeze(nanmean(linFit_2_30_EO(idx_OA_fft, FastMSEVanillaCluster),2));

    % calculate partial correlation between x: entropy, y: PSDslopes and; z: age category status (1 vs.2)

    [rho, p] = partialcorr([[x1;x2],[y1;y2],[repmat(1,numel(x1),1); repmat(2,numel(x2),1)]]);

    % entropy & age, control for slope
    
    rho(1,3)
    p(1,3)
    
    rho(2,3)
    p(2,3)
    
    % PSD & MSE correlated .8, controlling for age
    % PSD and age correlated .4, controlling for MSE

%% Supplementary Figure: scatters for different Methods
    % rows: methods: 'Original', Low-pass, High-pass
    % columns: fast- vs. slow-scale
    % i.e., 3x2 figure

    labels = {'MSEVanilla'; 'MSEfiltskip'; 'MSEhp'};
    scales = {[1:3]; [32:42]};

    labelTitle = {'Original'; 'Low-pass'; 'High-pass'};
    scaleTitle = {'1-3 (> 50 Hz; Fine)'; '32-42 (< 8 Hz; Coarse)'};

    h = figure('units','normalized','position',[.1 .1 .5 .4]);
    for indScale = 1:2
        for indMethod = 1:3
            subplot(2,3,(indScale-1)*3+indMethod);
            channels = 43:60; % parieto-occipital channels
            cla; hold on;
            x = squeeze(nanmean(nanmean(mseMerged{1,2}.(labels{indMethod})(idx_YA_mse,channels,scales{indScale}),3),2));
            y = squeeze(nanmean(linFit_2_30_EO(idx_YA_fft, channels),2));
            l1 = scatter(x,y, 50,'filled', 'r'); y1_ls = polyval(polyfit(x,y,1),x); y1_ls = plot(x, y1_ls, 'Color', 'r', 'LineWidth', 3);
            [rl1,pl1] = corrcoef(x,y); pval1 = []; pval1 = convertPtoExponential(pl1(2));
            % add OA
            x = squeeze(nanmean(nanmean(mseMerged{2,2}.(labels{indMethod})(idx_OA_mse,channels,scales{indScale}),3),2));
            y = squeeze(nanmean(linFit_2_30_EO(idx_OA_fft, channels),2));
            l2 = scatter(x,y, 50,'filled', 'k'); y2_ls = polyval(polyfit(x,y,1),x); y2_ls = plot(x, y2_ls, 'Color', 'k', 'LineWidth', 3);
            [rl2,pl2] = corrcoef(x,y); pval2 = []; pval2 = convertPtoExponential(pl2(2));
            loc = 'South';
            lgd1 = legend([y1_ls, y2_ls], {['Younger Adults: r=', num2str(round(rl1(2),2)), ', p=',pval1{1}], ...
                    ['Older Adults: r=', num2str(round(rl2(2),2)), ', p=',pval2{1}]}, 'Location', loc); legend('boxoff');
            xlabel([labelTitle{indMethod}, ' MSE : Scales ', scaleTitle{indScale}]); 
            if indMethod == 1 
               ylabel('PSD Slope 2-64 Hz (excl. 7-13 Hz)');
            else
                ylabel('PSD Slope');
            end
            title({'Relation between MSE and PSD slopes:'; [labelTitle{indMethod}, ' MSE : Scales ', scaleTitle{indScale}]})
            %xlim([0 1]);
            ylim([-2.5, .2])
            yticks([-2, -1, 0])
        end
    end
    set(findall(gcf,'-property','FontSize'),'FontSize',14)

    pn.plotFolder = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/A_MSE_CSD_multiVariant/C_figures/';
    figureName = 'F4_FastScalePSDassociation_byMethod';
    saveas(h, [pn.plotFolder, figureName], 'fig');
    saveas(h, [pn.plotFolder, figureName], 'epsc');
    saveas(h, [pn.plotFolder, figureName], 'png');

%% Supplementary: Ratio of R parameters (i.e., high-to-low frequency power) recovers fast Vanilla MSE

    channels = 43:60; % parieto-occipital channels

    subplot(2,2,2); cla; hold on;
    scales = 1:3;
    hold on;
    x = squeeze(nanmean(nanmean(mseMerged{1,2}.Rhp(idx_YA_mse,channels,scales)./...
        mseMerged{1,2}.Rvanilla(idx_YA_mse,channels,scales),3),2));
    y = squeeze(nanmean(nanmean(mseMerged{1,2}.MSEVanilla(idx_YA_mse,channels,scales),3),2));
    l1 = scatter(x,y, 50, 'filled', 'r'); y1_ls = polyval(polyfit(x,y,1),x); y1_ls = plot(x, y1_ls, 'Color', 'r', 'LineWidth', 3);
    [rl1,pl1] = corrcoef(x,y);pval1 = []; pval1 = convertPtoExponential(pl1(2));
    % add OA
    x = squeeze(nanmean(nanmean(mseMerged{2,2}.Rhp(idx_OA_mse,channels,scales)./...
        mseMerged{2,2}.Rvanilla(idx_OA_mse,channels, scales),3),2));
    y = squeeze(nanmean(nanmean(mseMerged{2,2}.MSEVanilla(idx_OA_mse,channels,scales),3),2));
    l2 = scatter(x,y, 50,'filled', 'k'); y2_ls = polyval(polyfit(x,y,1),x); y2_ls = plot(x, y2_ls, 'Color', 'k', 'LineWidth', 3);
    [rl2,pl2] = corrcoef(x,y);pval2 = []; pval2 = convertPtoExponential(pl2(2));
    title({'Fine MSE reflects ratio of','high-frequency and broadband variance'}); 
    lgd3 = legend([y1_ls, y2_ls], {['Younger Adults: r=', num2str(round(rl1(2),2)),' p=',pval1{1}], ['Older Adults: r=', num2str(round(rl2(2),2)),' p=',pval2{1}]}, 'Location', 'SouthWest'); legend('boxoff');
    xlabel('Ratio: highpass R/lowpass (Vanilla) R'); ylabel('Vanilla MSE, Scales 1:3 (>50 Hz)')
    xlim([0 .4]); ylim([0 1]);

%% Supplementary: R parameter bias covers scales of OA > YA MSE increase

    load(['/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/A_MSE_CSD_multiVariant/B_data/E2_CBPA_Age_Condition_v2.mat'], 'stat', 'methodLabels')

    channels = find(nanmean(stat{4,1}.mask(:,find(stat{4,1}.freq>50)),2)>0); % channels of age difference

    RMat = []; PMat = [];
    for scales = 1:41
        x = squeeze(nanmean(nanmean(mseMerged{1,indCond}.Rhp(idx_YA_mse,channels,scales),3),2))./...
            squeeze(nanmean(nanmean(mseMerged{1,indCond}.Rvanilla(idx_YA_mse,channels,scales),3),2));
        y = squeeze(nanmean(nanmean(mseMerged{1,indCond}.MSEVanilla(idx_YA_mse,channels,scales),3),2));
        [rl1,pl1] = corrcoef(x,y);
        RMat(1,scales) = rl1(2);
        PMat(1,scales) = pl1(2);
    end
    for scales = 1:41
        x = squeeze(nanmean(nanmean(mseMerged{2,indCond}.Rhp(idx_OA_mse,channels,scales),3),2))./...
            squeeze(nanmean(nanmean(mseMerged{2,indCond}.Rvanilla(idx_OA_mse,channels,scales),3),2));
        y = squeeze(nanmean(nanmean(mseMerged{2,indCond}.MSEVanilla(idx_OA_mse,channels,scales),3),2));
        [rl1,pl1] = corrcoef(x,y);
        RMat(2,scales) = rl1(2);
        PMat(2,scales) = pl1(2);
    end

    figure; cla; hold on;
    l1 = plot(squeeze(nanmedian(nanmedian(mseMerged{2,indCond}.MSEVanilla(:, channels,:),2),1))-...
        squeeze(nanmedian(nanmedian(mseMerged{1,indCond}.MSEVanilla(:, channels,:),2),1)), 'Color', [.5 .5 .5], 'LineWidth', 3);
    l2 = plot(RMat(1,:), 'k', 'LineWidth', 3);
    l3 = plot(RMat(2,:), 'r', 'LineWidth', 3);
    xlim([1 41]);
    title({'R parameter association covers ','time scales of MSE age difference'})
    %set(gca, 'XTick', 1:5:41); set(gca, 'XTickLabel', round(mseMerged{2,indCond}.freq(1:5:41)));
    xlabel('Scale [Hz, scale]; log-scaled'); ylabel('Estimate magnitude');
    lgd4 = legend([l1, l2, l3], {'MSE Difference OA-YA'; 'YA: R ratio correlation with MSE'; 'OA: R ratio correlation with MSE'}, 'location', 'South'); legend('boxoff');
    set(gca, 'XScale', 'log')
    scales = 1:41;
    set(gca, 'XTick',round(logspace(log10(1), log10(41), 5)));
    tmp_meas = [round(mseMerged{1,indCond}.freq(get(gca, 'XTick')),0); round(scales(get(gca, 'XTick')),0)];
    xLabels = [];
    for indScale = 1:size(tmp_meas,2)
        xLabels{indScale} = [num2str(tmp_meas(1,indScale)), ' (',num2str(tmp_meas(2,indScale)),')'];
    end; clear tmp_meas;
    set(gca, 'XTickLabels', xLabels);
    ylim([-1 1])

    set(findall(gcf,'-property','FontSize'),'FontSize',20)