#!/bin/bash

# This script prepares tardis by compiling the necessary function in MATLAB.

ssh tardis # access tardis

# check and choose matlab version
module avail matlab
module load matlab/R2016b

# compile functions

matlab
pn.rootDir = '/home/mpib/kosciessa/STSWD_Rest/WIP_eeg/A_MSE_CSD_multiVariant/';
%% add fieldtrip toolbox
addpath([pn.rootDir, 'T_tools/fieldtrip-20170904/']); ft_defaults(); ft_compile_mex(true)
%% go to analysis directory containing .m-file
cd([pn.rootDir, 'A_scripts/B_MSE_tardis/'])
%% compile function and append dependencies
mcc -m B_runMSE.m -a '/home/mpib/kosciessa/STSWD_Rest/WIP_eeg/A_MSE_CSD_multiVariant/T_tools/mMSE/ft_entropyanalysis.m'
exit