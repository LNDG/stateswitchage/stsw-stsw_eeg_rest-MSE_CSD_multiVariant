%% Indicate relationship between scale-invariant r parameter and coarse-scale MSE

    pn.root = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/A_MSE_CSD_multiVariant/';
    load([pn.root, 'B_data/E_mseMerged.mat'], 'mseMerged', 'IDs')
    addpath([pn.root, 'T_tools/']); % add convertPtoExponential
    addpath([pn.root, 'T_tools/shadedErrorBar']);
    
    indCond = 2; channels = 1:60;
    load('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/B_FFT/B_data/B_FFT_grandavg_individual.mat')
    commonYA = info.IDs_all(ismember(info.IDs_all, IDs{1}));
    commonOA = info.IDs_all(ismember(info.IDs_all, IDs{2}));
    idx_YA_fft = ismember(info.IDs_all, commonYA);
    idx_OA_fft = ismember(info.IDs_all, commonOA);
    idx_YA_mse = ismember(IDs{1}, commonYA);
    idx_OA_mse = ismember(IDs{2}, commonOA);

%% FIGURE 6: coarse MSE: influence of high frequency power
   
    % extract channels from frontal entropy cluster

    load(['/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/A_MSE_CSD_multiVariant/B_data/E2_CBPA_Age_Condition_v2.mat'], 'stat', 'methodLabels')

    CoarseScaleChannels = find(nanmean(stat{4,1}.mask(:,find(stat{4,1}.freq<6)),2)>0);

   % plot scale-wise R vs global R by scale
   
   h = figure('units','normalized','position',[.1 .1 .5 .6]);
   set(gcf,'renderer','Painters')
   subplot(2,2,1); cla; hold on;
       x = squeeze(nanmean(nanmean(mseMerged{1,indCond}.Rvanilla(:,1:60,:),2),1)); l1 = plot(x, 'Color', 'r', 'LineStyle', ':', 'LineWidth', 5);
       x = squeeze(nanmean(nanmean(mseMerged{1,indCond}.Rpointavg(:,1:60,:),2),1)); l2 = plot(x, 'Color', 'r', 'LineStyle', '-', 'LineWidth', 5);
       %l1 = scatter(x,y, 'filled', 'k'); y1_ls = polyval(polyfit(x,y,1),x); y1_ls = plot(x, y1_ls, 'Color', 'k');
       %[rl1,pl1] = corrcoef(x,y);
       x = squeeze(nanmean(nanmean(mseMerged{2,indCond}.Rvanilla(:,1:60,:),2),1)); l3 = plot(x,'Color', 'k', 'LineStyle', ':', 'LineWidth', 5);
       x = squeeze(nanmean(nanmean(mseMerged{2,indCond}.Rpointavg(:,1:60,:),2),1)); l4 = plot(x,'Color', 'k', 'LineStyle', '-', 'LineWidth', 5);
       ylabel('Similarity bound (r x SD)'); xlabel('Time scale [Hz; log-scaled]')
       set(gca, 'XScale', 'log')
        scales = 1:41;
        set(gca, 'XTick',round(logspace(log10(1), log10(30), 6)));
        tmp_meas = [round(mseMerged{1,indCond}.freq(get(gca, 'XTick')),0); round(scales(get(gca, 'XTick')),0)];
        xLabels = [];
        for indScale = 1:size(tmp_meas,2)
            xLabels{indScale} = [num2str(tmp_meas(1,indScale)), ' Hz\newline(scale ',num2str(tmp_meas(2,indScale)), ')'];
        end; clear tmp_meas;
        set(gca, 'XTickLabels', xLabels);
        xl = get(gca,'XLabel'); xlFontSize = get(xl,'FontSize'); xAX = get(gca,'XAxis');
        %set(gca, 'XDir','reverse')
       lgd1 = legend([l1,l2,l3,l4], {'YA: global similarity bound'; 'YA: scale-wise similarity bound'; 'OA: global similarity bound'; 'OA: scale-wise similarity bound'}, 'location', 'SouthWest', 'FontSize', 18); legend('boxoff');
       xlim([1 41]);
       title({'Discrepancy between global and local bounds...';''})
   subplot(2,2,2); cla; hold on;
       x = squeeze(nanmean(nanmean(log10(grandavg_EO.powspctrm(idx_YA_fft,1:60,:)),2),1)); l2 = plot(grandavg_EC.freq,x,'Color', 'r', 'LineStyle', '-', 'LineWidth', 5);
       x = squeeze(nanmean(nanmean(log10(grandavg_EO.powspctrm(idx_OA_fft,1:60,:)),2),1)); l4 = plot(grandavg_EC.freq,x,'Color', 'k', 'LineStyle', '-', 'LineWidth', 5);
       x = repmat(squeeze(nanmean(nanmean(nanmean(log10(grandavg_EO.powspctrm(idx_YA_fft,1:60,:)),3),2),1)),1,41); l1 = plot(grandavg_EC.freq,x,'Color', 'r', 'LineStyle', ':', 'LineWidth', 5);
       x = repmat(squeeze(nanmean(nanmean(nanmean(log10(grandavg_EO.powspctrm(idx_OA_fft,1:60,:)),3),2),1)),1,41); l3 = plot(grandavg_EC.freq,x,'Color', 'k', 'LineStyle', ':', 'LineWidth', 5);
       set(gca, 'XScale', 'log')
    xlim([2, 64])
    set(gca, 'XTick',grandavg_EC.freq(3:6:end)); 
       lgd2 = legend([l1,l2,l3,l4], {'YA: mean power'; 'YA: frequency-specific power'; 'OA: mean power'; 'OA: frequency-specific power'}, 'location', 'SouthEast', 'FontSize', 15); legend('boxoff');
       ylabel('Power (log10)'); xlabel('Frequency (Hz; log-scaled)')
       xlim([2, 64]);
       set(gca, 'XDir','reverse')
       title({'... reflects spectral shape'; ''})

   % Let's add a nice illustration of the imbalance between scale-wise
   % variance and R
   
   subplot(2,2,3); cla; hold on;
       x = squeeze(nanmean(nanmean(mseMerged{1,indCond}.Rpointavg(:,CoarseScaleChannels,find(mseMerged{1,2}.freq<8))./mseMerged{1,indCond}.Rvanilla(:,CoarseScaleChannels,find(mseMerged{1,2}.freq<8)),3),2));
       y = squeeze(nanmean(nanmean(mseMerged{1,indCond}.MSEPointavg(:,CoarseScaleChannels,find(mseMerged{1,2}.freq<8))-mseMerged{1,indCond}.MSEVanilla(:,CoarseScaleChannels,find(mseMerged{1,2}.freq<8)),3),2));
       l1 = scatter(x,y, 100,'filled', 'r'); y1_ls = polyval(polyfit(x,y,1),x); y1_ls = plot(x, y1_ls, 'Color', 'r', 'LineWidth', 3);
       [rl1,pl1] = corrcoef(x,y); pval1 = []; pval1 = convertPtoExponential(pl1(2));
       x = squeeze(nanmean(nanmean(mseMerged{2,indCond}.Rpointavg(:,CoarseScaleChannels,find(mseMerged{1,2}.freq<8))./mseMerged{2,indCond}.Rvanilla(:,CoarseScaleChannels,find(mseMerged{1,2}.freq<8)),3),2));
       y = squeeze(nanmean(nanmean(mseMerged{2,indCond}.MSEPointavg(:,CoarseScaleChannels,find(mseMerged{1,2}.freq<8))-mseMerged{2,indCond}.MSEVanilla(:,CoarseScaleChannels,find(mseMerged{1,2}.freq<8)),3),2));
       %title('Interindividual shifts in R and MSE')
       title({'Liberality of similarity bounds ';'relates to coarse-scale (<8 Hz) entropy'})
       xlabel('Ratio of similarity bounds: Rescaled vs. global bound'); ylabel('Difference in MSE: Rescaled vs. global bound');
       l2 = scatter(x,y, 100,'filled', 'k'); y2_ls = polyval(polyfit(x,y,1),x); y2_ls = plot(x, y2_ls, 'Color', 'k', 'LineWidth', 3);
       [rl2,pl2] = corrcoef(x,y);pval2 = []; pval2 = convertPtoExponential(pl2(2));
       legend([y1_ls, y2_ls], {['Younger Adults: r=', num2str(round(rl1(2),2)), ', p=',pval1{1}], ...
        ['Older Adults: r=', num2str(round(rl2(2),2)), ', p=',pval2{1}]}, 'Location', 'NorthEast'); legend('boxoff');
       set(findall(gcf,'-property','FontSize'),'FontSize',18)
       xlim([0.25,1]); ylim([0 .9])
   subplot(2,2,4); hold on;
       x = squeeze(nanmean(nanmean(log10(grandavg_EO.powspctrm(idx_YA_fft,CoarseScaleChannels,33:41)),3),2));
       y = squeeze(nanmean(nanmean(mseMerged{1,indCond}.MSEVanilla(:,CoarseScaleChannels,find(mseMerged{1,2}.freq<8)),3),2));
       l1 = scatter(x,y, 100, 'filled', 'r'); y1_ls = polyval(polyfit(x,y,1),x); y1_ls = plot(x, y1_ls, 'Color', 'r', 'LineWidth', 3);
       [rl1,pl1] = corrcoef(x,y);pval1 = []; pval1 = convertPtoExponential(pl1(2));
       x = squeeze(nanmean(nanmean(log10(grandavg_EO.powspctrm(idx_OA_fft,CoarseScaleChannels,33:41)),3),2));
       y = squeeze(nanmean(nanmean(mseMerged{2,indCond}.MSEVanilla(:,CoarseScaleChannels,find(mseMerged{1,2}.freq<8)),3),2));
       l2 = scatter(x,y, 100,'filled', 'k'); y2_ls = polyval(polyfit(x,y,1),x); y2_ls = plot(x, y2_ls, 'Color', 'k', 'LineWidth', 3);
       [rl2,pl2] = corrcoef(x,y);pval2 = []; pval2 = convertPtoExponential(pl2(2));
       legend([y1_ls, y2_ls], {['Younger Adults: r=', num2str(round(rl1(2),2)), ', p=',pval1{1}], ...
        ['Older Adults: r=', num2str(round(rl2(2),2)), ', p=',pval2{1}]}, 'Location', 'NorthEast'); legend('boxoff');
       title({'Interindividual differences in high frequency power';'relate to coarse-scale "Original" MSE'})
       xlabel('Power 32-64 Hz (log10)'); ylabel('"Original" coarse-scale entropy');
       set(findall(gcf,'-property','FontSize'),'FontSize',18)
       ylim([0 1])
   
   pn.plotFolder = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/A_MSE_CSD_multiVariant/C_figures/';
   figureName = 'F_LowFreqShiftEntropyR';
   saveas(h, [pn.plotFolder, figureName], 'fig');
   saveas(h, [pn.plotFolder, figureName], 'epsc');
   saveas(h, [pn.plotFolder, figureName], 'png');
   
   %% Supplementary: show absence of correlation with rescaled R params
   
  figure; hold on;
       x = squeeze(nanmean(nanmean(log10(grandavg_EO.powspctrm(idx_YA_fft,CoarseScaleChannels,33:41)),3),2)); % 32-64 Hz power
       y = squeeze(nanmean(nanmean(mseMerged{1,2}.MSEfiltskip(:,CoarseScaleChannels,find(mseMerged{1,2}.freq<8)),3),2));
       l1 = scatter(x,y, 'filled', 'k'); y1_ls = polyval(polyfit(x,y,1),x); y1_ls = plot(x, y1_ls, 'Color', 'k');
       [rl1,pl1] = corrcoef(x,y);
       x = squeeze(nanmean(nanmean(log10(grandavg_EO.powspctrm(idx_OA_fft,CoarseScaleChannels,33:41)),3),2));
       y = squeeze(nanmean(nanmean(mseMerged{2,2}.MSEfiltskip(:,CoarseScaleChannels,find(mseMerged{1,2}.freq<8)),3),2));
       l2 = scatter(x,y, 'filled', 'r'); y2_ls = polyval(polyfit(x,y,1),x); y2_ls = plot(x, y2_ls, 'Color', 'r');
       [rl2,pl2] = corrcoef(x,y);
       legend([y1_ls, y2_ls], {['Younger Adults: r=', num2str(round(rl1(2),2)), ', p=',num2str(round(pl1(2),3))], ...
        ['Older Adults: r=', num2str(round(rl2(2),2)), ', p=',num2str(round(pl2(2),3))]}, 'Location', 'NorthEast'); legend('boxoff');
       title('High frequency power predicts coarse Vanilla MSE')
       xlabel('Power 32-64 Hz (log10)'); ylabel('Vanilla MSE');
       set(findall(gcf,'-property','FontSize'),'FontSize',18)
       ylim([0 1])
   
   % t-test of age difference in shift
       
       R_YA = squeeze(nanmean(nanmean(mseMerged{1,2}.Rfiltskip(:,CoarseScaleChannels,find(mseMerged{1,2}.freq<8)),3),2))-...
                squeeze(nanmean(nanmean(mseMerged{1,2}.Rvanilla(:,CoarseScaleChannels,find(mseMerged{1,2}.freq<8)),3),2));
       MSE_YA = squeeze(nanmedian(nanmean(mseMerged{1,2}.MSEfiltskip(:,CoarseScaleChannels,find(mseMerged{1,2}.freq<8)),3),2))-...
                squeeze(nanmedian(nanmean(mseMerged{1,2}.MSEVanilla(:,CoarseScaleChannels,find(mseMerged{1,2}.freq<8)),3),2));
       R_OA = squeeze(nanmean(nanmean(mseMerged{2,2}.Rfiltskip(:,CoarseScaleChannels,find(mseMerged{1,2}.freq<8)),3),2))-...
                squeeze(nanmean(nanmean(mseMerged{2,2}.Rvanilla(:,CoarseScaleChannels,find(mseMerged{1,2}.freq<8)),3),2));
       MSE_OA = squeeze(nanmedian(nanmean(mseMerged{2,2}.MSEfiltskip(:,CoarseScaleChannels,find(mseMerged{1,2}.freq<8)),3),2))-...
                squeeze(nanmedian(nanmean(mseMerged{2,2}.MSEVanilla(:,CoarseScaleChannels,find(mseMerged{1,2}.freq<8)),3),2));

      [~, p] = ttest2(R_YA, R_OA)
      [~, p] = ttest2(MSE_YA, MSE_OA)
            
%% Supplement: assess correlation (i.e. shared variance) of coarse-scale MSE with low-frequency power

    figure; hold on;
        x = squeeze(nanmean(nanmean(log10(grandavg_EO.powspctrm(idx_YA_fft,CoarseScaleChannels,grandavg_EO.freq<8)),3),2));
        y = squeeze(nanmean(nanmean(mseMerged{1,indCond}.MSEVanilla(:,CoarseScaleChannels,find(mseMerged{1,2}.freq<8)),3),2));
        l1 = scatter(x,y, 100, 'filled', 'r'); y1_ls = polyval(polyfit(x,y,1),x); y1_ls = plot(x, y1_ls, 'Color', 'r', 'LineWidth', 3);
        [rl1,pl1] = corrcoef(x,y);pval1 = []; pval1 = convertPtoExponential(pl1(2));
        x = squeeze(nanmean(nanmean(log10(grandavg_EO.powspctrm(idx_OA_fft,CoarseScaleChannels,grandavg_EO.freq<8)),3),2));
        y = squeeze(nanmean(nanmean(mseMerged{2,indCond}.MSEVanilla(:,CoarseScaleChannels,find(mseMerged{1,2}.freq<8)),3),2));
        l2 = scatter(x,y, 100,'filled', 'k'); y2_ls = polyval(polyfit(x,y,1),x); y2_ls = plot(x, y2_ls, 'Color', 'k', 'LineWidth', 3);
        [rl2,pl2] = corrcoef(x,y);pval2 = []; pval2 = convertPtoExponential(pl2(2));
        legend([y1_ls, y2_ls], {['Younger Adults: r=', num2str(round(rl1(2),2)), ', p=',pval1{1}], ...
        ['Older Adults: r=', num2str(round(rl2(2),2)), ', p=',pval2{1}]}, 'Location', 'NorthEast'); legend('boxoff');
        title({'Interindividual differences in low frequency power';'relate to coarse-scale Vanilla MSE'})
        xlabel('Power < 8 Hz (log10)'); ylabel('Vanilla MSE');
        set(findall(gcf,'-property','FontSize'),'FontSize',18)
        ylim([0 1])
        
%% Supplementary: plot difference, not ratio of similarity bounds

figure; cla; hold on;
   x = squeeze(nanmean(nanmean(mseMerged{1,indCond}.Rpointavg(:,CoarseScaleChannels,find(mseMerged{1,2}.freq<8))-mseMerged{1,indCond}.Rvanilla(:,CoarseScaleChannels,find(mseMerged{1,2}.freq<8)),3),2));
   y = squeeze(nanmean(nanmean(mseMerged{1,indCond}.MSEPointavg(:,CoarseScaleChannels,find(mseMerged{1,2}.freq<8))-mseMerged{1,indCond}.MSEVanilla(:,CoarseScaleChannels,find(mseMerged{1,2}.freq<8)),3),2));
   l1 = scatter(x,y, 100,'filled', 'r'); y1_ls = polyval(polyfit(x,y,1),x); y1_ls = plot(x, y1_ls, 'Color', 'r', 'LineWidth', 3);
   [rl1,pl1] = corrcoef(x,y); pval1 = []; pval1 = convertPtoExponential(pl1(2));
   x = squeeze(nanmean(nanmean(mseMerged{2,indCond}.Rpointavg(:,CoarseScaleChannels,find(mseMerged{1,2}.freq<8))-mseMerged{2,indCond}.Rvanilla(:,CoarseScaleChannels,find(mseMerged{1,2}.freq<8)),3),2));
   y = squeeze(nanmean(nanmean(mseMerged{2,indCond}.MSEPointavg(:,CoarseScaleChannels,find(mseMerged{1,2}.freq<8))-mseMerged{2,indCond}.MSEVanilla(:,CoarseScaleChannels,find(mseMerged{1,2}.freq<8)),3),2));
   %title('Interindividual shifts in R and MSE')
   title({'Liberality of similarity bounds ';'relates to coarse-scale (<8 Hz) entropy'})
   xlabel('Difference in similarity bounds: Rescaled vs. global bound'); ylabel('Difference in MSE: Rescaled vs. global bound');
   l2 = scatter(x,y, 100,'filled', 'k'); y2_ls = polyval(polyfit(x,y,1),x); y2_ls = plot(x, y2_ls, 'Color', 'k', 'LineWidth', 3);
   [rl2,pl2] = corrcoef(x,y);pval2 = []; pval2 = convertPtoExponential(pl2(2));
   legend([y1_ls, y2_ls], {['Younger Adults: r=', num2str(round(rl1(2),2)), ', p=',pval1{1}], ...
    ['Older Adults: r=', num2str(round(rl2(2),2)), ', p=',pval2{1}]}, 'Location', 'NorthEast'); legend('boxoff');
   set(findall(gcf,'-property','FontSize'),'FontSize',18)
