pn.data = '/Users/kosciessa/Desktop/mntTardis/STSWD_Rest/WIP_eeg/D_eBOSC_noDur/B_data/C_RhythmCharacteristics/';
load([pn.data, 'C2_EventsMerged.mat'], 'Events_Merged', 'EventsNonRhythm_Merged', 'EventsRhythm_Merged');

freq = 2.^[1:.125:6];
freq = freq(4:end-3);
 
% load stats cluster of bandpass difference

pn.root = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/A_MSE_CSD_multiVariant/';
load([pn.root, 'B_data/E2_CBPA_Age_Condition.mat'], 'stat', 'methodLabels')

% correlate with individual MSE values

% load('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/A_MSE_CSD_multiVariant/B_data/E_mseMerged.mat')

addpath('/Users/kosciessa/Desktop/mntTardis/STSWD_Rest/WIP_eeg/D_eBOSC_noDur/T_tools')

betaCluster = max(stat{4,5}.mask(:,23:27),[],2);
alphaCluster = max(stat{4,5}.mask(:,15:20),[],2);

BetaRate_YA = squeeze(nanmean(max(EventsRhythm_Merged(1:47, betaCluster, freq > 14 & freq< 20,2),[],3),2));
BetaRate_OA = squeeze(nanmean(max(EventsRhythm_Merged(48:end, betaCluster, freq > 14 & freq< 20,2),[],3),2));

AlphaRate_YA = squeeze(nanmean(max(EventsRhythm_Merged(1:47, alphaCluster, freq > 8 & freq< 12,2),[],3),2));
AlphaRate_OA = squeeze(nanmean(max(EventsRhythm_Merged(48:end, alphaCluster, freq > 8 & freq< 12,2),[],3),2));

%% for Vanilla & bandpass, compute scale-wise correlations with spectral rate & plot

% mark center frequency of rhythm rate

for indFreq = 1:42
    BetaMSE_YA = squeeze(nanmean(nanmean(mseMerged{1,2}.MSEbp(:,betaCluster,indFreq),3),2));
    BetaMSE_OA = squeeze(nanmean(nanmean(mseMerged{2,2}.MSEbp(:,betaCluster,indFreq),3),2));

    AlphaMSE_YA = squeeze(nanmean(nanmean(mseMerged{1,2}.MSEbp(:,alphaCluster,indFreq),3),2));
    AlphaMSE_OA = squeeze(nanmean(nanmean(mseMerged{2,2}.MSEbp(:,alphaCluster,indFreq),3),2));

    x = AlphaMSE_YA; y = AlphaRate_YA; [r, p] = corrcoef(x, y); RMat_YA_Alpha_bp(indFreq) = r(2);
    x = AlphaMSE_OA; y = AlphaRate_OA; [r, p] = corrcoef(x, y); RMat_OA_Alpha_bp(indFreq) = r(2);
    x = BetaMSE_YA; y = BetaRate_YA; [r, p] = corrcoef(x, y); RMat_YA_Beta_bp(indFreq) = r(2);
    x = BetaMSE_OA; y = BetaRate_OA; [r, p] = corrcoef(x, y); RMat_OA_Beta_bp(indFreq) = r(2);
end

for indFreq = 1:42
    BetaMSE_YA = squeeze(nanmean(nanmean(mseMerged{1,2}.MSEfiltskip(:,betaCluster,indFreq),3),2));
    BetaMSE_OA = squeeze(nanmean(nanmean(mseMerged{2,2}.MSEfiltskip(:,betaCluster,indFreq),3),2));

    AlphaMSE_YA = squeeze(nanmean(nanmean(mseMerged{1,2}.MSEfiltskip(:,alphaCluster,indFreq),3),2));
    AlphaMSE_OA = squeeze(nanmean(nanmean(mseMerged{2,2}.MSEfiltskip(:,alphaCluster,indFreq),3),2));

    x = AlphaMSE_YA; y = AlphaRate_YA; [r, p] = corrcoef(x, y); RMat_YA_Alpha_lp(indFreq) = r(2);
    x = AlphaMSE_OA; y = AlphaRate_OA; [r, p] = corrcoef(x, y); RMat_OA_Alpha_lp(indFreq) = r(2);
    x = BetaMSE_YA; y = BetaRate_YA; [r, p] = corrcoef(x, y); RMat_YA_Beta_lp(indFreq) = r(2);
    x = BetaMSE_OA; y = BetaRate_OA; [r, p] = corrcoef(x, y); RMat_OA_Beta_lp(indFreq) = r(2);
end

h = figure('units','normalized','position',[0 0 1 .5]);
subplot(2,8,[3,4]); cla; hold on; 
    % add frequency indicators
    frequencies_num = [8.5 12, 6 8];
    patches.ampVec = [-1; 1; -1; 1];
    patches.timeVec = [250/frequencies_num(1);250/frequencies_num(2); 250/frequencies_num(3);250/frequencies_num(4)];
    patches.colorVec =[.8 .8 .8; 1 1 1; 255/255 230/255 160/255]; 
    for indP = 1:2:size(patches.ampVec,1)-1
        p = patch([patches.timeVec(indP,1) patches.timeVec(indP+1,1) patches.timeVec(indP+1,1) patches.timeVec(indP,1)], ...
                    [patches.ampVec(indP,1) patches.ampVec(indP,1) patches.ampVec(indP+1,1) patches.ampVec(indP+1,1)], patches.colorVec(indP,:));
        p.EdgeColor = 'none';%patches.colorVec(indP,:);
    end    
    % plot correlations
    plot(squeeze(RMat_YA_Alpha_bp), 'k',  'LineWidth', 2); 
    plot(squeeze(RMat_OA_Alpha_bp), 'r', 'LineWidth', 2); 
    xlim([1,42]); 
    title('Bandpass MPE - Alpha rate')
    set(gca, 'XTickLabels', round(mseMerged{1,2}.freq(get(gca,'XTick')),2)); ylim([-1 1])
    set(gca, 'XScale', 'log')
    scales = 1:42;
    set(gca, 'XTick',round(logspace(log10(1), log10(41), 6)));
    tmp_meas = [round(mseMerged{1,1}.freq(get(gca, 'XTick')),0); round(scales(get(gca, 'XTick')),0)];
    xLabels = [];
    for indScale = 1:size(tmp_meas,2)
        xLabels{indScale} = [num2str(tmp_meas(1,indScale))];
    end; clear tmp_meas;
    set(gca, 'XTickLabels', xLabels);
    xlabel('Time scale [Hz; log-scaled]'); ylabel('Correlation coefficient (r)');
subplot(2,8,[5,6]); cla; hold on; 
    % add frequency indicators
    frequencies_num = [14 20, 8 12];
    patches.ampVec = [-1; 1; -1; 1];
    patches.timeVec = [250/frequencies_num(1);250/frequencies_num(2); 250/frequencies_num(3);250/frequencies_num(4)];
    patches.colorVec =[.8 .8 .8; 1 1 1; 255/255 230/255 160/255]; 
    for indP = 1:2:size(patches.ampVec,1)-1
        p = patch([patches.timeVec(indP,1) patches.timeVec(indP+1,1) patches.timeVec(indP+1,1) patches.timeVec(indP,1)], ...
                    [patches.ampVec(indP,1) patches.ampVec(indP,1) patches.ampVec(indP+1,1) patches.ampVec(indP+1,1)], patches.colorVec(indP,:));
        p.EdgeColor = 'none';%patches.colorVec(indP,:);
    end    
    % plot correlations
    plot(squeeze(RMat_YA_Beta_bp), 'k',  'LineWidth', 2); 
    plot(squeeze(RMat_OA_Beta_bp), 'r', 'LineWidth', 2); 
    xlim([1,42]); 
    title('Bandpass MPE - Beta rate')
    ylim([-1 1])
    set(gca, 'XScale', 'log')
    scales = 1:42;
    set(gca, 'XTick',round(logspace(log10(1), log10(41), 6)));
    tmp_meas = [round(mseMerged{1,1}.freq(get(gca, 'XTick')),0); round(scales(get(gca, 'XTick')),0)];
    xLabels = [];
    for indScale = 1:size(tmp_meas,2)
        xLabels{indScale} = [num2str(tmp_meas(1,indScale))];
    end; clear tmp_meas;
    set(gca, 'XTickLabels', xLabels);
    xlabel('Time scale [Hz; log-scaled]'); ylabel('Correlation coefficient (r)');
subplot(2,8,[11,12]); hold on; 
    % add frequency indicators
    frequencies_num = [8 12, 60 250];
    patches.ampVec = [-1; 1; -1; 1];
    patches.timeVec = [250/frequencies_num(1);250/frequencies_num(2); 250/frequencies_num(3);250/frequencies_num(4)];
    patches.colorVec =[.8 .8 .8; 1 1 1; 255/255 230/255 160/255]; 
    for indP = 1:2:size(patches.ampVec,1)-1
        p = patch([patches.timeVec(indP,1) patches.timeVec(indP+1,1) patches.timeVec(indP+1,1) patches.timeVec(indP,1)], ...
                    [patches.ampVec(indP,1) patches.ampVec(indP,1) patches.ampVec(indP+1,1) patches.ampVec(indP+1,1)], patches.colorVec(indP,:));
        p.EdgeColor = 'none';%patches.colorVec(indP,:);
    end    
    % plot correlations
    plot(squeeze(RMat_YA_Alpha_lp), 'k',  'LineWidth', 2); 
    plot(squeeze(RMat_OA_Alpha_lp), 'r', 'LineWidth', 2); 
    xlim([1,42]); 
    title('Low-pass MPE - Alpha rate')
    set(gca, 'XScale', 'log')
    scales = 1:42;
    set(gca, 'XTick',round(logspace(log10(1), log10(41), 6)));
    tmp_meas = [round(mseMerged{1,1}.freq(get(gca, 'XTick')),0); round(scales(get(gca, 'XTick')),0)];
    xLabels = [];
    for indScale = 1:size(tmp_meas,2)
        xLabels{indScale} = [num2str(tmp_meas(1,indScale))];
    end; clear tmp_meas;
    set(gca, 'XTickLabels', xLabels);
    ylim([-1 1])
    xlabel('Time scale [Hz; log-scaled]'); ylabel('Correlation coefficient (r)');
subplot(2,8,[13,14]); cla; hold on; 
     % add frequency indicators
    frequencies_num = [14 20, 60 250];
    patches.ampVec = [-1; 1; -1; 1];
    patches.timeVec = [250/frequencies_num(1);250/frequencies_num(2); 250/frequencies_num(3);250/frequencies_num(4)];
    patches.colorVec =[.8 .8 .8; 1 1 1; 255/255 230/255 160/255]; 
    for indP = 1:2:size(patches.ampVec,1)-1
        p = patch([patches.timeVec(indP,1) patches.timeVec(indP+1,1) patches.timeVec(indP+1,1) patches.timeVec(indP,1)], ...
                    [patches.ampVec(indP,1) patches.ampVec(indP,1) patches.ampVec(indP+1,1) patches.ampVec(indP+1,1)], patches.colorVec(indP,:));
        p.EdgeColor = 'none';%patches.colorVec(indP,:);
    end    
    % plot correlations
    plot(squeeze(RMat_YA_Beta_lp), 'k',  'LineWidth', 2); 
    plot(squeeze(RMat_OA_Beta_lp), 'r', 'LineWidth', 2); 
    xlim([1,42]); 
    title('Low-pass MPE - Beta rate')
    set(gca, 'XScale', 'log')
    scales = 1:42;
    set(gca, 'XTick',round(logspace(log10(1), log10(41), 6)));
    tmp_meas = [round(mseMerged{1,1}.freq(get(gca, 'XTick')),0); round(scales(get(gca, 'XTick')),0)];
    xLabels = [];
    for indScale = 1:size(tmp_meas,2)
        xLabels{indScale} = [num2str(tmp_meas(1,indScale))];
    end; clear tmp_meas;
    set(gca, 'XTickLabels', xLabels);
    ylim([-1 1])
    xlabel('Time scale [Hz; log-scaled]'); ylabel('Correlation coefficient (r)');
    
%% add correlation example plots with statistics: band-specific

subplot(2,8,1); cla; hold on;

    BetaMSE_YA = squeeze(nanmean(nanmean(mseMerged{1,2}.MSEbp(:,betaCluster,mseMerged{1,1}.freq> 14 & mseMerged{1,1}.freq< 20),3),2));
    BetaMSE_OA = squeeze(nanmean(nanmean(mseMerged{2,2}.MSEbp(:,betaCluster,mseMerged{1,1}.freq> 14 & mseMerged{1,1}.freq< 20),3),2));

    AlphaMSE_YA = squeeze(nanmean(nanmean(mseMerged{1,2}.MSEbp(:,alphaCluster,mseMerged{1,1}.freq> 8 & mseMerged{1,1}.freq< 12),3),2));
    AlphaMSE_OA = squeeze(nanmean(nanmean(mseMerged{2,2}.MSEbp(:,alphaCluster,mseMerged{1,1}.freq> 8 & mseMerged{1,1}.freq< 12),3),2));

    x = AlphaRate_YA; y = AlphaMSE_YA; 
    l1 = scatter(x,y, 'filled', 'k'); y1_ls = polyval(polyfit(x,y,1),x); y1_ls = plot(x, y1_ls, 'Color', 'k');
    [r1, p1] = corrcoef(x, y); p1 = convertPtoExponential(p1(2));
    x = AlphaRate_OA; y = AlphaMSE_OA; 
    l3 = scatter(x,y, 'filled', 'r'); y3_ls = polyval(polyfit(x,y,1),x); y3_ls = plot(x, y3_ls, 'Color', 'r');
    [r3, p3] = corrcoef(x, y); p3 = convertPtoExponential(p3(2));
    [lgd1, lgd1i] = legend([y1_ls, y3_ls], {['YA Alpha: r = ', num2str(round(r1(2),2)), ', p = ' p1{1}], ...
        ['OA Alpha: r = ', num2str(round(r3(2),2)), ', p = ' p3{1}]}, 'location', 'SouthEast'); legend('boxoff');
    xlabel('Number of Alpha events'); ylabel('Entropy');
    %ylim([.2 .55])
    title('Parietooccipital Alpha')
    set(gca,'Color',[.8 .8 .8]); 
    ylim([min([AlphaMSE_YA;AlphaMSE_OA])-min([AlphaMSE_YA;AlphaMSE_OA])*.03; max([AlphaMSE_YA;AlphaMSE_OA])+max([AlphaMSE_YA;AlphaMSE_OA])*.01])

subplot(2,8,2); cla; hold on;
    x = BetaRate_YA; y = BetaMSE_YA; 
    l2 = scatter(x,y, 'filled', 'k'); y2_ls = polyval(polyfit(x,y,1),x); y2_ls = plot(x, y2_ls, 'Color', 'k');
    [r2, p2] = corrcoef(x, y);  p2 = convertPtoExponential(p2(2));
    x = BetaRate_OA; y = BetaMSE_OA; 
    l4 = scatter(x,y, 'filled', 'r'); y4_ls = polyval(polyfit(x,y,1),x); y4_ls = plot(x, y4_ls, 'Color', 'r');
    [r4, p4] = corrcoef(x, y);  p4 = convertPtoExponential(p4(2));
    [lgd2, lgd2i] = legend([y2_ls, y4_ls], {['YA Beta: r = ', num2str(round(r2(2),2)), ', p = ' p2{1}],...
        ['OA Beta: r = ', num2str(round(r4(2),2)), ', p = ' p4{1}]}, 'location', 'SouthEast'); legend('boxoff');
    xlabel('Number of Beta events'); ylabel('Entropy');
    %ylim([.3 .5])
    title('Central Beta')
    set(gca,'Color',[.8 .8 .8]); 
    ylim([min([BetaMSE_YA;BetaMSE_OA])-min([BetaMSE_YA;BetaMSE_OA])*.03; max([BetaMSE_YA;BetaMSE_OA])+max([BetaMSE_YA;BetaMSE_OA])*.01])

subplot(2,8,15); cla; hold on;

    BetaMSE_YA = squeeze(nanmean(nanmean(mseMerged{1,2}.MSEfiltskip(:,betaCluster,mseMerged{1,1}.freq> 14 & mseMerged{1,1}.freq< 20),3),2));
    BetaMSE_OA = squeeze(nanmean(nanmean(mseMerged{2,2}.MSEfiltskip(:,betaCluster,mseMerged{1,1}.freq> 14 & mseMerged{1,1}.freq< 20),3),2));

    AlphaMSE_YA = squeeze(nanmean(nanmean(mseMerged{1,2}.MSEfiltskip(:,alphaCluster,mseMerged{1,1}.freq> 8 & mseMerged{1,1}.freq< 12),3),2));
    AlphaMSE_OA = squeeze(nanmean(nanmean(mseMerged{2,2}.MSEfiltskip(:,alphaCluster,mseMerged{1,1}.freq> 8 & mseMerged{1,1}.freq< 12),3),2));

    x = AlphaRate_YA; y = AlphaMSE_YA; 
    l1 = scatter(x,y, 'filled', 'k'); y1_ls = polyval(polyfit(x,y,1),x); y1_ls = plot(x, y1_ls, 'Color', 'k');
    [r1, p1] = corrcoef(x, y); p1 = convertPtoExponential(p1(2));
    x = AlphaRate_OA; y = AlphaMSE_OA; 
    l3 = scatter(x,y, 'filled', 'r'); y3_ls = polyval(polyfit(x,y,1),x); y3_ls = plot(x, y3_ls, 'Color', 'r');
    [r3, p3] = corrcoef(x, y); p3 = convertPtoExponential(p3(2));
    [lgd9, lgd9i] = legend([y1_ls, y3_ls], {['YA Alpha: r = ', num2str(round(r1(2),2)), ', p = ' p1{1}], ...
        ['OA Alpha: r = ', num2str(round(r3(2),2)), ', p = ' p3{1}]}, 'location', 'SouthEast'); legend('boxoff');
    xlabel('Number of Alpha events'); ylabel('Entropy');
    %ylim([.2 .55])
    title('Parietooccipital Alpha')
    set(gca,'Color',[.8 .8 .8]); 
    ylim([min([AlphaMSE_YA;AlphaMSE_OA])-min([AlphaMSE_YA;AlphaMSE_OA])*.03; max([AlphaMSE_YA;AlphaMSE_OA])+max([AlphaMSE_YA;AlphaMSE_OA])*.01])

subplot(2,8,16); cla; hold on;
    x = BetaRate_YA; y = BetaMSE_YA; 
    l2 = scatter(x,y, 'filled', 'k'); y2_ls = polyval(polyfit(x,y,1),x); y2_ls = plot(x, y2_ls, 'Color', 'k');
    [r2, p2] = corrcoef(x, y);  p2 = convertPtoExponential(p2(2));
    x = BetaRate_OA; y = BetaMSE_OA; 
    l4 = scatter(x,y, 'filled', 'r'); y4_ls = polyval(polyfit(x,y,1),x); y4_ls = plot(x, y4_ls, 'Color', 'r');
    [r4, p4] = corrcoef(x, y);  p4 = convertPtoExponential(p4(2));
    [lgd10, lgd10i] = legend([y2_ls, y4_ls], {['YA Beta: r = ', num2str(round(r2(2),2)), ', p = ' p2{1}],...
        ['OA Beta: r = ', num2str(round(r4(2),2)), ', p = ' p4{1}]}, 'location', 'SouthEast'); legend('boxoff');
    xlabel('Number of Beta events'); ylabel('Entropy');
    %ylim([.3 .5])
    title('Central Beta')
    set(gca,'Color',[.8 .8 .8]); 
    ylim([min([BetaMSE_YA;BetaMSE_OA])-min([BetaMSE_YA;BetaMSE_OA])*.03; max([BetaMSE_YA;BetaMSE_OA])+max([BetaMSE_YA;BetaMSE_OA])*.01])

%% add lateral correlations
    
subplot(2,8,7); cla; hold on;

    BetaMSE_YA = squeeze(nanmean(nanmean(mseMerged{1,2}.MSEbp(:,betaCluster,mseMerged{1,1}.freq> 8 & mseMerged{1,1}.freq< 12),3),2));
    BetaMSE_OA = squeeze(nanmean(nanmean(mseMerged{2,2}.MSEbp(:,betaCluster,mseMerged{1,1}.freq> 8 & mseMerged{1,1}.freq< 12),3),2));

    AlphaMSE_YA = squeeze(nanmean(nanmean(mseMerged{1,2}.MSEbp(:,alphaCluster,mseMerged{1,1}.freq> 6 & mseMerged{1,1}.freq< 8),3),2));
    AlphaMSE_OA = squeeze(nanmean(nanmean(mseMerged{2,2}.MSEbp(:,alphaCluster,mseMerged{1,1}.freq> 6 & mseMerged{1,1}.freq< 8),3),2));

    x = AlphaRate_YA; y = AlphaMSE_YA; 
    l1 = scatter(x,y, 'filled', 'k'); y1_ls = polyval(polyfit(x,y,1),x); y1_ls = plot(x, y1_ls, 'Color', 'k');
    [r1, p1] = corrcoef(x, y); p1 = convertPtoExponential(p1(2));
    x = AlphaRate_OA; y = AlphaMSE_OA; 
    l3 = scatter(x,y, 'filled', 'r'); y3_ls = polyval(polyfit(x,y,1),x); y3_ls = plot(x, y3_ls, 'Color', 'r');
    [r3, p3] = corrcoef(x, y); p3 = convertPtoExponential(p3(2));
    [lgd7, lgd7i] = legend([y1_ls, y3_ls], {['YA Alpha: r = ', num2str(round(r1(2),2)), ', p = ' p1{1}], ...
        ['OA Alpha: r = ', num2str(round(r3(2),2)), ', p = ' p3{1}]}, 'location', 'SouthEast'); legend('boxoff');
    xlabel('Number of Alpha events'); ylabel('Entropy');
    %ylim([.2 .55])
    title('Parietooccipital Alpha')
    set(gca,'Color',[255/255 230/255 160/255]); 
    ylim([min([AlphaMSE_YA;AlphaMSE_OA])-min([AlphaMSE_YA;AlphaMSE_OA])*.03; max([AlphaMSE_YA;AlphaMSE_OA])+max([AlphaMSE_YA;AlphaMSE_OA])*.01])

subplot(2,8,8); cla; hold on;
    x = BetaRate_YA; y = BetaMSE_YA; 
    l2 = scatter(x,y, 'filled', 'k'); y2_ls = polyval(polyfit(x,y,1),x); y2_ls = plot(x, y2_ls, 'Color', 'k');
    [r2, p2] = corrcoef(x, y);  p2 = convertPtoExponential(p2(2));
    x = BetaRate_OA; y = BetaMSE_OA; 
    l4 = scatter(x,y, 'filled', 'r'); y4_ls = polyval(polyfit(x,y,1),x); y4_ls = plot(x, y4_ls, 'Color', 'r');
    [r4, p4] = corrcoef(x, y);  p4 = convertPtoExponential(p4(2));
    [lgd8, lgd8i] = legend([y2_ls, y4_ls], {['YA Beta: r = ', num2str(round(r2(2),2)), ', p = ' p2{1}],...
        ['OA Beta: r = ', num2str(round(r4(2),2)), ', p = ' p4{1}]}, 'location', 'SouthEast'); legend('boxoff');
    xlabel('Number of Beta events'); ylabel('Entropy');
    %ylim([.3 .5])
    title('Central Beta')
    set(gca,'Color',[255/255 230/255 160/255]);
    ylim([min([BetaMSE_YA;BetaMSE_OA])-min([BetaMSE_YA;BetaMSE_OA])*.03; max([BetaMSE_YA;BetaMSE_OA])+max([BetaMSE_YA;BetaMSE_OA])*.01])

subplot(2,8,9); cla; hold on;

    BetaMSE_YA = squeeze(nanmean(nanmean(mseMerged{1,2}.MSEfiltskip(:,betaCluster,mseMerged{1,1}.freq> 60),3),2));
    BetaMSE_OA = squeeze(nanmean(nanmean(mseMerged{2,2}.MSEfiltskip(:,betaCluster,mseMerged{1,1}.freq> 60),3),2));

    AlphaMSE_YA = squeeze(nanmean(nanmean(mseMerged{1,2}.MSEfiltskip(:,alphaCluster,mseMerged{1,1}.freq> 60),3),2));
    AlphaMSE_OA = squeeze(nanmean(nanmean(mseMerged{2,2}.MSEfiltskip(:,alphaCluster,mseMerged{1,1}.freq> 60),3),2));

    x = AlphaRate_YA; y = AlphaMSE_YA; 
    l1 = scatter(x,y, 'filled', 'k'); y1_ls = polyval(polyfit(x,y,1),x); y1_ls = plot(x, y1_ls, 'Color', 'k');
    [r1, p1] = corrcoef(x, y); p1 = convertPtoExponential(p1(2));
    x = AlphaRate_OA; y = AlphaMSE_OA; 
    l3 = scatter(x,y, 'filled', 'r'); y3_ls = polyval(polyfit(x,y,1),x); y3_ls = plot(x, y3_ls, 'Color', 'r');
    [r3, p3] = corrcoef(x, y); p3 = convertPtoExponential(p3(2));
    [lgd15, lgd15i] = legend([y1_ls, y3_ls], {['YA Alpha: r = ', num2str(round(r1(2),2)), ', p = ' p1{1}], ...
        ['OA Alpha: r = ', num2str(round(r3(2),2)), ', p = ' p3{1}]}, 'location', 'SouthEast'); legend('boxoff');
    xlabel('Number of Alpha events'); ylabel('Entropy');
    %ylim([.2 .55])
    title('Parietooccipital Alpha')
    set(gca,'Color',[255/255 230/255 160/255]); 
    ylim([min([AlphaMSE_YA;AlphaMSE_OA])-min([AlphaMSE_YA;AlphaMSE_OA])*.03; max([AlphaMSE_YA;AlphaMSE_OA])+max([AlphaMSE_YA;AlphaMSE_OA])*.01])

subplot(2,8,10); cla; hold on;
    x = BetaRate_YA; y = BetaMSE_YA; 
    l2 = scatter(x,y, 'filled', 'k'); y2_ls = polyval(polyfit(x,y,1),x); y2_ls = plot(x, y2_ls, 'Color', 'k');
    [r2, p2] = corrcoef(x, y);  p2 = convertPtoExponential(p2(2));
    x = BetaRate_OA; y = BetaMSE_OA; 
    l4 = scatter(x,y, 'filled', 'r'); y4_ls = polyval(polyfit(x,y,1),x); y4_ls = plot(x, y4_ls, 'Color', 'r');
    [r4, p4] = corrcoef(x, y);  p4 = convertPtoExponential(p4(2));
    [lgd16, lgd16i] = legend([y2_ls, y4_ls], {['YA Beta: r = ', num2str(round(r2(2),2)), ', p = ' p2{1}],...
        ['OA Beta: r = ', num2str(round(r4(2),2)), ', p = ' p4{1}]}, 'location', 'SouthEast'); legend('boxoff');
    xlabel('Number of Beta events'); ylabel('Entropy');
    %ylim([.3 .5])
    title('Central Beta')
    set(gca,'Color',[255/255 230/255 160/255]); 
    ylim([min([BetaMSE_YA;BetaMSE_OA])-min([BetaMSE_YA;BetaMSE_OA])*.03; max([BetaMSE_YA;BetaMSE_OA])+max([BetaMSE_YA;BetaMSE_OA])*.01])

    set(findall(gcf,'-property','FontSize'),'FontSize',15)

    % reset legend Font sizes
    set(lgd1,'FontSize',10, 'location', 'south');
    set(lgd2,'FontSize',10, 'location', 'south');
    set(lgd7,'FontSize',10, 'location', 'south');
    set(lgd8,'FontSize',10, 'location', 'south');
    set(lgd9,'FontSize',10, 'location', 'south');
    set(lgd10,'FontSize',10, 'location', 'south');
    set(lgd15,'FontSize',10, 'location', 'south');
    set(lgd16,'FontSize',10, 'location', 'south');
    
    %% save Figure
    
    set(gcf,'color','w');
    set(gcf, 'InvertHardCopy', 'off');
    pn.plotFolder = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/A_MSE_CSD_multiVariant/C_figures/';
    figureName = 'H3_spectralRate_BPvsLP';
    saveas(h, [pn.plotFolder, figureName], 'epsc');
    saveas(h, [pn.plotFolder, figureName], 'png');

%% check reliability of event-related entropy decrease between methods
% 
% BetaMSE_YA_lp = squeeze(nanmean(nanmean(mseMerged{1,2}.MSEfiltskip(:,betaCluster,mseMerged{1,1}.freq> 14 & mseMerged{1,1}.freq< 20),3),2));
% BetaMSE_OA_lp = squeeze(nanmean(nanmean(mseMerged{2,2}.MSEfiltskip(:,betaCluster,mseMerged{1,1}.freq> 14 & mseMerged{1,1}.freq< 20),3),2));
% 
% % r_AlphaMSE_YA_lp = squeeze(nanmean(nanmean(mseMerged{1,2}.MSEfiltskip(:,alphaCluster,mseMerged{1,1}.freq> 60),3),2));
% % r_AlphaMSE_OA_lp = squeeze(nanmean(nanmean(mseMerged{2,2}.MSEfiltskip(:,alphaCluster,mseMerged{1,1}.freq> 60),3),2));
% 
% AlphaMSE_YA_lp = squeeze(nanmean(nanmean(mseMerged{1,2}.MSEfiltskip(:,alphaCluster,mseMerged{1,1}.freq> 8 & mseMerged{1,1}.freq< 12),3),2));
% AlphaMSE_OA_lp = squeeze(nanmean(nanmean(mseMerged{2,2}.MSEfiltskip(:,alphaCluster,mseMerged{1,1}.freq> 8 & mseMerged{1,1}.freq< 12),3),2));
% 
% % r_BetaMSE_YA_lp = squeeze(nanmean(nanmean(mseMerged{1,2}.MSEfiltskip(:,betaCluster,mseMerged{1,1}.freq> 15),3),2));
% % r_BetaMSE_OA_lp = squeeze(nanmean(nanmean(mseMerged{2,2}.MSEfiltskip(:,betaCluster,mseMerged{1,1}.freq> 15),3),2));
% 
% BetaMSE_YA_bp = squeeze(nanmean(nanmean(mseMerged{1,2}.MSEbp(:,betaCluster,mseMerged{1,1}.freq> 14 & mseMerged{1,1}.freq< 20),3),2));
% BetaMSE_OA_bp = squeeze(nanmean(nanmean(mseMerged{2,2}.MSEbp(:,betaCluster,mseMerged{1,1}.freq> 14 & mseMerged{1,1}.freq< 20),3),2));
% 
% AlphaMSE_YA_bp = squeeze(nanmean(nanmean(mseMerged{1,2}.MSEbp(:,alphaCluster,mseMerged{1,1}.freq> 8 & mseMerged{1,1}.freq< 12),3),2));
% AlphaMSE_OA_bp = squeeze(nanmean(nanmean(mseMerged{2,2}.MSEbp(:,alphaCluster,mseMerged{1,1}.freq> 8 & mseMerged{1,1}.freq< 12),3),2));
% 
% % create residuals
% 
% [~, ~, BetaMSE_YA_lp_r] = regress(BetaMSE_YA_lp, [repmat(1,size(AlphaMSE_YA_lp)), AlphaMSE_YA_lp]);
% [~, ~, BetaMSE_OA_lp_r] = regress(BetaMSE_OA_lp, [repmat(1,size(AlphaMSE_OA_lp)), AlphaMSE_OA_lp]);
% 
% [~, ~, AlphaMSE_YA_lp_r] = regress(AlphaMSE_YA_lp, [repmat(1,size(BetaMSE_YA_lp)), BetaMSE_YA_lp]);
% [~, ~, AlphaMSE_OA_lp_r] = regress(AlphaMSE_OA_lp, [repmat(1,size(BetaMSE_OA_lp)), BetaMSE_OA_lp]);
% 
% 
% figure
% subplot(1,2,1); cla; hold on;
%     x = BetaMSE_YA_lp_r; y = BetaMSE_YA_bp; 
%     l2 = scatter(x,y, 'filled', 'k'); y2_ls = polyval(polyfit(x,y,1),x); y2_ls = plot(x, y2_ls, 'Color', 'k');
%     [r2, p2] = corrcoef(x, y);  p2 = convertPtoExponential(p2(2));
%     x = BetaMSE_OA_lp_r; y = BetaMSE_OA_bp; 
%     l4 = scatter(x,y, 'filled', 'r'); y4_ls = polyval(polyfit(x,y,1),x); y4_ls = plot(x, y4_ls, 'Color', 'r');
%     [r4, p4] = corrcoef(x, y);  p4 = convertPtoExponential(p4(2));
%     legend([y2_ls, y4_ls], {['YA Beta: r = ', num2str(round(r2(2),2)), ', p = ' p2{1}],...
%         ['OA Beta: r = ', num2str(round(r4(2),2)), ', p = ' p4{1}]}); legend('boxoff');
%     xlabel('Number of Beta events'); ylabel('Entropy');
%     %ylim([.3 .5])
%     title('Central Beta')
%     
% subplot(1,2,2); cla; hold on;
%     x = AlphaMSE_YA_lp_r; y = AlphaMSE_YA_bp; 
%     l2 = scatter(x,y, 'filled', 'k'); y2_ls = polyval(polyfit(x,y,1),x); y2_ls = plot(x, y2_ls, 'Color', 'k');
%     [r2, p2] = corrcoef(x, y);  p2 = convertPtoExponential(p2(2));
%     x = AlphaMSE_OA_lp_r; y = AlphaMSE_OA_bp; 
%     l4 = scatter(x,y, 'filled', 'r'); y4_ls = polyval(polyfit(x,y,1),x); y4_ls = plot(x, y4_ls, 'Color', 'r');
%     [r4, p4] = corrcoef(x, y);  p4 = convertPtoExponential(p4(2));
%     legend([y2_ls, y4_ls], {['YA Beta: r = ', num2str(round(r2(2),2)), ', p = ' p2{1}],...
%         ['OA Alpha: r = ', num2str(round(r4(2),2)), ', p = ' p4{1}]}); legend('boxoff');
%     xlabel(''); ylabel('Entropy');
%     %ylim([.3 .5])
%     title('Alpha')
%     
%     
% figure; scatter(AlphaMSE_YA_lp, BetaMSE_YA_lp)
% figure; scatter(BetaMSE_OA_lp, AlphaMSE_OA_lp)
% 
% figure; scatter(AlphaMSE_YA_bp, BetaMSE_YA_bp)
% figure; scatter(BetaMSE_OA_bp, AlphaMSE_OA_bp)
% 
% % low-pass clusters are highly positively correlated and reflect alpha events
% 
% % in contrast, beta does not relate to fast scale decrease