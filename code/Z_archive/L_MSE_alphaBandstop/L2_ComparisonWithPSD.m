%% Plot difference between methods

pn.dataPath = '/Users/kosciessa/Desktop/mntTardis/STSWD_Rest/WIP_eeg/A_MSE_CSD_multiVariant/B_data/C_MSE_Output_alphaBS/';

IDs{1} = {'1117';'1118';'1120';'1124';'1126';'1131';'1132';'1135';'1136';'1138';...
    '1144';'1151';'1158';'1160';'1163';'1164';'1167';'1169';'1172';'1173';...
    '1178';'1182';'1215';'1216';'1219';'1221';'1223';'1227';'1228';...
    '1233';'1234';'1237';'1239';'1240';'1243';'1245';'1247';'1250';'1252';...
    '1257';'1261';'1265';'1266';'1268';'1270';'1276';'1281'};

IDs{2} = {'2104';'2107';'2108';'2112';'2118';'2120';'2121';'2123';'2125';'2129';...
    '2130';'2131';'2132';'2133';'2134';'2135';'2139';'2140';'2145';'2147';...
    '2149';'2157';'2160';'2202';'2203';'2205';'2206';'2209';'2210';...
    '2211';'2213';'2214';'2215';'2216';'2217';'2219';'2222';'2224';'2226';...
    '2227';'2236';'2237';'2238';'2241';'2244';'2246';'2248';'2250';'2251';...
    '2252';'2258';'2261'};

conds = {'EC'; 'EO'};

mseMerged = cell(1,1);
for indGroup = 1:2
    for indID = 1:numel(IDs{indGroup})
        for indCond = 1:2
            try
            curID = IDs{indGroup}{indID};
            % vanilla
            load([pn.dataPath, curID, '_',conds{indCond},'_MSE_OUT_vanilla.mat']);
            mseMerged{indGroup,indCond}.MSEVanilla(indID,:,:) = mse.sampen(1:60,:);
            mseMerged{indGroup,indCond}.Rvanilla(indID,:,:) = squeeze(mse.r(1:60,:));
            % pointavg
            load([pn.dataPath, curID, '_',conds{indCond},'_MSE_OUT_pointavg.mat']);
            mseMerged{indGroup,indCond}.MSEPointavg(indID,:,:) = mse.sampen(1:60,:);
            mseMerged{indGroup,indCond}.Rpointavg(indID,:,:) = squeeze(mse.r(1:60,:));
            % filtskip (LP)
            load([pn.dataPath, curID, '_',conds{indCond},'_MSE_OUT_filtskip.mat']);
            mseMerged{indGroup,indCond}.MSEfiltskip(indID,:,:) = mse.sampen(1:60,:);
            mseMerged{indGroup,indCond}.Rfiltskip(indID,:,:) = squeeze(mse.r(1:60,:));
            % filtskip (HP)
            load([pn.dataPath, curID, '_',conds{indCond},'_MSE_OUT_hp.mat']);
            mseMerged{indGroup,indCond}.MSEhp(indID,:,:) = mse.sampen(1:60,:);
            mseMerged{indGroup,indCond}.Rhp(indID,:,:) = squeeze(mse.r(1:60,:));
            % filtskip (BP)
            load([pn.dataPath, curID, '_',conds{indCond},'_MSE_OUT_bp.mat']);
            mseMerged{indGroup,indCond}.MSEbp(indID,:,:) = mse.sampen(1:60,:);
            mseMerged{indGroup,indCond}.Rbp(indID,:,:) = squeeze(mse.r(1:60,:));
            % convert into powerRatios
            mseMerged{indGroup,indCond}.LPRatio(indID,:,:) = mseMerged{indGroup,indCond}.Rfiltskip(indID,:,:)./mseMerged{indGroup,indCond}.Rvanilla(indID,:,:);
            mseMerged{indGroup,indCond}.HPRatio(indID,:,:) = mseMerged{indGroup,indCond}.Rhp(indID,:,:)./mseMerged{indGroup,indCond}.Rvanilla(indID,:,:);
            % add info
            mseMerged{indGroup,indCond}.dimord = 'subj_chan_freq';
            mseMerged{indGroup,indCond}.label = mse.label(1:60);
            mseMerged{indGroup,indCond}.timescales = mse.timescales;
            mseMerged{indGroup,indCond}.freq = (1./[1:42])*250;
            mseMerged{indGroup,indCond}.time = mse.time;
            catch
                disp([curID, ' ',conds{indCond}, ' skipped']);
                continue
            end
        end
    end
end

%% show PSD/R with errorbars

pn.shadedError = ['/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/A_MSE_CSD_multiVariant/T_tools/shadedErrorBar']; addpath(pn.shadedError);

indCond = 2; channels = 1:60;

h = figure('units','normalized','position',[0 0 1 .7]);
subplot(2,5,1); hold on;
    curAverage = nanmean(mseMerged{1,indCond}.MSEVanilla(:, channels,:),2);
    standError = nanstd(curAverage,1)./sqrt(size(curAverage,1));
    l1 = shadedErrorBar([],nanmean(curAverage,1),standError, 'lineprops', {'r','linewidth', 1.5}, 'patchSaturation', .05);
    curAverage = nanmean(mseMerged{2,indCond}.MSEVanilla(:, channels,:),2);
    standError = nanstd(curAverage,1)./sqrt(size(curAverage,1));
    l2 = shadedErrorBar([],nanmean(curAverage,1),standError, 'lineprops', {'k','linewidth', 1.5}, 'patchSaturation', .05);
    xlim([1 41]); ylim([0 1.3]); title('Vanilla (r = .5, m = 2)');
    set(gca, 'XTick', 1:6:41); set(gca, 'XTickLabel', round(mseMerged{2,indCond}.freq(1:6:41))); xlabel('Scale [Hz]'); ylabel('Sample Entropy');
    legend([l1.mainLine, l2.mainLine],{'Young adults'; 'Older adults'}); legend('boxoff');
subplot(2,5,2); hold on;
    curAverage = nanmean(mseMerged{1,indCond}.MSEPointavg(:, channels,:),2);
    standError = nanstd(curAverage,1)./sqrt(size(curAverage,1));
    l1 = shadedErrorBar([],nanmean(curAverage,1),standError, 'lineprops', {'r','linewidth', 1.5}, 'patchSaturation', .05);
    curAverage = nanmean(mseMerged{2,indCond}.MSEPointavg(:, channels,:),2);
    standError = nanstd(curAverage,1)./sqrt(size(curAverage,1));
    l2 = shadedErrorBar([],nanmean(curAverage,1),standError, 'lineprops', {'k','linewidth', 1.5}, 'patchSaturation', .05);
    xlim([1 41]); ylim([0 1.3]); title('Vanilla + scale-wise R');
    set(gca, 'XTick', 1:6:41); set(gca, 'XTickLabel', round(mseMerged{2,indCond}.freq(1:6:41))); xlabel('Scale [Hz]'); ylabel('Sample Entropy');
subplot(2,5,3); hold on;
    curAverage = nanmean(mseMerged{1,indCond}.MSEfiltskip(:, channels,:),2);
    standError = nanstd(curAverage,1)./sqrt(size(curAverage,1));
    l1 = shadedErrorBar([],nanmean(curAverage,1),standError, 'lineprops', {'r','linewidth', 1.5}, 'patchSaturation', .05);
    curAverage = nanmean(mseMerged{2,indCond}.MSEfiltskip(:, channels,:),2);
    standError = nanstd(curAverage,1)./sqrt(size(curAverage,1));
    l2 = shadedErrorBar([],nanmean(curAverage,1),standError, 'lineprops', {'k','linewidth', 1.5}, 'patchSaturation', .05);
    xlim([1 41]); ylim([0 1.3]); title('Lowpass + scale-wise R');
    set(gca, 'XTick', 1:6:41); set(gca, 'XTickLabel', round(mseMerged{2,indCond}.freq(1:6:41))); xlabel('Scale [Hz]'); ylabel('Sample Entropy');
subplot(2,5,4); hold on;
    curAverage = nanmean(mseMerged{1,indCond}.MSEhp(:, channels,:),2);
    standError = nanstd(curAverage,1)./sqrt(size(curAverage,1));
    l1 = shadedErrorBar([],nanmean(curAverage,1),standError, 'lineprops', {'r','linewidth', 1.5}, 'patchSaturation', .05);
    curAverage = nanmean(mseMerged{2,indCond}.MSEhp(:, channels,:),2);
    standError = nanstd(curAverage,1)./sqrt(size(curAverage,1));
    l2 = shadedErrorBar([],nanmean(curAverage,1),standError, 'lineprops', {'k','linewidth', 1.5}, 'patchSaturation', .05);
    xlim([1 41]); ylim([0 1.3]); title('Highpass + scale-wise R');
    set(gca, 'XTick', 1:6:41); set(gca, 'XTickLabel', round(mseMerged{2,indCond}.freq(1:6:41))); xlabel('Scale [Hz]'); ylabel('Sample Entropy');
subplot(2,5,5); hold on;
    curAverage = nanmean(mseMerged{1,indCond}.MSEbp(:, channels,:),2);
    standError = nanstd(curAverage,1)./sqrt(size(curAverage,1));
    l1 = shadedErrorBar([],nanmean(curAverage,1),standError, 'lineprops', {'r','linewidth', 1.5}, 'patchSaturation', .05);
    curAverage = nanmean(mseMerged{2,indCond}.MSEbp(:, channels,:),2);
    standError = nanstd(curAverage,1)./sqrt(size(curAverage,1));
    l2 = shadedErrorBar([],nanmean(curAverage,1),standError, 'lineprops', {'k','linewidth', 1.5}, 'patchSaturation', .05);
    xlim([1 41]); ylim([0 1.3]); title('Bandpass + scale-wise R');
    set(gca, 'XTick', 1:6:41); set(gca, 'XTickLabel', round(mseMerged{2,indCond}.freq(1:6:41))); xlabel('Scale [Hz]'); ylabel('Sample Entropy');
subplot(2,5,5+1); hold on;
    curAverage = nanmean(mseMerged{1,indCond}.Rvanilla(:, channels,:),2);
    standError = nanstd(curAverage,1)./sqrt(size(curAverage,1));
    l1 = shadedErrorBar([],nanmean(curAverage,1),standError, 'lineprops', {'r','linewidth', 1.5}, 'patchSaturation', .05);
    curAverage = nanmean(mseMerged{2,indCond}.Rvanilla(:, channels,:),2);
    standError = nanstd(curAverage,1)./sqrt(size(curAverage,1));
    l2 = shadedErrorBar([],nanmean(curAverage,1),standError, 'lineprops', {'k','linewidth', 1.5}, 'patchSaturation', .05);
    xlim([1 41]); ylim([0 9]*10^-4); title('Vanilla (r = .5, m = 2)');
    set(gca, 'XTick', 1:6:41); set(gca, 'XTickLabel', round(mseMerged{2,indCond}.freq(1:6:41))); xlabel('Scale [Hz]'); ylabel('log10(R)');
    %legend([l1.mainLine, l2.mainLine],{'Young adults'; 'Older adults'}); legend('boxoff');
subplot(2,5,5+2); hold on;
    curAverage = nanmean(mseMerged{1,indCond}.Rpointavg(:, channels,:),2);
    standError = nanstd(curAverage,1)./sqrt(size(curAverage,1));
    l1 = shadedErrorBar([],nanmean(curAverage,1),standError, 'lineprops', {'r','linewidth', 1.5}, 'patchSaturation', .05);
    curAverage = nanmean(mseMerged{2,indCond}.Rpointavg(:, channels,:),2);
    standError = nanstd(curAverage,1)./sqrt(size(curAverage,1));
    l2 = shadedErrorBar([],nanmean(curAverage,1),standError, 'lineprops', {'k','linewidth', 1.5}, 'patchSaturation', .05);
    xlim([1 41]); ylim([0 9]*10^-4); title('Vanilla + scale-wise R');
    set(gca, 'XTick', 1:6:41); set(gca, 'XTickLabel', round(mseMerged{2,indCond}.freq(1:6:41))); xlabel('Scale [Hz]'); ylabel('R');
subplot(2,5,5+3); hold on;
    curAverage = nanmean(mseMerged{1,indCond}.Rfiltskip(:, channels,:),2);
    standError = nanstd(curAverage,1)./sqrt(size(curAverage,1));
    l1 = shadedErrorBar([],nanmean(curAverage,1),standError, 'lineprops', {'r','linewidth', 1.5}, 'patchSaturation', .05);
    curAverage = nanmean(mseMerged{2,indCond}.Rfiltskip(:, channels,:),2);
    standError = nanstd(curAverage,1)./sqrt(size(curAverage,1));
    l2 = shadedErrorBar([],nanmean(curAverage,1),standError, 'lineprops', {'k','linewidth', 1.5}, 'patchSaturation', .05);
    xlim([1 41]); ylim([0 9]*10^-4); title('Lowpass + scale-wise R');
    set(gca, 'XTick', 1:6:41); set(gca, 'XTickLabel', round(mseMerged{2,indCond}.freq(1:6:41))); xlabel('Scale [Hz]'); ylabel('R');
subplot(2,5,5+4); hold on;
    curAverage = nanmean(mseMerged{1,indCond}.Rhp(:, channels,:),2);
    standError = nanstd(curAverage,1)./sqrt(size(curAverage,1));
    l1 = shadedErrorBar([],nanmean(curAverage,1),standError, 'lineprops', {'r','linewidth', 1.5}, 'patchSaturation', .05);
    curAverage = nanmean(mseMerged{2,indCond}.Rhp(:, channels,:),2);
    standError = nanstd(curAverage,1)./sqrt(size(curAverage,1));
    l2 = shadedErrorBar([],nanmean(curAverage,1),standError, 'lineprops', {'k','linewidth', 1.5}, 'patchSaturation', .05);
    xlim([1 41]); ylim([0 9]*10^-4); title('Highpass + scale-wise R');
    set(gca, 'XTick', 1:6:41); set(gca, 'XTickLabel', round(mseMerged{2,indCond}.freq(1:6:41))); xlabel('Scale [Hz]'); ylabel('R');
subplot(2,5,5+5); hold on;
    curAverage = nanmean(mseMerged{1,indCond}.Rbp(:, channels,:),2);
    standError = nanstd(curAverage,1)./sqrt(size(curAverage,1));
    l1 = shadedErrorBar([],nanmean(curAverage,1),standError, 'lineprops', {'r','linewidth', 1.5}, 'patchSaturation', .05);
    curAverage = nanmean(mseMerged{2,indCond}.Rbp(:, channels,:),2);
    standError = nanstd(curAverage,1)./sqrt(size(curAverage,1));
    l2 = shadedErrorBar([],nanmean(curAverage,1),standError, 'lineprops', {'k','linewidth', 1.5}, 'patchSaturation', .05);
    xlim([1 41]); ylim([0 9]*10^-4); title('Bandpass + scale-wise R');
    set(gca, 'XTick', 1:6:41); set(gca, 'XTickLabel', round(mseMerged{2,indCond}.freq(1:6:41))); xlabel('Scale [Hz]'); ylabel('R');
set(findall(gcf,'-property','FontSize'),'FontSize',18)
pn.plotFolder = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/A_MSE_CSD_multiVariant/C_figures/';
figureName = 'FX_MSE_R_byMethod';

saveas(h, [pn.plotFolder, figureName], 'fig');
saveas(h, [pn.plotFolder, figureName], 'epsc');
saveas(h, [pn.plotFolder, figureName], 'png');

%% load PSD & compare

load('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/B_FFT/B_data/B_FFT_grandavg_individual.mat')

% figure; 
% subplot(1,2,1); imagesc(squeeze(nanmean(grandavg_EC.powspctrm,2))); title('Eyes Closed');
% subplot(1,2,2); imagesc(squeeze(nanmean(grandavg_EO.powspctrm,2))); title('Eyes Open');

commonYA = info.IDs_all(ismember(info.IDs_all, IDs{1}));
commonOA = info.IDs_all(ismember(info.IDs_all, IDs{2}));

idx_YA_fft = ismember(info.IDs_all, commonYA);
idx_OA_fft = ismember(info.IDs_all, commonOA);
idx_YA_mse = ismember(IDs{1}, commonYA);
idx_OA_mse = ismember(IDs{2}, commonOA);

pn.shadedError = ['/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/A_MSE_CSD_multiVariant/T_tools/shadedErrorBar']; addpath(pn.shadedError);

h = figure('units','normalized','position',[.1 .1 .5 .7]);
subplot(2,1,1); hold on;
    curAverage = nanmean(log10(grandavg_EC.powspctrm(idx_YA_fft,:,:)),2);
    standError = nanstd(curAverage,1)./sqrt(size(curAverage,1));
    shadedErrorBar(log10(grandavg_EC.freq),nanmean(curAverage,1),standError, 'lineprops', 'r', 'patchSaturation', .05);
    curAverage = nanmean(log10(grandavg_EC.powspctrm(idx_OA_fft,:,:)),2);
    standError = nanstd(curAverage,1)./sqrt(size(curAverage,1));
    shadedErrorBar(log10(grandavg_EC.freq),nanmean(curAverage,1),standError, 'lineprops', 'k', 'patchSaturation', .05);
    title('Eyes Closed');
    xlabel('Frequency (log10 Hz)'); ylabel('Power (log10)')
subplot(2,1,2); hold on;
    curAverage = nanmean(log10(grandavg_EO.powspctrm(idx_YA_fft,:,:)),2);
    standError = nanstd(curAverage,1)./sqrt(size(curAverage,1));
    l1 = shadedErrorBar(log10(grandavg_EC.freq),nanmean(curAverage,1),standError, 'lineprops', 'r', 'patchSaturation', .05);
    curAverage = nanmean(log10(grandavg_EO.powspctrm(idx_OA_fft,:,:)),2);
    standError = nanstd(curAverage,1)./sqrt(size(curAverage,1));
    l2 = shadedErrorBar(log10(grandavg_EC.freq),nanmean(curAverage,1),standError, 'lineprops', 'k', 'patchSaturation', .05);
    title('Eyes Open');
    legend([l1.mainLine, l2.mainLine],{'Young adults'; 'Older adults'});
    set(findall(gcf,'-property','FontSize'),'FontSize',18); legend('boxoff');
    xlabel('Frequency (log10 Hz)'); ylabel('Power (log10)')
pn.plotFolder = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/A_MSE_CSD_multiVariant/C_figures/';
figureName = 'F1_PSD';

saveas(h, [pn.plotFolder, figureName], 'fig');
saveas(h, [pn.plotFolder, figureName], 'epsc');
saveas(h, [pn.plotFolder, figureName], 'png');

%% contrast Vanilla MSE spectrum with PSD

indCond = 2; channels = 1:60;

pn.shadedError = ['/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/A_MSE_CSD_multiVariant/T_tools/shadedErrorBar']; addpath(pn.shadedError);

h = figure('units','normalized','position',[.1 .1 .4 .7]);
subplot(2,1,1); hold on;
    curAverage = nanmean(mseMerged{1,indCond}.MSEVanilla(:, channels,:),2);
    standError = nanstd(curAverage,1)./sqrt(size(curAverage,1));
    l1 = shadedErrorBar([],nanmean(curAverage,1),standError, 'lineprops', 'r', 'patchSaturation', .05);
    curAverage = nanmean(mseMerged{2,indCond}.MSEVanilla(:, channels,:),2);
    standError = nanstd(curAverage,1)./sqrt(size(curAverage,1));
    l2 = shadedErrorBar([],nanmean(curAverage,1),standError, 'lineprops', 'k', 'patchSaturation', .05);
    % mseMerged{1,indCond}.freq
    xlim([1 41]); 
    set(gca, 'XScale', 'log')
    scales = 1:41;
    set(gca, 'XTick',round(logspace(log10(1), log10(41), 8)));
    tmp_meas = [round(mseMerged{1,indCond}.freq(get(gca, 'XTick')),0); round(scales(get(gca, 'XTick')),0)];
    xLabels = [];
    for indScale = 1:size(tmp_meas,2)
        xLabels{indScale} = [num2str(tmp_meas(1,indScale)), ' (',num2str(tmp_meas(2,indScale)),')'];
    end; clear tmp_meas;
    set(gca, 'XTickLabels', xLabels);
    set(gca, 'XDir','reverse')
    legend([l1.mainLine, l2.mainLine],{'Young adults'; 'Older adults'}, 'location', 'NorthWest'); legend('boxoff')
    title('Eyes Open rest: Entropy spectrum'); xlabel('Time scale [Hz (scale); log-scaled]'); ylabel('Entropy')
    set(findall(gcf,'-property','FontSize'),'FontSize',18);
subplot(2,1,2); hold on;
    curAverage = nanmean(grandavg_EO.powspctrm(idx_YA_fft,:,:),2);
    standError = nanstd(curAverage,1)./sqrt(size(curAverage,1));
    l1 = shadedErrorBar(grandavg_EC.freq,nanmean(curAverage,1),standError, 'lineprops', 'r', 'patchSaturation', .05);
    curAverage = nanmean(grandavg_EO.powspctrm(idx_OA_fft,:,:),2);
    standError = nanstd(curAverage,1)./sqrt(size(curAverage,1));
    l2 = shadedErrorBar(grandavg_EC.freq,nanmean(curAverage,1),standError, 'lineprops', 'k', 'patchSaturation', .05);
    title('Eyes Open rest: Power spectrum');
    set(gca, 'XScale', 'log')
    xlim([2, 64])
    set(gca, 'XTick',grandavg_EC.freq(3:6:end));    
    legend([l1.mainLine, l2.mainLine],{'Young adults'; 'Older adults'});
    set(findall(gcf,'-property','FontSize'),'FontSize',18); legend('boxoff');
    xlabel('Frequency [Hz, log-scaled]'); ylabel('Power')
pn.plotFolder = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/A_MSE_CSD_multiVariant/C_figures/';
figureName = 'F1_MSE_PSD';
saveas(h, [pn.plotFolder, figureName], 'fig');
saveas(h, [pn.plotFolder, figureName], 'epsc');
saveas(h, [pn.plotFolder, figureName], 'png');

%% load PSD slopes

load('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/B_FFT/B_data/D_PSDslopes.mat')

%% correlation between spectral slope and entropy

% channels = 1:60;
% 
% h = figure('units','normalized','position',[.1 .1 .7 .4]);
% subplot(1,2,1); hold on;
%     x = squeeze(nanmedian(nanmean(mseMerged{1,2}.MSEVanilla(idx_YA_mse,channels,1),3),2));
%     y = squeeze(nanmedian(linFit_2_30_EO(idx_YA_fft, channels),2));
%     l1 = scatter(x,y, 'filled', 'k'); lsline;
%     [rl1,pl1] = corrcoef(x,y);
%     % add OA
%     x = squeeze(nanmedian(nanmean(mseMerged{2,2}.MSEVanilla(idx_OA_mse,channels,1),3),2));
%     y = squeeze(nanmedian(linFit_2_30_EO(idx_OA_fft, channels),2));
%     l2 = scatter(x,y, 'filled', 'r'); lsline;
%     [rl2,pl2] = corrcoef(x,y);
%     legend([l1, l2], {['Younger Adults: r=', num2str(round(rl1(2),2))], ...
%         ['Older Adults: r=', num2str(round(rl2(2),2))]}, 'Location', 'NorthWest'); legend('boxoff');
%     xlabel('Vanilla MSE, Scale 1'); ylabel('PSD Slope 2-64 Hz (excl. 7-13 Hz)');
%     title('Fast scale entropy reflects PSD slope')
% subplot(1,2,2); hold on;
%     x = squeeze(nanmedian(nanmean(mseMerged{1,2}.MSEVanilla(idx_YA_mse,channels,41),3),2));
%     y = squeeze(nanmedian(linFit_2_30_EO(idx_YA_fft, channels),2));
%     l1 = scatter(x,y, 'filled', 'k'); lsline;
%     [rl1,pl1] = corrcoef(x,y);
%     % add OA
%     x = squeeze(nanmedian(nanmean(mseMerged{2,2}.MSEVanilla(idx_OA_mse,channels,41),3),2));
%     y = squeeze(nanmedian(linFit_2_30_EO(idx_OA_fft, channels),2));
%     l2 = scatter(x,y, 'filled', 'r'); lsline;
%     [rl2,pl2] = corrcoef(x,y);
%     legend([l1, l2], {['Younger Adults: r=', num2str(round(rl1(2),2))], ...
%         ['Older Adults: r=', num2str(round(rl2(2),2))]}, 'Location', 'NorthEast'); legend('boxoff');
%     xlabel('Vanilla MSE, Scale 41'); ylabel('PSD Slope 2-64 Hz (excl. 7-13 Hz)');
%     title('Slow scale entropy inversely reflects PSD slope')
%     set(findall(gcf,'-property','FontSize'),'FontSize',18)
%     pn.plotFolder = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/A_MSE_CSD_multiVariant/C_figures/';
%     figureName = 'F2_WaschkeReplication';
%     saveas(h, [pn.plotFolder, figureName], 'epsc');
%     saveas(h, [pn.plotFolder, figureName], 'png');
    
    %% PSD correlations across methods
    % PSD slope is positively coupled with high freq entropy, negatively with low freq entropy

    metrics = {'MSEVanilla'; 'MSEPointavg'; 'MSEfiltskip'; 'MSEhp'; 'MSEbp'};
    channels = 1:60;
    metricLabels = {'Vanilla'; 'Vanilla + scale-wise R'; 'Lowpass + scale-wise R'; 'Highpass + scale-wise R'; 'Bandpass + scale-wise R'};
    indCond = 2;
    
    h = figure('units','normalized','position',[0 0 1 .4]);
    for indMetric = 1:5
    subplot(2,5,indMetric); hold on;
        x = squeeze(nanmedian(nanmean(mseMerged{1,indCond}.(metrics{indMetric})(idx_YA_mse,channels,1:3),3),2));
        y = squeeze(nanmedian(linFit_2_30_EO(idx_YA_fft, channels),2));
        l1 = scatter(x,y, 'filled', 'k'); y1_ls = polyval(polyfit(x,y,1),x); y1_ls = plot(x, y1_ls, 'Color', 'k');
        [rl1,pl1] = corrcoef(x,y);
        % add OA
        x = squeeze(nanmedian(nanmean(mseMerged{2,indCond}.(metrics{indMetric})(idx_OA_mse,channels,1:3),3),2));
        y = squeeze(nanmedian(linFit_2_30_EO(idx_OA_fft, channels),2));
        l2 = scatter(x,y, 'filled', 'r'); y2_ls = polyval(polyfit(x,y,1),x); y2_ls = plot(x, y2_ls, 'Color', 'r');
        [rl2,pl2] = corrcoef(x,y);
        legend([y1_ls, y2_ls], {['Younger Adults: r=', num2str(round(rl1(2),2)), ', p=',num2str(round(pl1(2),3))], ...
            ['Older Adults: r=', num2str(round(rl2(2),2)), ', p=',num2str(round(pl2(2),3))]}, 'Location', 'SouthEast'); legend('boxoff');
        xlabel('MSE, Scales 1:3'); ylabel('PSD Slope 2-64 Hz (excl. 7-13 Hz)');
        title([metricLabels{indMetric}, ': fast MSE'])
        ylim([-.06, 0.01]);
    subplot(2,5,5+indMetric); hold on;
        x = squeeze(nanmedian(nanmean(mseMerged{1,indCond}.(metrics{indMetric})(idx_YA_mse,channels,35:41),3),2));
        y = squeeze(nanmedian(linFit_2_30_EO(idx_YA_fft, channels),2));
        l1 = scatter(x,y, 'filled', 'k'); y1_ls = polyval(polyfit(x,y,1),x); y1_ls = plot(x, y1_ls, 'Color', 'k');
        [rl1,pl1] = corrcoef(x,y);
        % add OA
        x = squeeze(nanmedian(nanmean(mseMerged{2,indCond}.(metrics{indMetric})(idx_OA_mse,channels,35:41),3),2));
        y = squeeze(nanmedian(linFit_2_30_EO(idx_OA_fft, channels),2));
        l2 = scatter(x,y, 'filled', 'r'); y2_ls = polyval(polyfit(x,y,1),x); y2_ls = plot(x, y2_ls, 'Color', 'r');
        [rl2,pl2] = corrcoef(x,y);
        legend([y1_ls, y2_ls], {['Younger Adults: r=', num2str(round(rl1(2),2)), ', p=',num2str(round(pl1(2),3))], ...
            ['Older Adults: r=', num2str(round(rl2(2),2)), ', p=',num2str(round(pl2(2),3))]}, 'Location', 'SouthEast'); legend('boxoff');
        xlabel('MSE, Scales 35-41'); ylabel('PSD Slope 2-64 Hz (excl. 7-13 Hz)');
        title([metricLabels{indMetric}, ': coarse MSE'])
        ylim([-.06, 0.01]);
    end
    set(findall(gcf,'-property','FontSize'),'FontSize',15)
    pn.plotFolder = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/A_MSE_CSD_multiVariant/C_figures/';
    figureName = 'F2_PSDLinkAcrossMethods';
    saveas(h, [pn.plotFolder, figureName], 'epsc');
    saveas(h, [pn.plotFolder, figureName], 'png');
    
   
    %% assess point of PSD relation inversion
    
    h = figure('units','normalized','position',[0 0 .4 .4]);
    for indScale = 1:41
        cla;
        hold on;
        x = squeeze(nanmedian(nanmean(mseMerged{1,indCond}.MSEVanilla(idx_YA_mse,channels,indScale),3),2));
        y = squeeze(nanmedian(linFit_2_30_EC(idx_YA_fft, channels),2));
        [r, p] = corrcoef(x,y);
        RMat(1,indScale) = r(2);
        scatter(x,y, 'filled', 'k'); lsline;
        % add OA
        x = squeeze(nanmedian(nanmean(mseMerged{2,indCond}.MSEVanilla(idx_OA_mse,channels,indScale),3),2));
        y = squeeze(nanmedian(linFit_2_30_EC(idx_OA_fft, channels),2));
        [r, p] = corrcoef(x,y);
        RMat(2,indScale) = r(2);
        scatter(x,y, 'filled', 'r'); lsline;
        pause(.2)
    end
    
    h = figure('units','normalized','position',[0 0 .4 .4]);
    hold on; plot(squeeze(RMat(1,:)), 'LineWidth', 5); plot(squeeze(RMat(2,:)), 'LineWidth', 5); 
    legend({'YA', 'OA'}); legend('boxoff'); xlabel('Time scale'); ylabel('Correlation coefficient r')
    set(findall(gcf,'-property','FontSize'),'FontSize',18)
    title('Correlation estimates between SE and PSD slope')
    pn.plotFolder = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/A_MSE_CSD_multiVariant/C_figures/';
    figureName = 'F2_PSDCorrelationInversion';
    saveas(h, [pn.plotFolder, figureName], 'epsc');
    saveas(h, [pn.plotFolder, figureName], 'png');
    
    %% correlations using power
        
    metrics = {'MSEVanilla'; 'MSEPointavg'; 'MSEfiltskip'; 'MSEhp'; 'MSEbp'};
    channels = 1:60;
    metricLabels = {'Vanilla'; 'Vanilla + scale-wise R'; 'Lowpass + scale-wise R'; 'Highpass + scale-wise R'; 'Bandpass + scale-wise R'};
    indCond = 2;
    
    h = figure('units','normalized','position',[0 0 1 .4]);
    for indMetric = 1:5
    subplot(2,5,indMetric); hold on;
        x = squeeze(nanmedian(nanmean(mseMerged{1,indCond}.(metrics{indMetric})(idx_YA_mse,channels,1:3),3),2));
        y = squeeze(nanmedian(nanmean(grandavg_EO.powspctrm(idx_YA_fft,channels,1:16),3),2));
        l1 = scatter(x,y, 'filled', 'k'); y1_ls = polyval(polyfit(x,y,1),x); y1_ls = plot(x, y1_ls, 'Color', 'k');
        [rl1,pl1] = corrcoef(x,y);
        % add OA
        x = squeeze(nanmedian(nanmean(mseMerged{2,indCond}.(metrics{indMetric})(idx_OA_mse,channels,1:3),3),2));
        y = squeeze(nanmedian(nanmean(grandavg_EO.powspctrm(idx_OA_fft,channels,1:16),3),2));
        l2 = scatter(x,y, 'filled', 'r'); y2_ls = polyval(polyfit(x,y,1),x); y2_ls = plot(x, y2_ls, 'Color', 'r');
        [rl2,pl2] = corrcoef(x,y);
        legend([y1_ls, y2_ls], {['Younger Adults: r=', num2str(round(rl1(2),2)), ', p=',num2str(round(pl1(2),3))], ...
            ['Older Adults: r=', num2str(round(rl2(2),2)), ', p=',num2str(round(pl2(2),3))]}, 'Location', 'SouthEast'); legend('boxoff');
        xlabel('MSE, Scales 1:3'); ylabel('Power 2-8 Hz');
        title([metricLabels{indMetric}, ': fast MSE'])
        ylim([-2.5*10^-8, 2.5*10^-8]);
    subplot(2,5,5+indMetric); hold on;
        x = squeeze(nanmedian(nanmean(mseMerged{1,indCond}.(metrics{indMetric})(idx_YA_mse,channels,35:41),3),2));
        y = squeeze(nanmedian(nanmean(grandavg_EO.powspctrm(idx_YA_fft,channels,1:16),3),2));
        l1 = scatter(x,y, 'filled', 'k'); y1_ls = polyval(polyfit(x,y,1),x); y1_ls = plot(x, y1_ls, 'Color', 'k');
        [rl1,pl1] = corrcoef(x,y);
        % add OA
        x = squeeze(nanmedian(nanmean(mseMerged{2,indCond}.(metrics{indMetric})(idx_OA_mse,channels,35:41),3),2));
        y = squeeze(nanmedian(nanmean(grandavg_EO.powspctrm(idx_OA_fft,channels,1:16),3),2));
        l2 = scatter(x,y, 'filled', 'r'); y2_ls = polyval(polyfit(x,y,1),x); y2_ls = plot(x, y2_ls, 'Color', 'r');
        [rl2,pl2] = corrcoef(x,y);
        legend([y1_ls, y2_ls], {['Younger Adults: r=', num2str(round(rl1(2),2)), ', p=',num2str(round(pl1(2),3))], ...
            ['Older Adults: r=', num2str(round(rl2(2),2)), ', p=',num2str(round(pl2(2),3))]}, 'Location', 'SouthEast'); legend('boxoff');
        xlabel('MSE, Scales 35-41'); ylabel('Power 2-8 Hz');
        title([metricLabels{indMetric}, ': coarse MSE'])
        ylim([-2.5*10^-8, 2.5*10^-8]);
    end
    set(findall(gcf,'-property','FontSize'),'FontSize',15)

    pn.plotFolder = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/A_MSE_CSD_multiVariant/C_figures/';
    figureName = 'F2_LowFreqPowerCorrelation';
    saveas(h, [pn.plotFolder, figureName], 'epsc');
    saveas(h, [pn.plotFolder, figureName], 'png');
   
    
    h = figure('units','normalized','position',[0 0 1 .4]);
    for indMetric = 1:5
    subplot(2,5,indMetric); hold on;
        x = squeeze(nanmedian(nanmean(mseMerged{1,indCond}.(metrics{indMetric})(idx_YA_mse,channels,1:3),3),2));
        y = squeeze(nanmedian(nanmean(grandavg_EO.powspctrm(idx_YA_fft,channels,33:41),3),2));
        l1 = scatter(x,y, 'filled', 'k'); y1_ls = polyval(polyfit(x,y,1),x); y1_ls = plot(x, y1_ls, 'Color', 'k');
        [rl1,pl1] = corrcoef(x,y);
        % add OA
        x = squeeze(nanmedian(nanmean(mseMerged{2,indCond}.(metrics{indMetric})(idx_OA_mse,channels,1:3),3),2));
        y = squeeze(nanmedian(nanmean(grandavg_EO.powspctrm(idx_OA_fft,channels,33:41),3),2));
        l2 = scatter(x,y, 'filled', 'r'); y2_ls = polyval(polyfit(x,y,1),x); y2_ls = plot(x, y2_ls, 'Color', 'r');
        [rl2,pl2] = corrcoef(x,y);
        legend([y1_ls, y2_ls], {['Younger Adults: r=', num2str(round(rl1(2),2)), ', p=',num2str(round(pl1(2),3))], ...
            ['Older Adults: r=', num2str(round(rl2(2),2)), ', p=',num2str(round(pl2(2),3))]}, 'Location', 'SouthEast'); legend('boxoff');
        xlabel('MSE, Scales 1:3'); ylabel('Power 32-64 Hz');
        title([metricLabels{indMetric}, ': fast MSE'])
        ylim([-.5*10^-8, .5*10^-8]);
    subplot(2,5,5+indMetric); hold on;
        x = squeeze(nanmedian(nanmean(mseMerged{1,indCond}.(metrics{indMetric})(idx_YA_mse,channels,35:41),3),2));
        y = squeeze(nanmedian(nanmean(grandavg_EO.powspctrm(idx_YA_fft,channels,33:41),3),2));
        l1 = scatter(x,y, 'filled', 'k'); y1_ls = polyval(polyfit(x,y,1),x); y1_ls = plot(x, y1_ls, 'Color', 'k');
        [rl1,pl1] = corrcoef(x,y);
        % add OA
        x = squeeze(nanmedian(nanmean(mseMerged{2,indCond}.(metrics{indMetric})(idx_OA_mse,channels,35:41),3),2));
        y = squeeze(nanmedian(nanmean(grandavg_EO.powspctrm(idx_OA_fft,channels,33:41),3),2));
        l2 = scatter(x,y, 'filled', 'r'); y2_ls = polyval(polyfit(x,y,1),x); y2_ls = plot(x, y2_ls, 'Color', 'r');
        [rl2,pl2] = corrcoef(x,y);
        legend([y1_ls, y2_ls], {['Younger Adults: r=', num2str(round(rl1(2),2)), ', p=',num2str(round(pl1(2),3))], ...
            ['Older Adults: r=', num2str(round(rl2(2),2)), ', p=',num2str(round(pl2(2),3))]}, 'Location', 'SouthEast'); legend('boxoff');
        xlabel('MSE, Scales 35-41'); ylabel('Power 32-64 Hz');
        title([metricLabels{indMetric}, ': coarse MSE'])
        ylim([-.5*10^-8, .5*10^-8]);
        set(findall(gcf,'-property','FontSize'),'FontSize',13)
    end
    pn.plotFolder = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/A_MSE_CSD_multiVariant/C_figures/';
    figureName = 'F2_HighFreqPowerCorrelation';
    saveas(h, [pn.plotFolder, figureName], 'epsc');
    saveas(h, [pn.plotFolder, figureName], 'png');
    
    %% Explaining fast-scale MSE as ratio of r parameters
    
    % correlate R bias with MSE vanilla
    % low scale MSE should be approximated by R contrast (vanilla R / highpass R)
    % high scale MSE should be approximated by R contrast (vanilla R / lowpass R)
    
%     indCond = 2;
%     
%     figure
%     subplot(2,2,1); hold on;
%         x = squeeze(nanmedian(nanmean(mseMerged{1,indCond}.Rvanilla(idx_YA_mse,channels,1:3),3),2))./...
%             squeeze(nanmedian(nanmean(mseMerged{1,indCond}.Rhp(idx_YA_mse,channels,1:3),3),2));
%         y = squeeze(nanmedian(nanmean(mseMerged{1,indCond}.MSEVanilla(idx_YA_mse,channels,1:3),3),2));
%         scatter(x,y, 'filled', 'k'); lsline;
%         % add OA
%         x = squeeze(nanmedian(nanmean(mseMerged{2,indCond}.Rvanilla(idx_OA_mse,channels,1:3),3),2))./...
%             squeeze(nanmedian(nanmean(mseMerged{2,indCond}.Rhp(idx_OA_mse,channels,1:3),3),2));
%         y = squeeze(nanmedian(nanmean(mseMerged{2,indCond}.MSEVanilla(idx_OA_mse,channels,1:3),3),2));
%         scatter(x,y, 'filled', 'r'); lsline;
%     subplot(2,2,2); hold on;
%         x = squeeze(nanmedian(nanmean(mseMerged{1,indCond}.Rvanilla(idx_YA_mse,channels,35:41),3),2))./...
%             squeeze(nanmedian(nanmean(mseMerged{1,indCond}.Rfiltskip(idx_YA_mse,channels,35:41),3),2));
%         y = squeeze(nanmedian(nanmean(mseMerged{1,indCond}.MSEVanilla(idx_YA_mse,channels,35:41),3),2));
%         scatter(x,y, 'filled', 'k'); lsline;
%         % add OA
%         x = squeeze(nanmedian(nanmean(mseMerged{2,indCond}.Rvanilla(idx_OA_mse,channels,35:41),3),2))./...
%             squeeze(nanmedian(nanmean(mseMerged{2,indCond}.Rfiltskip(idx_OA_mse,channels,35:41),3),2));
%         y = squeeze(nanmedian(nanmean(mseMerged{2,indCond}.MSEVanilla(idx_OA_mse,channels,35:41),3),2));
%         scatter(x,y, 'filled', 'r'); lsline;
%     subplot(2,2,3); hold on;
%         x = squeeze(nanmedian(nanmean(mseMerged{1,indCond}.Rvanilla(idx_YA_mse,channels,1:3),3),2))./...
%             squeeze(nanmedian(nanmean(mseMerged{1,indCond}.Rhp(idx_YA_mse,channels,1:3),3),2));
%         y = squeeze(nanmedian(nanmean(mseMerged{1,indCond}.Rvanilla(idx_YA_mse,channels,1:3),3),2));
%         scatter(x,y, 'filled', 'k'); lsline;
%         % add OA
%         x = squeeze(nanmedian(nanmean(mseMerged{2,indCond}.Rvanilla(idx_OA_mse,channels,1:3),3),2))./...
%             squeeze(nanmedian(nanmean(mseMerged{2,indCond}.Rhp(idx_OA_mse,channels,1:3),3),2));
%         y = squeeze(nanmedian(nanmean(mseMerged{2,indCond}.Rvanilla(idx_OA_mse,channels,1:3),3),2));
%         scatter(x,y, 'filled', 'r'); lsline;
%     subplot(2,2,4); hold on;
%         x = squeeze(nanmedian(nanmean(mseMerged{1,indCond}.Rvanilla(idx_YA_mse,channels,35:41),3),2))./...
%             squeeze(nanmedian(nanmean(mseMerged{1,indCond}.Rfiltskip(idx_YA_mse,channels,35:41),3),2));
%         y = squeeze(nanmedian(nanmean(mseMerged{1,indCond}.Rvanilla(idx_YA_mse,channels,35:41),3),2));
%         scatter(x,y, 'filled', 'k'); lsline;
%         % add OA
%         x = squeeze(nanmedian(nanmean(mseMerged{2,indCond}.Rvanilla(idx_OA_mse,channels,35:41),3),2))./...
%             squeeze(nanmedian(nanmean(mseMerged{2,indCond}.Rfiltskip(idx_OA_mse,channels,35:41),3),2));
%         y = squeeze(nanmedian(nanmean(mseMerged{2,indCond}.Rvanilla(idx_OA_mse,channels,35:41),3),2));
%         scatter(x,y, 'filled', 'r'); lsline;
%     
   %% plot Vanilla MSE association with R parameter ratio
   
    h = figure('units','normalized','position',[.1 .1 .7 .4]);
    scales = 1:3; indCond = 2;
    subplot(1,2,1); hold on;
        x = squeeze(nanmedian(nanmean(mseMerged{1,indCond}.Rvanilla(idx_YA_mse,channels,scales),3),2))./...
            squeeze(nanmedian(nanmean(mseMerged{1,indCond}.Rhp(idx_YA_mse,channels,scales),3),2));
        y = squeeze(nanmedian(nanmean(mseMerged{1,indCond}.MSEVanilla(idx_YA_mse,channels,scales),3),2));
        l1 = scatter(x,y, 'filled', 'k'); lsline;
        [rl1,pl1] = corrcoef(x,y);
        % add OA
        x = squeeze(nanmedian(nanmean(mseMerged{2,indCond}.Rvanilla(idx_OA_mse,channels,scales),3),2))./...
            squeeze(nanmedian(nanmean(mseMerged{2,indCond}.Rhp(idx_OA_mse,channels, scales),3),2));
        y = squeeze(nanmedian(nanmean(mseMerged{2,indCond}.MSEVanilla(idx_OA_mse,channels,scales),3),2));
        l2 = scatter(x,y, 'filled', 'r'); lsline;
        [rl2,pl2] = corrcoef(x,y);
        title('Eyes open: Scales 1-3'); 
        legend([l1, l2], {['Younger Adults: r=', num2str(round(rl1(2),2))], ['Older Adults: r=', num2str(round(rl2(2),2))]}, 'Location', 'NorthWest'); legend('boxoff');
        xlabel('Ratio: lowpass R/highpass R'); ylabel('MSE Vanilla')
   subplot(1,2,2); hold on;
        indCond = 1;
        x = squeeze(nanmedian(nanmean(mseMerged{1,indCond}.Rvanilla(idx_YA_mse,channels,scales),3),2))./...
            squeeze(nanmedian(nanmean(mseMerged{1,indCond}.Rhp(idx_YA_mse,channels,scales),3),2));
        y = squeeze(nanmedian(nanmean(mseMerged{1,indCond}.MSEVanilla(idx_YA_mse,channels,scales),3),2));
        l1 = scatter(x,y, 'filled', 'k'); lsline;
        [rl1,pl1] = corrcoef(x,y);
        % add OA
        x = squeeze(nanmedian(nanmean(mseMerged{2,indCond}.Rvanilla(idx_OA_mse,channels,scales),3),2))./...
            squeeze(nanmedian(nanmean(mseMerged{2,indCond}.Rhp(idx_OA_mse,channels,scales),3),2));
        y = squeeze(nanmedian(nanmean(mseMerged{2,indCond}.MSEVanilla(idx_OA_mse,channels,scales),3),2));
        l2 = scatter(x,y, 'filled', 'r'); lsline;
        [rl2,pl2] = corrcoef(x,y);
        title('Eyes closed: Scales 1-3');
        legend([l1, l2], {['Younger Adults: r=', num2str(round(rl1(2),2))], ['Older Adults: r=', num2str(round(rl2(2),2))]}, 'Location', 'NorthWest'); legend('boxoff');
        xlabel('Ratio: lowpass R/highpass R'); ylabel('MSE Vanilla')
   set(findall(gcf,'-property','FontSize'),'FontSize',18)
   pn.plotFolder = '/Users/kosciessa/Desktop/mntTardis/STSWD_Rest/WIP_eeg/A_MSE_CSD_multiVariant/C_figures/';
   figureName = 'F3_FastMSEreflectsPowerRatio';

    saveas(h, [pn.plotFolder, figureName], 'epsc');
    saveas(h, [pn.plotFolder, figureName], 'png');
   
    %% plot Vanilla MSE association with R parameter ratio (inverted)
   
    h = figure('units','normalized','position',[.1 .1 .7 .4]);
    scales = 1:3; indCond = 2;
    subplot(1,2,1); hold on;
        x = squeeze(nanmedian(nanmean(mseMerged{1,indCond}.Rhp(idx_YA_mse,channels,scales),3),2))./...
            squeeze(nanmedian(nanmean(mseMerged{1,indCond}.Rvanilla(idx_YA_mse,channels,scales),3),2));
        y = squeeze(nanmedian(nanmean(mseMerged{1,indCond}.MSEVanilla(idx_YA_mse,channels,scales),3),2));
        l1 = scatter(-1.*x,y, 'filled', 'k'); lsline;
        [rl1,pl1] = corrcoef(-1.*x,y);
        % add OA
        x = squeeze(nanmedian(nanmean(mseMerged{2,indCond}.Rhp(idx_OA_mse,channels,scales),3),2))./...
            squeeze(nanmedian(nanmean(mseMerged{2,indCond}.Rvanilla(idx_OA_mse,2,channels, scales),3),2));
        y = squeeze(nanmedian(nanmean(mseMerged{2,indCond}.MSEVanilla(idx_OA_mse,channels,scales),3),2));
        l2 = scatter(-1.*x,y, 'filled', 'r'); lsline;
        [rl2,pl2] = corrcoef(-1.*x,y);
        title('Eyes open: Scales 1-3'); 
        legend([l1, l2], {['Younger Adults: r=', num2str(round(rl1(2),2))], ['Older Adults: r=', num2str(round(rl2(2),2))]}, 'Location', 'NorthWest'); legend('boxoff');
        xlabel('(-1*highpass R)/Vanilla(lowpass) R'); ylabel('MSE Vanilla')
   subplot(1,2,2); hold on;
        indCond = 2;
        x = squeeze(nanmedian(nanmean(mseMerged{1,indCond}.Rhp(idx_YA_mse,channels,scales),3),2))./...
            squeeze(nanmedian(nanmean(mseMerged{1,indCond}.Rvanilla(idx_YA_mse,channels,scales),3),2));
        y = squeeze(nanmedian(nanmean(mseMerged{1,indCond}.MSEVanilla(idx_YA_mse,channels,scales),3),2));
        l1 = scatter(-1.*x,y, 'filled', 'k'); lsline;
        [rl1,pl1] = corrcoef(-1.*x,y);
        % add OA
        x = squeeze(nanmedian(nanmean(mseMerged{2,indCond}.Rhp(idx_OA_mse,channels,scales),3),2))./...
            squeeze(nanmedian(nanmean(mseMerged{2,indCond}.Rvanilla(idx_OA_mse,channels,scales),3),2));
        y = squeeze(nanmedian(nanmean(mseMerged{2,indCond}.MSEVanilla(idx_OA_mse,channels,scales),3),2));
        l2 = scatter(-1.*x,y, 'filled', 'r'); lsline;
        [rl2,pl2] = corrcoef(-1.*x,y);
        title('Eyes closed: Scales 1-3');
        legend([l1, l2], {['Younger Adults: r=', num2str(round(rl1(2),2))], ['Older Adults: r=', num2str(round(rl2(2),2))]}, 'Location', 'NorthWest'); legend('boxoff');
        xlabel('(-1*highpass R)/Vanilla(lowpass) R'); ylabel('MSE Vanilla')
   set(findall(gcf,'-property','FontSize'),'FontSize',18)
   pn.plotFolder = '/Users/kosciessa/Desktop/mntTardis/STSWD_Rest/WIP_eeg/A_MSE_CSD_multiVariant/C_figures/';
   figureName = 'F3_FastMSEreflectsPowerRatio';

    saveas(h, [pn.plotFolder, figureName], 'epsc');
    saveas(h, [pn.plotFolder, figureName], 'png');
    
    %% plot association with power ratio
    
    h = figure('units','normalized','position',[.1 .1 .7 .4]);
    scales = 1:3;
    subplot(1,2,1); hold on;
        x = squeeze(nanmean(nanmean(grandavg_EO.powspctrm(idx_YA_fft,channels,30:41),3),2))./...
            squeeze(nanmean(nanmean(grandavg_EO.powspctrm(idx_YA_fft,channels,1:5),3),2));
        y = squeeze(nanmedian(nanmean(mseMerged{1,indCond}.MSEVanilla(idx_YA_mse,channels,scales),3),2));
        l1 = scatter(x,y, 'filled', 'k'); lsline;
        [rl1,pl1] = corrcoef(x,y);
        % add OA
        x = squeeze(nanmean(nanmean(grandavg_EO.powspctrm(idx_OA_fft,channels,30:41),3),2))./...
            squeeze(nanmean(nanmean(grandavg_EO.powspctrm(idx_OA_fft,channels,1:5),3),2));
        y = squeeze(nanmedian(nanmean(mseMerged{2,indCond}.MSEVanilla(idx_OA_mse,channels,scales),3),2));
        l2 = scatter(x,y, 'filled', 'r'); lsline;
        [rl2,pl2] = corrcoef(x,y);
        title('Eyes open: Scales 1-3'); 
        legend([l1, l2], {['Younger Adults: r=', num2str(round(rl1(2),2))], ['Older Adults: r=', num2str(round(rl2(2),2))]}, 'Location', 'NorthWest'); legend('boxoff');
        xlabel('Ratio: high freq Power/low freq power'); ylabel('MSE Vanilla')
   subplot(1,2,2); hold on;
        x = squeeze(nanmean(nanmean(grandavg_EC.powspctrm(idx_YA_fft,channels,30:41),3),2))./...
            squeeze(nanmean(nanmean(grandavg_EC.powspctrm(idx_YA_fft,channels,1:5),3),2));
        y = squeeze(nanmedian(nanmean(mseMerged{1,indCond}.MSEVanilla(idx_YA_mse,channels,scales),3),2));
        l1 = scatter(x,y, 'filled', 'k'); lsline;
        [rl1,pl1] = corrcoef(x,y);
        % add OA
        x = squeeze(nanmean(nanmean(grandavg_EC.powspctrm(idx_OA_fft,channels,30:41),3),2))./...
            squeeze(nanmean(nanmean(grandavg_EC.powspctrm(idx_OA_fft,channels,1:5),3),2));
        y = squeeze(nanmedian(nanmean(mseMerged{2,indCond}.MSEVanilla(idx_OA_mse,channels,scales),3),2));
        l2 = scatter(x,y, 'filled', 'r'); lsline;
        [rl2,pl2] = corrcoef(x,y);
        title('Eyes closed: Scales 1-3');
        legend([l1, l2], {['Younger Adults: r=', num2str(round(rl1(2),2))], ['Older Adults: r=', num2str(round(rl2(2),2))]}, 'Location', 'NorthWest'); legend('boxoff');
        xlabel('Ratio: low freq Power/high freq power'); ylabel('MSE Vanilla')
   set(findall(gcf,'-property','FontSize'),'FontSize',18)
   pn.plotFolder = '/Users/kosciessa/Desktop/mntTardis/STSWD_Rest/WIP_eeg/A_MSE_CSD_multiVariant/C_figures/';
   figureName = 'F3_FastMSEreflectsInversePowerRatio';
  
   saveas(h, [pn.plotFolder, figureName], 'epsc');
   saveas(h, [pn.plotFolder, figureName], 'png');
   
   %% interindividual correlations: PSD difference vs. MSE difference
   
   % hypothesis: difference between scale-wise R parameter and global r
   % parameter predicts difference between vanilla MSE and filtskip MSE
   
   indCond = 2;
    x = squeeze(nanmean(nanmean(mseMerged{1,indCond}.Rfiltskip(:,:,1:3),3),1))-...
            squeeze(nanmean(nanmean(mseMerged{1,indCond}.Rvanilla(:,:,1:3),3),1));
    y = squeeze(nanmedian(nanmean(mseMerged{1,indCond}.MSEfiltskip(:,:,1:3),3),1))-...
            squeeze(nanmedian(nanmean(mseMerged{1,indCond}.MSEVanilla(:,:,1:3),3),1));
   figure; scatter(x(1:60),y(1:60), 'filled')
 
   h = figure('units','normalized','position',[.1 .1 .9 .4]);
   subplot(1,3,1); 
       x = squeeze(nanmean(nanmean(mseMerged{1,indCond}.Rfiltskip(:,:,39:41),3),1));
       y = squeeze(nanmean(nanmean(mseMerged{1,indCond}.Rvanilla(:,:,39:41),3),1));
       hold on; scatter(x(1:60),y(1:60), 'filled')
       x = squeeze(nanmean(nanmean(mseMerged{2,indCond}.Rfiltskip(:,:,39:41),3),1));
       y = squeeze(nanmean(nanmean(mseMerged{2,indCond}.Rvanilla(:,:,39:41),3),1));
       scatter(x(1:60),y(1:60), 'filled')
       xlabel('R filtskip'); ylabel('R Vanilla');
       title('Interindividual association between R')
   subplot(1,3,2); 
       x = squeeze(nanmean(nanmean(mseMerged{1,indCond}.MSEfiltskip(:,:,39:41),3),1));
       y = squeeze(nanmean(nanmean(mseMerged{1,indCond}.MSEVanilla(:,:,39:41),3),1));
       hold on; scatter(x(1:60),y(1:60), 'filled')
       x = squeeze(nanmean(nanmean(mseMerged{2,indCond}.MSEfiltskip(:,:,39:41),3),1));
       y = squeeze(nanmean(nanmean(mseMerged{2,indCond}.MSEVanilla(:,:,39:41),3),1));
       scatter(x(1:60),y(1:60), 'filled')
       xlabel('MSE filtskip'); ylabel('MSE Vanilla');
       title('Interindividual association between Entropy')
   subplot(1,3,3); 
       x = squeeze(nanmean(nanmean(mseMerged{1,indCond}.Rfiltskip(:,:,39:41),3),1))-...
                squeeze(nanmean(nanmean(mseMerged{1,indCond}.Rvanilla(:,:,39:41),3),1));
       y = squeeze(nanmedian(nanmean(mseMerged{1,indCond}.MSEfiltskip(:,:,39:41),3),1))-...
                squeeze(nanmedian(nanmean(mseMerged{1,indCond}.MSEVanilla(:,:,39:41),3),1));
       hold on; scatter(x(1:60),y(1:60), 'filled')
       x = squeeze(nanmean(nanmean(mseMerged{2,indCond}.Rfiltskip(:,:,39:41),3),1))-...
                squeeze(nanmean(nanmean(mseMerged{2,indCond}.Rvanilla(:,:,39:41),3),1));
       y = squeeze(nanmedian(nanmean(mseMerged{2,indCond}.MSEfiltskip(:,:,39:41),3),1))-...
                squeeze(nanmedian(nanmean(mseMerged{2,indCond}.MSEVanilla(:,:,39:41),3),1));
       scatter(x(1:60),y(1:60), 'filled')
   title('Interindividual shifts in R and MSE')
   xlabel('Shift in R parameter: filtskip-Vanilla'); ylabel('Shift in MSE: filtskip-Vanilla');
   suptitle('Scale-wise re-estimation of the r parameter can account for differences in entropy (slow scales)')
   legend({'YA', 'OA'}); legend('boxoff');
   set(findall(gcf,'-property','FontSize'),'FontSize',18)
   pn.plotFolder = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/A_MSE_CSD_multiVariant/C_figures/';
   figureName = 'F_LowFreqShiftEntropyR';
   saveas(h, [pn.plotFolder, figureName], 'fig');
   saveas(h, [pn.plotFolder, figureName], 'epsc');
   saveas(h, [pn.plotFolder, figureName], 'png');
   
   % compare vanilla-filtskip to estimate the bias
   
   h = figure('units','normalized','position',[.1 .1 .9 .4]);
   subplot(1,3,1); 
       x = squeeze(nanmean(nanmean(mseMerged{1,indCond}.Rvanilla(:,2,1:60,20:41),3),2));
       y = squeeze(nanmean(nanmean(mseMerged{1,indCond}.Rfiltskip(:,2,1:60,20:41),3),2));
       hold on; scatter(x,y, 'filled')
       x = squeeze(nanmean(nanmean(mseMerged{2,indCond}.Rvanilla(:,2,1:60,20:41),3),2));
       y = squeeze(nanmean(nanmean(mseMerged{2,indCond}.Rfiltskip(:,2,1:60,20:41),3),2));
       scatter(x,y, 'filled')
       xlabel('R Vanilla'); ylabel('R filtskip');
       title('Interindividual association between R')
   subplot(1,3,2); 
       x = squeeze(nanmean(nanmean(mseMerged{1,indCond}.MSEVanilla(:,2,1:60,20:41),3),2));
       y = squeeze(nanmean(nanmean(mseMerged{1,indCond}.MSEfiltskip(:,2,1:60,20:41),3),2));
       hold on; scatter(x,y, 'filled')
       x = squeeze(nanmean(nanmean(mseMerged{2,indCond}.MSEVanilla(:,2,1:60,20:41),3),2));
       y = squeeze(nanmean(nanmean(mseMerged{2,indCond}.MSEfiltskip(:,2,1:60,20:41),3),2));
       scatter(x,y, 'filled')
       xlabel('MSE Vanilla'); ylabel('MSE filtskip');
       title('Interindividual association between Entropy')
   subplot(1,3,3); 
       x = squeeze(nanmean(nanmean(mseMerged{1,indCond}.Rvanilla(:,2,1:60,20:41),3),2))-...
                squeeze(nanmean(nanmean(mseMerged{1,indCond}.Rfiltskip(:,2,1:60,20:41),3),2));
       y = squeeze(nanmedian(nanmean(mseMerged{1,indCond}.MSEVanilla(:,2,1:60,20:41),3),2))-...
                squeeze(nanmedian(nanmean(mseMerged{1,indCond}.MSEfiltskip(:,2,1:60,20:41),3),2));
       hold on; scatter(x,y, 'filled')
       x = squeeze(nanmean(nanmean(mseMerged{2,indCond}.Rvanilla(:,2,1:60,20:41),3),2))-...
                squeeze(nanmean(nanmean(mseMerged{2,indCond}.Rfiltskip(:,2,1:60,20:41),3),2));
       y = squeeze(nanmedian(nanmean(mseMerged{2,indCond}.MSEVanilla(:,2,1:60,20:41),3),2))-...
                squeeze(nanmedian(nanmean(mseMerged{2,indCond}.MSEfiltskip(:,2,1:60,20:41),3),2));
       scatter(x,y, 'filled')
   title('Interindividual shifts in R and MSE')
   xlabel('Shift in R parameter: Vanilla-filtskip'); ylabel('Shift in MSE: Vanilla-filtskip');
   suptitle('Scale-wise bias in r parameter can account for differences in entropy at slow time scales')
   legend({'YA', 'OA'}); legend('boxoff');
   set(findall(gcf,'-property','FontSize'),'FontSize',18)
   pn.plotFolder = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/A_MSE_CSD_multiVariant/C_figures/';
   figureName = 'F_LowFreqShiftEntropyR';
   saveas(h, [pn.plotFolder, figureName], 'fig');
   saveas(h, [pn.plotFolder, figureName], 'epsc');
   saveas(h, [pn.plotFolder, figureName], 'png');
   
   %% attempt to get a scale-wise representation of the bias
   
   for indScale = 1:41
       x = squeeze(nanmean(nanmean(mseMerged{1,indCond}.Rvanilla(:,2,:,indScale),4),1))-...
                squeeze(nanmean(nanmean(mseMerged{1,indCond}.Rfiltskip(:,2,:,indScale),4),1));
       y = squeeze(nanmedian(nanmean(mseMerged{1,indCond}.MSEVanilla(:,2,:,indScale),4),1))-...
                squeeze(nanmedian(nanmean(mseMerged{1,indCond}.MSEfiltskip(:,2,:,indScale),4),1));
       RMat(1,indScale) = nanmean(x);
       RMat(2,indScale) = nanmean(y);
       x = squeeze(nanmean(nanmean(mseMerged{2,indCond}.Rvanilla(:,2,:,indScale),4),1))-...
                squeeze(nanmean(nanmean(mseMerged{2,indCond}.Rfiltskip(:,2,:,indScale),4),1));
       y = squeeze(nanmedian(nanmean(mseMerged{2,indCond}.MSEVanilla(:,2,:,indScale),4),1))-...
                squeeze(nanmedian(nanmean(mseMerged{2,indCond}.MSEfiltskip(:,2,:,indScale),4),1));
       RMat(3,indScale) = nanmean(x);
       RMat(4,indScale) = nanmean(y);
   end
   figure; hold on; plot(RMat(1,:));  plot(RMat(3,:))
   hold on; plot(RMat(2,:));  plot(RMat(4,:))
   hold on; plot(squeeze(nanmedian(nanmean(mseMerged{1,indCond}.MSEVanilla(:,2,:,:),3),1)))
   plot(squeeze(nanmedian(nanmean(mseMerged{2,indCond}.MSEVanilla(:,2,:,:),3),1)))   
   
   % predicts difference between channels with strong age difference and no
   % age difference
   
   x = squeeze(nanmean(nanmean(grandavg_EO.powspctrm(idx_OA_fft,:,30:41),3),1))-...
            squeeze(nanmean(nanmean(grandavg_EO.powspctrm(idx_YA_fft,:,30:41),3),1));
    y = squeeze(nanmedian(nanmean(mseMerged{2,indCond}.MSEVanilla(:,2,:,1),4),1))-...
            squeeze(nanmedian(nanmean(mseMerged{1,indCond}.MSEVanilla(:,2,:,1),4),1));
   
   
   
   % slow MSE vs. fast PSD
   
    x = squeeze(nanmean(nanmean(grandavg_EO.powspctrm(idx_OA_fft,:,30:41),3),1))-...
            squeeze(nanmean(nanmean(grandavg_EO.powspctrm(idx_YA_fft,:,30:41),3),1));
    y = squeeze(nanmedian(nanmean(mseMerged{2,indCond}.MSEVanilla(:,2,:,1),4),1))-...
            squeeze(nanmedian(nanmean(mseMerged{1,indCond}.MSEVanilla(:,2,:,1),4),1));
   
   
    figure; 
    x = squeeze(nanmean(nanmean(grandavg_EO.powspctrm(idx_OA_fft,:,1:5),3),2));
    y = squeeze(nanmedian(nanmean(mseMerged{2,indCond}.MSEVanilla(:,2,:,1:3),3),2));
    subplot(2,2,1); scatter(x,y, 'filled')
    x = squeeze(nanmean(nanmean(grandavg_EO.powspctrm(idx_YA_fft,:,1:5),3),2));
    y = squeeze(nanmedian(nanmean(mseMerged{1,indCond}.MSEVanilla(:,2,:,1:3),3),2));
    subplot(2,2,2); scatter(x,y, 'filled')
    x = squeeze(nanmean(nanmean(grandavg_EO.powspctrm(idx_OA_fft,:,35:41),3),2));
    y = squeeze(nanmedian(nanmean(mseMerged{2,indCond}.MSEVanilla(:,2,:,35:41),3),2));
    subplot(2,2,3); scatter(x,y, 'filled')
    x = squeeze(nanmean(nanmean(grandavg_EO.powspctrm(idx_YA_fft,:,35:41),3),2));
    y = squeeze(nanmedian(nanmean(mseMerged{1,indCond}.MSEVanilla(:,2,:,35:41),3),2));
    subplot(2,2,4); scatter(x,y, 'filled')
    
    figure; 
    x = squeeze(nanmean(nanmean(grandavg_EO.powspctrm(idx_OA_fft,:,1:5),3),2));
    y = squeeze(nanmedian(nanmean(mseMerged{2,indCond}.Rvanilla(:,2,:,1:3),3),2));
    subplot(2,2,1); scatter(x,y, 'filled')
    x = squeeze(nanmean(nanmean(grandavg_EO.powspctrm(idx_YA_fft,:,1:5),3),2));
    y = squeeze(nanmedian(nanmean(mseMerged{1,indCond}.Rvanilla(:,2,:,1:3),3),2));
    subplot(2,2,2); scatter(x,y, 'filled')
    x = squeeze(nanmean(nanmean(grandavg_EO.powspctrm(idx_OA_fft,:,35:41),3),2));
    y = squeeze(nanmedian(nanmean(mseMerged{2,indCond}.Rvanilla(:,2,:,35:41),3),2));
    subplot(2,2,3); scatter(x,y, 'filled')
    x = squeeze(nanmean(nanmean(grandavg_EO.powspctrm(idx_YA_fft,:,35:41),3),2));
    y = squeeze(nanmedian(nanmean(mseMerged{1,indCond}.Rvanilla(:,2,:,35:41),3),2));
    subplot(2,2,4); scatter(x,y, 'filled')
    
    figure; 
    x = squeeze(nanmean(nanmean(grandavg_EO.powspctrm(idx_OA_fft,:,1:3),3),2))./squeeze(nanmean(nanmean(grandavg_EO.powspctrm(idx_OA_fft,:,1:41),3),2));
    y = squeeze(nanmedian(nanmean(mseMerged{2,indCond}.MSEVanilla(:,2,:,1:3),3),2));
    subplot(2,2,1); scatter(x,y, 'filled')
    x = squeeze(nanmean(nanmean(grandavg_EO.powspctrm(idx_YA_fft,:,1:3),3),2))./squeeze(nanmean(nanmean(grandavg_EO.powspctrm(idx_YA_fft,:,1:41),3),2));
    y = squeeze(nanmedian(nanmean(mseMerged{1,indCond}.Rvanilla(:,2,:,1:3),3),2));
    subplot(2,2,2); scatter(x,y, 'filled')
    x = squeeze(nanmean(nanmean(grandavg_EO.powspctrm(idx_OA_fft,:,35:41),3),2))./squeeze(nanmean(nanmean(grandavg_EO.powspctrm(idx_OA_fft,:,1:41),3),2));
    y = squeeze(nanmedian(nanmean(mseMerged{2,indCond}.Rvanilla(:,2,:,35:41),3),2));
    subplot(2,2,3); scatter(x,y, 'filled')
    x = squeeze(nanmean(nanmean(grandavg_EO.powspctrm(idx_YA_fft,:,35:41),3),2))./squeeze(nanmean(nanmean(grandavg_EO.powspctrm(idx_YA_fft,:,1:41),3),2));
    y = squeeze(nanmedian(nanmean(mseMerged{1,indCond}.Rvanilla(:,2,:,35:41),3),2));
    subplot(2,2,4); scatter(x,y, 'filled')
        
   % fast MSE vs. slow PSD
   
   %% coarse MSE: influence of high frequency power
   
   % plot scale-wise R vs global R by scale
   
   h = figure('units','normalized','position',[.1 .1 .9 .4]);
   subplot(2,3,1); hold on;
       x = squeeze(nanmean(nanmean(mseMerged{1,indCond}.Rvanilla(:,1:60,:),2),1));
       l1 = plot(x, 'k.', 'LineWidth', 2);
       x = squeeze(nanmean(nanmean(mseMerged{1,indCond}.Rpointavg(:,1:60,:),2),1));
       l2 = plot(x, 'k-', 'LineWidth', 2);
       %l1 = scatter(x,y, 'filled', 'k'); y1_ls = polyval(polyfit(x,y,1),x); y1_ls = plot(x, y1_ls, 'Color', 'k');
       %[rl1,pl1] = corrcoef(x,y);
       x = squeeze(nanmean(nanmean(mseMerged{2,indCond}.Rvanilla(:,1:60,:),2),1));
       l3 = plot(x,'r--', 'LineWidth', 2);
       x = squeeze(nanmean(nanmean(mseMerged{2,indCond}.Rpointavg(:,1:60,:),2),1));
       l4 = plot(x,'r-', 'LineWidth', 2);
       ylabel('Similarity criterion (r)'); xlabel('Time scale [Hz(scale); log-scaled]')
       set(gca, 'XScale', 'log')
        scales = 1:41;
        set(gca, 'XTick',round(logspace(log10(1), log10(30), 6)));
        tmp_meas = [round(mseMerged{1,indCond}.freq(get(gca, 'XTick')),0); round(scales(get(gca, 'XTick')),0)];
        xLabels = [];
        for indScale = 1:size(tmp_meas,2)
            xLabels{indScale} = [num2str(tmp_meas(1,indScale)), ' (',num2str(tmp_meas(2,indScale)),')'];
        end; clear tmp_meas;
        set(gca, 'XTickLabels', xLabels);
        set(gca, 'XDir','reverse')
       lgd1 = legend([l1,l2,l3,l4], {'YA: global R'; 'YA: scale-wise R'; 'OA: global R'; 'OA: scale-wise R'}, 'location', 'SouthEast', 'FontSize', 9); legend('boxoff');
       xlim([1 41]);
       title({'Discrepancy between global and local';'R reflects power spectrum'})
   subplot(2,3,4); hold on;
       x = squeeze(nanmean(nanmean(log10(grandavg_EO.powspctrm(idx_YA_fft,:,:)),2),1));
       l2 = plot(grandavg_EC.freq,x,'k-', 'LineWidth', 2);
       x = squeeze(nanmean(nanmean(log10(grandavg_EO.powspctrm(idx_OA_fft,:,:)),2),1));
       l4 = plot(grandavg_EC.freq,x,'r-', 'LineWidth', 2);
       x = repmat(squeeze(nanmean(nanmean(nanmean(log10(grandavg_EO.powspctrm(idx_YA_fft,:,:)),3),2),1)),1,41);
       l1 = plot(grandavg_EC.freq,x,'k.', 'LineWidth', 2);
       x = repmat(squeeze(nanmean(nanmean(nanmean(log10(grandavg_EO.powspctrm(idx_OA_fft,:,:)),3),2),1)),1,41);
       l3 = plot(grandavg_EC.freq,x,'r--', 'LineWidth', 2);
       set(gca, 'XScale', 'log')
    xlim([2, 64])
    set(gca, 'XTick',grandavg_EC.freq(3:6:end)); 
       lgd2 = legend([l1,l2,l3,l4], {'YA: mean power'; 'YA: frequency-specific R'; 'OA: mean power'; 'OA: frequency-specific power'}, 'location', 'SouthWest', 'FontSize', 9); legend('boxoff');
       ylabel('Power (log10)'); xlabel('Frequency (Hz; log-scaled)')
       xlim([2, 64]);

   % Let's add a nice illustration of the imbalance between scale-wise
   % variance and R
   
   %h = figure('units','normalized','position',[.1 .1 .7 .4]);
   subplot(2,3,[2,5]); hold on;
       x = squeeze(nanmean(nanmean(mseMerged{1,indCond}.Rpointavg(:,1:60,39:41)./mseMerged{1,indCond}.Rvanilla(:,1:60,39:41),3),2));
       y = squeeze(nanmean(nanmean(mseMerged{1,indCond}.MSEPointavg(:,1:60,39:41)-mseMerged{1,indCond}.MSEVanilla(:,1:60,39:41),3),2));
       l1 = scatter(x,y, 'filled', 'k'); y1_ls = polyval(polyfit(x,y,1),x); y1_ls = plot(x, y1_ls, 'Color', 'k');
       [rl1,pl1] = corrcoef(x,y);
       x = squeeze(nanmean(nanmean(mseMerged{2,indCond}.Rpointavg(:,1:60,39:41)./mseMerged{2,indCond}.Rvanilla(:,1:60,39:41),3),2));
       y = squeeze(nanmean(nanmean(mseMerged{2,indCond}.MSEPointavg(:,1:60,39:41)-mseMerged{2,indCond}.MSEVanilla(:,1:60,39:41),3),2));
       title('Interindividual shifts in R and MSE')
       xlabel('Ratio of R parameters: Rescaled R vs. global R'); ylabel('Difference in MSE: Rescaled R vs. global R');
       l2 = scatter(x,y, 'filled', 'r'); y2_ls = polyval(polyfit(x,y,1),x); y2_ls = plot(x, y2_ls, 'Color', 'r');
       [rl2,pl2] = corrcoef(x,y);
       legend([y1_ls, y2_ls], {['Younger Adults: r=', num2str(round(rl1(2),2)), ', p=',num2str(round(pl1(2),3))], ...
        ['Older Adults: r=', num2str(round(rl2(2),2)), ', p=',num2str(round(pl2(2),3))]}, 'Location', 'NorthEast'); legend('boxoff');
       set(findall(gcf,'-property','FontSize'),'FontSize',18)
       xlim([0,1]); ylim([0 1])
%    subplot(1,3,2); hold on;
%        x = squeeze(nanmedian(linFit_2_30_EC(idx_YA_fft, channels),2));
%        y = squeeze(nanmedian(nanmean(mseMerged{1,indCond}.MSEfiltskip(:,2,1:60,39:41),3),2))-...
%                 squeeze(nanmedian(nanmean(mseMerged{1,indCond}.MSEVanilla(:,2,1:60,39:41),3),2));
%        l1 = scatter(x,y, 'filled', 'k'); y1_ls = polyval(polyfit(x,y,1),x); y1_ls = plot(x, y1_ls, 'Color', 'k');
%        [rl1,pl1] = corrcoef(x,y);
%        x = squeeze(nanmedian(linFit_2_30_EC(idx_OA_fft, channels),2));
%        y = squeeze(nanmedian(nanmean(mseMerged{2,indCond}.MSEfiltskip(:,2,1:60,39:41),3),2))-...
%                 squeeze(nanmedian(nanmean(mseMerged{2,indCond}.MSEVanilla(:,2,1:60,39:41),3),2));
%        l2 = scatter(x,y, 'filled', 'r'); y2_ls = polyval(polyfit(x,y,1),x); y2_ls = plot(x, y2_ls, 'Color', 'r');
%        [rl2,pl2] = corrcoef(x,y);
%        legend([y1_ls, y2_ls], {['Younger Adults: r=', num2str(round(rl1(2),2)), ', p=',num2str(round(pl1(2),3))], ...
%         ['Older Adults: r=', num2str(round(rl2(2),2)), ', p=',num2str(round(pl2(2),3))]}, 'Location', 'NorthEast'); legend('boxoff');
%        title('PSD Slope predicts MSE shift')
%        xlabel('PSD slope'); ylabel('Shift in MSE: filtskip-Vanilla');
%        set(findall(gcf,'-property','FontSize'),'FontSize',18)
%        ylim([0 1])
   subplot(2,3,[3,6]); hold on;
       x = squeeze(nanmean(nanmean(log10(grandavg_EO.powspctrm(idx_YA_fft,1:20,33:41)),3),2));
       y = squeeze(nanmean(nanmean(mseMerged{1,indCond}.MSEVanilla(:,1:20,39:41),3),2));
       l1 = scatter(x,y, 'filled', 'k'); y1_ls = polyval(polyfit(x,y,1),x); y1_ls = plot(x, y1_ls, 'Color', 'k');
       [rl1,pl1] = corrcoef(x,y);
       x = squeeze(nanmean(nanmean(log10(grandavg_EO.powspctrm(idx_OA_fft,1:20,33:41)),3),2));
       y = squeeze(nanmean(nanmean(mseMerged{2,indCond}.MSEVanilla(:,1:20,39:41),3),2));
       l2 = scatter(x,y, 'filled', 'r'); y2_ls = polyval(polyfit(x,y,1),x); y2_ls = plot(x, y2_ls, 'Color', 'r');
       [rl2,pl2] = corrcoef(x,y);
       legend([y1_ls, y2_ls], {['Younger Adults: r=', num2str(round(rl1(2),2)), ', p=',num2str(round(pl1(2),3))], ...
        ['Older Adults: r=', num2str(round(rl2(2),2)), ', p=',num2str(round(pl2(2),3))]}, 'Location', 'NorthEast'); legend('boxoff');
       title('High frequency power predicts coarse Vanilla MSE')
       xlabel('Power 32-64 Hz (log10)'); ylabel('Vanilla MSE');
       set(findall(gcf,'-property','FontSize'),'FontSize',18)
       ylim([0 1])
   
   % reset legend Font sizes
   set(lgd1,'FontSize',14);
   set(lgd2,'FontSize',14);
   
   pn.plotFolder = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/A_MSE_CSD_multiVariant/C_figures/';
   figureName = 'F_LowFreqShiftEntropyR';
   saveas(h, [pn.plotFolder, figureName], 'fig');
   saveas(h, [pn.plotFolder, figureName], 'epsc');
   saveas(h, [pn.plotFolder, figureName], 'png');
   
   % show absence of correlation with rescaled R params
   
  figure; hold on;
       x = squeeze(nanmean(nanmean(log10(grandavg_EO.powspctrm(idx_YA_fft,1:20,33:41)),3),2));
       y = squeeze(nanmean(nanmean(mseMerged{1,indCond}.MSEfiltskip(:,2,1:20,39:41),3),2));
       l1 = scatter(x,y, 'filled', 'k'); y1_ls = polyval(polyfit(x,y,1),x); y1_ls = plot(x, y1_ls, 'Color', 'k');
       [rl1,pl1] = corrcoef(x,y);
       x = squeeze(nanmean(nanmean(log10(grandavg_EO.powspctrm(idx_OA_fft,1:20,33:41)),3),2));
       y = squeeze(nanmean(nanmean(mseMerged{2,indCond}.MSEfiltskip(:,2,1:20,39:41),3),2));
       l2 = scatter(x,y, 'filled', 'r'); y2_ls = polyval(polyfit(x,y,1),x); y2_ls = plot(x, y2_ls, 'Color', 'r');
       [rl2,pl2] = corrcoef(x,y);
       legend([y1_ls, y2_ls], {['Younger Adults: r=', num2str(round(rl1(2),2)), ', p=',num2str(round(pl1(2),3))], ...
        ['Older Adults: r=', num2str(round(rl2(2),2)), ', p=',num2str(round(pl2(2),3))]}, 'Location', 'NorthEast'); legend('boxoff');
       title('High frequency power predicts coarse Vanilla MSE')
       xlabel('Power 32-64 Hz (log10)'); ylabel('Vanilla MSE');
       set(findall(gcf,'-property','FontSize'),'FontSize',18)
       ylim([0 1])
   
   % t-test of age difference in shift
       
       R_YA = squeeze(nanmean(nanmean(mseMerged{1,indCond}.Rfiltskip(:,2,1:60,39:41),3),2))-...
                squeeze(nanmean(nanmean(mseMerged{1,indCond}.Rvanilla(:,2,1:60,39:41),3),2));
       MSE_YA = squeeze(nanmedian(nanmean(mseMerged{1,indCond}.MSEfiltskip(:,2,1:60,39:41),3),2))-...
                squeeze(nanmedian(nanmean(mseMerged{1,indCond}.MSEVanilla(:,2,1:60,39:41),3),2));
       R_OA = squeeze(nanmean(nanmean(mseMerged{2,indCond}.Rfiltskip(:,2,1:60,39:41),3),2))-...
                squeeze(nanmean(nanmean(mseMerged{2,indCond}.Rvanilla(:,2,1:60,39:41),3),2));
       MSE_OA = squeeze(nanmedian(nanmean(mseMerged{2,indCond}.MSEfiltskip(:,2,1:60,39:41),3),2))-...
                squeeze(nanmedian(nanmean(mseMerged{2,indCond}.MSEVanilla(:,2,1:60,39:41),3),2));

      [~, p] = ttest2(R_YA, R_OA)
      [~, p] = ttest2(MSE_YA, MSE_OA)
            
    %% calculate MSE -- Ratio for multiple scales
    
    for scales = 1:41
        x = squeeze(nanmedian(nanmean(mseMerged{1,indCond}.Rvanilla(idx_YA_mse,channels,scales),3),2))./...
            squeeze(nanmedian(nanmean(mseMerged{1,indCond}.Rhp(idx_YA_mse,channels,scales),3),2));
        y = squeeze(nanmedian(nanmean(mseMerged{1,indCond}.MSEVanilla(idx_YA_mse,channels,scales),3),2));
        [rl1,pl1] = corrcoef(x,y);
        RMat(scales) = rl1(2);
    end

    h = figure('units','normalized','position',[.1 .1 .3 .4]);
    hold on;
    plot(squeeze(nanmedian(nanmedian(mseMerged{2,indCond}.MSEVanilla(:, channels,:),2),1))-...
        squeeze(nanmedian(nanmedian(mseMerged{1,indCond}.MSEVanilla(:, channels,:),2),1)), 'r', 'LineWidth', 5)
    plot(RMat, 'k', 'LineWidth', 5)
    xlim([1 41]);
    title('R parameter bias covers scales of OA > YA MSE increase')
    set(gca, 'XTick', 1:4:41); set(gca, 'XTickLabel', round(mseMerged{2,indCond}.freq(1:4:41))); xlabel('Scale [Hz]'); ylabel('Estimate magnitude');
    legend({'MSE Difference OA-YA'; 'R bias correlation with MSE'}); legend('boxoff');
    set(findall(gcf,'-property','FontSize'),'FontSize',18)
    pn.plotFolder = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/A_MSE_CSD_multiVariant/C_figures/';
    figureName = 'F4_RBias_ageContrast';
    saveas(h, [pn.plotFolder, figureName], 'fig');
    saveas(h, [pn.plotFolder, figureName], 'epsc');
    saveas(h, [pn.plotFolder, figureName], 'png');

    
    for scales = 1:41
        x = squeeze(nanmedian(nanmean(mseMerged{1,indCond}.Rhp(idx_YA_mse,channels,scales),3),2))./...
            squeeze(nanmedian(nanmean(mseMerged{1,indCond}.Rfiltskip(idx_YA_mse,channels,scales),3),2));
        y = squeeze(nanmedian(nanmean(mseMerged{1,indCond}.MSEhp(idx_YA_mse,channels,scales),3),2));
        [rl1,pl1] = corrcoef(x,y);
        RMat(scales) = rl1(2);
    end
    
    figure; plot(RMat)
    
   
    h = figure('units','normalized','position',[.1 .1 .7 .4]);
    scales = 1:2;
    subplot(1,2,1); hold on;
        x = squeeze(nanmedian(nanmean(mseMerged{1,indCond}.Rfiltskip(idx_YA_mse,channels,scales),3),2))./...
            squeeze(nanmedian(nanmean(mseMerged{1,indCond}.Rhp(idx_YA_mse,channels,scales),3),2));
        y = squeeze(nanmedian(nanmean(mseMerged{1,indCond}.MSEfiltskip(idx_YA_mse,channels,scales),3),2));
        l1 = scatter(x,y, 'filled', 'k'); lsline;
        [rl1,pl1] = corrcoef(x,y);
        % add OA
        x = squeeze(nanmedian(nanmean(mseMerged{2,indCond}.Rfiltskip(idx_OA_mse,channels,scales),3),2))./...
            squeeze(nanmedian(nanmean(mseMerged{2,indCond}.Rhp(idx_OA_mse,2,channels, scales),3),2));
        y = squeeze(nanmedian(nanmean(mseMerged{2,indCond}.MSEVanilla(idx_OA_mse,channels,scales),3),2));
        l2 = scatter(x,y, 'filled', 'r'); lsline;
        [rl2,pl2] = corrcoef(x,y);
        title('Eyes open: Scales 1-2'); 
        legend([l1, l2], {['Younger Adults: r=', num2str(round(rl1(2),2))], ['Older Adults: r=', num2str(round(rl2(2),2))]}, 'Location', 'NorthWest'); legend('boxoff');
        xlabel('Ratio: lowpass R/highpass R'); ylabel('MSE filtskip')
   subplot(1,2,2); hold on;
        x = squeeze(nanmedian(nanmean(mseMerged{1,indCond}.Rfiltskip(idx_YA_mse,channels,scales),3),2))./...
            squeeze(nanmedian(nanmean(mseMerged{1,indCond}.Rhp(idx_YA_mse,channels,scales),3),2));
        y = squeeze(nanmedian(nanmean(mseMerged{1,indCond}.MSEfiltskip(idx_YA_mse,channels,scales),3),2));
        l1 = scatter(x,y, 'filled', 'k'); lsline;
        [rl1,pl1] = corrcoef(x,y);
        % add OA
        x = squeeze(nanmedian(nanmean(mseMerged{2,indCond}.Rfiltskip(idx_OA_mse,channels,scales),3),2))./...
            squeeze(nanmedian(nanmean(mseMerged{2,indCond}.Rhp(idx_OA_mse,channels,scales),3),2));
        y = squeeze(nanmedian(nanmean(mseMerged{2,indCond}.MSEfiltskip(idx_OA_mse,channels,scales),3),2));
        l2 = scatter(x,y, 'filled', 'r'); lsline;
        [rl2,pl2] = corrcoef(x,y);
        title('Eyes closed: Scales 1-2');
        legend([l1, l2], {['Younger Adults: r=', num2str(round(rl1(2),2))], ['Older Adults: r=', num2str(round(rl2(2),2))]}, 'Location', 'NorthWest'); legend('boxoff');
        xlabel('Ratio: lowpass R/highpass R'); ylabel('MSE filtskip')
   set(findall(gcf,'-property','FontSize'),'FontSize',18)
        
    % estimate vanilla MSE based on contrasting unbiased entropy to vanilla R
    % VanillaMSE high = hpSE/VanillaR; VanillaSE high = lpSE/VanillaR
    
    figure;
    for indScale = 1:41
    y = squeeze(nanmedian(nanmean(mseMerged{1,indCond}.MSEVanilla(idx_YA_mse,2,channels,indScale),3),2));
    x = squeeze(nanmedian(nanmean(mseMerged{1,indCond}.MSEhp(idx_YA_mse,2,channels,indScale),3),2))./...
        squeeze(nanmedian(nanmean(mseMerged{1,indCond}.Rvanilla(idx_YA_mse,2,channels,indScale),3),2));
    scatter(x,y)
    r = corrcoef(x,y);
    RMat(indScale) = r(2);
    pause(.2)
    end
    figure; plot(RMat)
    
    figure;
    y = squeeze(nanmedian(nanmean(mseMerged{1,indCond}.MSEVanilla(idx_YA_mse,2,channels,38:41),3),2))./...
        squeeze(nanmedian(nanmean(mseMerged{1,indCond}.Rvanilla(idx_YA_mse,2,channels,38:41),3),2));
    x = squeeze(nanmedian(nanmean(mseMerged{1,indCond}.MSEfiltskip(idx_YA_mse,2,channels,38:41),3),2))./...
        squeeze(nanmedian(nanmean(mseMerged{1,indCond}.Rvanilla(idx_YA_mse,2,channels,38:41),3),2));
    scatter(x,y)
    
        
    figure
    for indScale = 1:41
        cla;
        hold on;
        x = squeeze(nanmedian(nanmean(mseMerged{1,indCond}.Rhp(idx_YA_mse,1,channels,indScale),3),2));
        y = squeeze(nanmedian(linFit_2_30_EC(idx_YA_fft, channels),2));
        scatter(x,y, 'filled', 'k'); lsline;
        % add OA
        x = squeeze(nanmedian(nanmean(mseMerged{2,indCond}.Rhp(idx_OA_mse,1,channels,indScale),3),2));
        y = squeeze(nanmedian(linFit_2_30_EC(idx_OA_fft, channels),2));
        scatter(x,y, 'filled', 'r'); lsline;
        pause(.2)
    end
    
figure;
subplot(1,2,1); hold on;
    x = squeeze(nanmean(nanmean(mseMerged{1,indCond}.MSEVanilla(idx_YA_mse,2,channels,1:3),3),2));
    y = squeeze(nanmean(linFit_2_30_EO(idx_YA_fft, channels),2));
    scatter(x,y, 'filled', 'k'); lsline;
    % add OA
    x = squeeze(nanmean(nanmean(mseMerged{2,indCond}.MSEVanilla(idx_OA_mse,2,channels,1:3),3),2));
    y = squeeze(nanmean(linFit_2_30_EO(idx_OA_fft, channels),2));
    scatter(x,y, 'filled', 'r'); lsline;
subplot(1,2,2); hold on;
    x = squeeze(nanmean(nanmean(mseMerged{1,indCond}.MSEhp(idx_YA_mse,2,channels,1:3),3),2));
    y = squeeze(nanmean(linFit_2_30_EO(idx_YA_fft, channels),2));
    scatter(x,y, 'filled', 'k'); lsline;
    % add OA
    x = squeeze(nanmean(nanmean(mseMerged{2,indCond}.MSEhp(idx_OA_mse,2,channels,1:3),3),2));
    y = squeeze(nanmean(linFit_2_30_EO(idx_OA_fft, channels),2));
    scatter(x,y, 'filled', 'r'); lsline;
    
    
    %% cumulative power plots in both directions
    
    figure;
    subplot(3,2,1); imagesc(squeeze(nanmean(grandavg_EO.powspctrm(idx_OA_fft,channels,:),2))); title('Power')
    subplot(3,2,3); imagesc(squeeze(nanmean(cumsum(grandavg_EO.powspctrm(idx_OA_fft,channels,:),3),2))); title('Cumulative power, lowpass case')
    subplot(3,2,4); imagesc(squeeze(nanmean(cumsum(grandavg_EO.powspctrm(idx_OA_fft,channels,:),3, 'reverse'),2))); title('Cumulative power, highpass case')
    subplot(3,2,5); imagesc(squeeze(nanmean(grandavg_EO.powspctrm(idx_OA_fft,channels,:),2))./...
        squeeze(nanmean(cumsum(grandavg_EO.powspctrm(idx_OA_fft,channels,:),3),2))); title('Relative power to cumulate, lowpass case')
    subplot(3,2,6); imagesc(squeeze(nanmean(grandavg_EO.powspctrm(idx_OA_fft,channels,:),2))./...
        squeeze(nanmean(cumsum(grandavg_EO.powspctrm(idx_OA_fft,channels,:),3, 'reverse'),2))); title('Relative power to cumulate, highpass case')
    
    figure;
    subplot(3,2,1); imagesc(squeeze(nanmean(grandavg_EO.powspctrm(idx_OA_fft,channels,:),2))); title('Power')
    subplot(3,2,3); imagesc(zscore(squeeze(nanmean(cumsum(grandavg_EO.powspctrm(idx_OA_fft,channels,:),3),2)),[],2)); title('Cumulative power, lowpass case')
    subplot(3,2,4); imagesc(zscore(squeeze(nanmean(cumsum(grandavg_EO.powspctrm(idx_OA_fft,channels,:),3, 'reverse'),2)),[],2)); title('Cumulative power, highpass case')
    subplot(3,2,5); imagesc(squeeze(nanmean(grandavg_EO.powspctrm(idx_OA_fft,channels,:),2))./...
        squeeze(nanmean(cumsum(grandavg_EO.powspctrm(idx_OA_fft,channels,:),3),2))); title('Relative power to cumulate, lowpass case')
    subplot(3,2,6); imagesc(squeeze(nanmean(grandavg_EO.powspctrm(idx_OA_fft,channels,:),2))./...
        squeeze(nanmean(cumsum(grandavg_EO.powspctrm(idx_OA_fft,channels,:),3, 'reverse'),2))); title('Relative power to cumulate, highpass case')
    
    
    %% ratio of power to R parameter as related to Vanilla MSE 
    
    figure;
    for indScale = 1:41
    y = squeeze(nanmedian(nanmean(mseMerged{1,indCond}.MSEVanilla(idx_YA_mse,2,channels,indScale),3),2));
    x = squeeze(nanmedian(nanmean(mseMerged{1,indCond}.Rvanilla(idx_YA_mse,2,channels,indScale),3),2));
    scatter(x,y)
    r = corrcoef(x,y);
    RMat(indScale) = r(2);
    pause(.2)
    end
    figure; plot(RMat)
    
    figure;
    for indScale = 1:41
    y = squeeze(nanmedian(nanmean(mseMerged{2,indCond}.MSEVanilla(idx_OA_mse,2,channels,indScale),3),2));
    x = squeeze(nanmedian(nanmean(mseMerged{2,indCond}.Rvanilla(idx_OA_mse,2,channels,indScale),3),2));
    scatter(x,y)
    r = corrcoef(x,y);
    RMat(indScale) = r(2);
    pause(.2)
    end
    figure; plot(RMat)
    
    figure;
    for indScale = 1:41
    y = squeeze(nanmedian(nanmean(mseMerged{1,indCond}.MSEhp(idx_YA_mse,1,channels,indScale),3),2));
    x = squeeze(nanmedian(nanmean(mseMerged{1,indCond}.Rhp(idx_YA_mse,1,channels,indScale),3),2));
    scatter(x,y)
    r = corrcoef(x,y);
    RMat(indScale) = r(2);
    pause(.2)
    end
    figure; plot(RMat)
    
    figure;
    for indScale = 1:41
    y = squeeze(nanmedian(nanmean(mseMerged{1,indCond}.MSEPointavg(idx_YA_mse,1,channels,indScale),3),2));
    x = squeeze(nanmedian(nanmean(mseMerged{1,indCond}.Rpointavg(idx_YA_mse,1,channels,indScale),3),2));
    scatter(x,y)
    r = corrcoef(x,y);
    RMat(indScale) = r(2);
    pause(.2)
    end
    figure; plot(RMat)
    
    figure; hold on;
    for indScale = 1:41
        y = squeeze(nanmedian(nanmean(mseMerged{1,indCond}.MSEPointavg(idx_YA_mse,1,channels,indScale),3),2));
        x = squeeze(nanmedian(nanmean(mseMerged{1,indCond}.Rpointavg(idx_YA_mse,1,channels,indScale),3),2));
        scatter(x,y, 'filled', 'k');
        y = squeeze(nanmedian(nanmean(mseMerged{2,indCond}.MSEPointavg(idx_OA_mse,1,channels,indScale),3),2));
        x = squeeze(nanmedian(nanmean(mseMerged{2,indCond}.Rpointavg(idx_OA_mse,1,channels,indScale),3),2));
        scatter(x,y, 'filled', 'r'); 
    end
    title('Scale-wise R and sample entropy are inter-individually inversely related')
    
    figure; hold on;
    y = squeeze(nanmedian(nanmean(mseMerged{1,indCond}.MSEVanilla(idx_YA_mse,1,channels,:),3),2));
    x = squeeze(nanmedian(nanmean(mseMerged{1,indCond}.Rvanilla(idx_YA_mse,1,channels,:),3),2));
    scatter(x,y, 'filled', 'k');
    y = squeeze(nanmedian(nanmean(mseMerged{2,indCond}.MSEVanilla(idx_OA_mse,1,channels,:),3),2));
    x = squeeze(nanmedian(nanmean(mseMerged{2,indCond}.Rvanilla(idx_OA_mse,1,channels,:),3),2));
    scatter(x,y, 'filled', 'r'); 
    title('Scale-wise R and sample entropy are inter-individuallyinversely related')
    
    % how about the relationship between scales within subject?
    
    figure; hold on;
    y = squeeze(nanmedian(nanmean(mseMerged{1,indCond}.MSEPointavg(idx_YA_mse,1,channels,:),1),3));
    x = squeeze(nanmedian(nanmean(mseMerged{1,indCond}.Rpointavg(idx_YA_mse,1,channels,:),1),3));
    scatter(x,y, 'filled', 'k');
    y = squeeze(nanmedian(nanmean(mseMerged{2,indCond}.MSEPointavg(idx_OA_mse,1,channels,:),1),3));
    x = squeeze(nanmedian(nanmean(mseMerged{2,indCond}.Rpointavg(idx_OA_mse,1,channels,:),1),3));
    scatter(x,y, 'filled', 'r'); 
    title('Scale-wise R and sample entropy are inter-individually inversely related')
    
    figure; hold on;
    y = squeeze(nanmedian(nanmean(mseMerged{1,indCond}.MSEfiltskip(idx_YA_mse,1,channels,:),1),3));
    x = squeeze(nanmedian(nanmean(mseMerged{1,indCond}.Rfiltskip(idx_YA_mse,1,channels,:),1),3));
    scatter(x,y, 'filled', 'k');
    y = squeeze(nanmedian(nanmean(mseMerged{2,indCond}.MSEfiltskip(idx_OA_mse,1,channels,:),1),3));
    x = squeeze(nanmedian(nanmean(mseMerged{2,indCond}.Rfiltskip(idx_OA_mse,1,channels,:),1),3));
    scatter(x,y, 'filled', 'r'); 
    title('Scale-wise R and sample entropy are inter-individually inversely related')
    
    figure; hold on;
    y = squeeze(nanmedian(nanmean(mseMerged{1,indCond}.MSEhp(idx_YA_mse,1,channels,:),1),3));
    x = squeeze(nanmedian(nanmean(mseMerged{1,indCond}.Rhp(idx_YA_mse,1,channels,:),1),3));
    scatter(x,y, 'filled', 'k');
    y = squeeze(nanmedian(nanmean(mseMerged{2,indCond}.MSEhp(idx_OA_mse,1,channels,:),1),3));
    x = squeeze(nanmedian(nanmean(mseMerged{2,indCond}.Rhp(idx_OA_mse,1,channels,:),1),3));
    scatter(x,y, 'filled', 'r'); 
    title('Scale-wise R and sample entropy are inter-individually inversely related')
    
    figure; hold on;
    y = squeeze(nanmedian(nanmean(mseMerged{1,indCond}.MSEbp(idx_YA_mse,1,channels,:),1),3));
    x = squeeze(nanmedian(nanmean(mseMerged{1,indCond}.Rbp(idx_YA_mse,1,channels,:),1),3));
    scatter(x,y, 'filled', 'k');
    y = squeeze(nanmedian(nanmean(mseMerged{2,indCond}.MSEbp(idx_OA_mse,1,channels,:),1),3));
    x = squeeze(nanmedian(nanmean(mseMerged{2,indCond}.Rbp(idx_OA_mse,1,channels,:),1),3));
    scatter(x,y, 'filled', 'r'); 
    title('Scale-wise R and sample entropy are inter-individually inversely related')
    
    
    %% topographies of relative measures and Vanilla MSE difference
    
    pn.root     = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/';
    pn.tools	= [pn.root, 'B_analyses/S2_TFR/T_tools/']; addpath(pn.tools);
    addpath([pn.tools, 'fieldtrip-20170904/']); ft_defaults;

    load('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S2_TFR/B_data/elec.mat')

    cfg = [];
    cfg.layout = 'acticap-64ch-standard2.mat';
    cfg.parameter = 'powspctrm';
    cfg.comment = 'no';
    cfg.colorbar = 'SouthOutside';
    cfg.zlim = [];

    figure;
    plotData = [];
    plotData.label = elec.label; % {1 x N}
    plotData.dimord = 'chan';
    plotData.powspctrm = squeeze(nanmean(nanmean(mseMerged{1,indCond}.Rhp(:,2,1:60,41)./mseMerged{1,indCond}.Rvanilla(:,2,1:60,41),4),1))-...
        squeeze(nanmedian(nanmedian(mseMerged{2,indCond}.Rhp(:,2,1:60,41)./mseMerged{2,indCond}.Rvanilla(:,2,1:60,41),4),1));
    
    plotData.powspctrm = squeeze(nanmean(nanmean(mseMerged{1,indCond}.Rfiltskip(:,2,1:60,1:15)./mseMerged{1,indCond}.Rvanilla(:,2,1:60,1:15),4),1))-...
        squeeze(nanmedian(nanmedian(mseMerged{2,indCond}.Rfiltskip(:,2,1:60,1:15)./mseMerged{2,indCond}.Rvanilla(:,2,1:60,1:15),4),1));
    
    
    plotData.powspctrm = squeeze(nanmedian(nanmedian(mseMerged{1,indCond}.MSEVanilla(:,2,1:60,41),4),1))-...
        squeeze(nanmedian(nanmedian(mseMerged{2,indCond}.MSEVanilla(:,2,1:60,41),4),1));
    ft_topoplotER(cfg,plotData);
    
    
      plotData.powspctrm = squeeze(nanmean(nanmean(mseMerged{1,indCond}.MSEVanilla(:,2,1:60,41),4),1))-...
        squeeze(nanmedian(nanmean(mseMerged{2,indCond}.MSEVanilla(:,2,1:60,41),4),1));
    ft_topoplotER(cfg,plotData);