function mseavg = C_SS_mse_merge()

fun2run = 'msen_calc';

% Make jobs for each subject to concatenate runs and collect sessions
% Scales:
% >> bsxfun(@rdivide, 250, 1:41)

%% set up paths
% restoredefaultpath
if ismac
    pn.root = '/Users/kosciessa/Desktop/mntTardis/STSWD_Rest/WIP_eeg/A_MSE_CSD/';
    backend = 'local'; % local parfor
else
%     pn.root = '/home/mpib/kosciessa/';
%     backend = 'torque';
end
pn.dataInOUT  = [pn.root, 'B_data/C_MSE_Output_v1/'];
addpath([pn.root, 'T_tools/qsub_tardis']);
addpath([pn.root, 'T_tools/functions_mse_time/']);
addpath([pn.root, 'T_tools/fieldtrip-20170904/']);
ft_defaults

cond_names  = {'EC'; 'EO'};

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% N = 47 YA, 52 OAs
% 2201 - no rest available; 1213 dropped (weird channel arrangement)

IDs = {'1117';'1118';'1120';'1124';'1126';'1131';'1132';'1135';'1136';'1138';...
    '1144';'1151';'1158';'1160';'1163';'1164';'1167';'1169';'1172';'1173';...
    '1178';'1182';'1215';'1216';'1219';'1221';'1223';'1227';'1228';...
    '1233';'1234';'1237';'1239';'1240';'1243';'1245';'1247';'1250';'1252';...
    '1257';'1261';'1265';'1266';'1268';'1270';'1276';'1281';...
    '2104';'2107';'2108';'2112';'2118';'2120';'2121';'2123';'2125';'2129';...
    '2130';'2131';'2132';'2133';'2134';'2135';'2139';'2140';'2145';'2147';...
    '2149';'2157';'2160';'2202';'2203';'2205';'2206';'2209';'2210';...
    '2211';'2213';'2214';'2215';'2216';'2217';'2219';'2222';'2224';'2226';...
    '2227';'2236';'2237';'2238';'2241';'2244';'2246';'2248';'2250';'2251';...
    '2252';'2258';'2261'};

nsub = length(IDs);

PREIN = pn.dataInOUT;

% load example mse to get nchan etc
subjdir = fullfile(PREIN);
fprintf('Loading Subject %s Session %d: . . .\n', 1, 1)
cd(subjdir)
runname = sprintf('%s_%s_MSE_OUT.mat', IDs{1}(1:4), cond_names{1});
run = dir(runname);
load(run.name)

% JQK: dimord should be: 'chan_scales_time'
ntim = size(mse.msen,3);
nchan = size(mse.msen,1);
nscales = size(mse.msen,2);

mseavg.dat = [];
mseavg.dat = nan(nsub, 1, nchan,nscales, ntim, 3); % IDs sess channel scale time cond
mseavg.time = mse.time;
mseavg.scale = mse.scale; % JQK: corrected field name
mseavg.fun2run = fun2run;

% collect mse results across subjects etc.
for isess = 1
    for isub = 1:numel(IDs)
        for icond = 1:numel(cond_names)
            subjdir = fullfile(PREIN);
            %fprintf('\n\nSubject directory: %s  . . .\n', subjdir)
            %fprintf('Loading Subject %s Condition %s: . . .\n', IDs{isub}(1:4), cond_names{icond})
            runname = sprintf('%s_%s_MSE_OUT.mat', IDs{isub}(1:4), cond_names{icond});
            run = dir(runname);
            if isempty(run)
                fprintf('%s not found\n', runname)
                mseavg.dat(isub,isess,:,:,:,icond) = NaN;
                continue
            end
            load(run.name)
            mseavg.dat(isub,isess,:,:,:,icond) = mse.msen;
        end
    end
end

mseavg.IDs = IDs;
mseavg.mse_leg = {'MSEn'};
mseavg.dimord = 'subj_sess_chan_scale_time_cond';
mseavg.dimordsize = size(mseavg.dat);

%% save

save([PREIN, 'STSWD_rest_mseavg_CSD.mat'], 'mseavg');

figure; imagesc(squeeze(nanmean(mseavg.dat(:,:,:,:,:,1),1)));
figure; imagesc(squeeze(nanmean(mseavg.dat(:,:,:,:,:,2)-mseavg.dat(:,:,:,:,:,1),1)));
figure; imagesc(squeeze(nanmean(mseavg.dat(:,:,:,:,:,2),1)));
