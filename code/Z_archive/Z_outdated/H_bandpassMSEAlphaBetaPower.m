% compare individual bandpass beta MSE and spectral power

addpath('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/A_MSE_CSD_multiVariant/T_tools/fieldtrip-20170904'); ft_defaults;

pn.dataPath = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/A_MSE_CSD_multiVariant/B_data/C_MSE_Output_v1/';

IDs{1} = {'1117';'1118';'1120';'1124';'1126';'1131';'1132';'1135';'1136';'1138';...
    '1144';'1151';'1158';'1160';'1163';'1164';'1167';'1169';'1172';'1173';...
    '1178';'1182';'1215';'1216';'1219';'1221';'1223';'1227';'1228';...
    '1233';'1234';'1237';'1239';'1240';'1243';'1245';'1247';'1250';'1252';...
    '1257';'1261';'1265';'1266';'1268';'1270';'1276';'1281'};

IDs{2} = {'2104';'2107';'2108';'2112';'2118';'2120';'2121';'2123';'2125';'2129';...
    '2130';'2131';'2132';'2133';'2134';'2135';'2139';'2140';'2145';'2147';...
    '2149';'2157';'2160';'2202';'2203';'2205';'2206';'2209';'2210';...
    '2211';'2213';'2214';'2215';'2216';'2217';'2219';'2222';'2224';'2226';...
    '2227';'2236';'2237';'2238';'2241';'2244';'2246';'2248';'2250';'2251';...
    '2252';'2258';'2261'};

conds = {'EC'; 'EO'};

mseMerged = cell(1,1);
for indGroup = 1:2
    for indID = 1:numel(IDs{indGroup})
        for indCond = 1:2
            try
            curID = IDs{indGroup}{indID};
            % filtskip (BP)
            load([pn.dataPath, curID, '_',conds{indCond},'_MSE_OUT_bp.mat']);
            mseMerged{indGroup,indCond}.MSEbp(indID,:,:) = mse.sampen(1:60,:);
            mseMerged{indGroup,indCond}.Rlog10bp(indID,:,:) = log10(squeeze(nanmean(mse.r(:,:,:,1:60),3)))';
            mseMerged{indGroup,indCond}.dimord = 'subj_chan_freq';
            mseMerged{indGroup,indCond}.label = mse.label(1:60);
            mseMerged{indGroup,indCond}.timescales = mse.timescales;
            mseMerged{indGroup,indCond}.freq = (1./[1:41])*250;
            mseMerged{indGroup,indCond}.time = mse.time;
            catch
                disp([curID, ' ',conds{indCond}, ' skipped']);
                continue
            end
        end
    end
end


%% load MSE

load('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/A_MSE_CSD_multiVariant/B_data/E_mseMerged.mat', 'mseMerged', 'IDs')

%% load PSD

load('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/B_FFT/B_data/B_FFT_grandavg_individual.mat')

commonYA = info.IDs_all(ismember(info.IDs_all, IDs{1}));
commonOA = info.IDs_all(ismember(info.IDs_all, IDs{2}));

idx_YA_fft = ismember(info.IDs_all, commonYA);
idx_OA_fft = ismember(info.IDs_all, commonOA);
idx_YA_mse = ismember(IDs_YA, commonYA);
idx_OA_mse = ismember(IDs_OA, commonOA);

%% load stats cluster

pn.root = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/A_MSE_CSD_multiVariant/';
load([pn.root, 'B_data/E2_CBPA_Age_Condition.mat'], 'stat', 'methodLabels')

%% correlate PSD with MSE

betaCluster = max(stat{4,5}.mask(:,23:27),[],2);
alphaCluster = max(stat{4,5}.mask(:,15:20),[],2);

BetaPSD_YA = squeeze(nanmean(nanmedian(log10(grandavg_EO.powspctrm(idx_YA_fft,betaCluster,grandavg_EC.freq > 12 & grandavg_EO.freq<18)),3),2));
AlphaPSD_YA = squeeze(nanmean(nanmedian(log10(grandavg_EO.powspctrm(idx_YA_fft,alphaCluster,grandavg_EC.freq > 8 & grandavg_EO.freq<12)),3),2));

BetaMSE_YA = squeeze(nanmean(nanmedian(mseMerged{1,2}.MSEbp(:,betaCluster,mseMerged{1,1}.freq> 12 & mseMerged{1,1}.freq< 18),3),2));
AlphaMSE_YA = squeeze(nanmean(nanmedian(mseMerged{1,2}.MSEbp(:,alphaCluster,mseMerged{1,1}.freq> 10 & mseMerged{1,1}.freq< 12),3),2));

BetaPSD_OA = squeeze(nanmean(nanmedian(log10(grandavg_EO.powspctrm(idx_OA_fft,betaCluster,grandavg_EO.freq > 12 & grandavg_EO.freq<18)),3),2));
AlphaPSD_OA = squeeze(nanmean(nanmedian(log10(grandavg_EO.powspctrm(idx_OA_fft,alphaCluster,grandavg_EO.freq > 8 & grandavg_EO.freq<12)),3),2));

BetaMSE_OA = squeeze(nanmean(nanmedian(mseMerged{2,2}.MSEbp(:,betaCluster,mseMerged{1,1}.freq> 12 & mseMerged{1,1}.freq< 18),3),2));
AlphaMSE_OA = squeeze(nanmean(nanmedian(mseMerged{2,2}.MSEbp(:,alphaCluster,mseMerged{1,1}.freq> 8 & mseMerged{1,1}.freq< 12),3),2));

h = figure('units','normalized','position',[.1 .1 .7 .4]);
subplot(1,2,1); cla; hold on;
    x = AlphaPSD_YA; y = AlphaMSE_YA; 
    l1 = scatter(x,y, 'filled', 'k'); y1_ls = polyval(polyfit(x,y,1),x); y1_ls = plot(x, y1_ls, 'Color', 'k');
    [r1, p1] = corrcoef(x, y);
    x = AlphaPSD_OA; y = AlphaMSE_OA; 
    l3 = scatter(x,y, 'filled', 'r'); y3_ls = polyval(polyfit(x,y,1),x); y3_ls = plot(x, y3_ls, 'Color', 'r');
    [r3, p3] = corrcoef(x, y);
    legend([y1_ls, y3_ls], {['YA Alpha: r = ', num2str(round(r1(2),2)), ', p = ' num2str(round(p1(2),3))], ...
        ['OA Alpha: r = ', num2str(round(r3(2),2)), ', p = ' num2str(round(p3(2),3))]}); legend('boxoff');
    xlabel('Power (log10)'); ylabel('Entropy');
    title('Parietooccipital Alpha')
subplot(1,2,2); cla; hold on;
    x = BetaPSD_YA; y = BetaMSE_YA; 
    l2 = scatter(x,y, 'filled', 'k'); y2_ls = polyval(polyfit(x,y,1),x); y2_ls = plot(x, y2_ls, 'Color', 'k');
    [r2, p2] = corrcoef(x, y); 
    x = BetaPSD_OA; y = BetaMSE_OA; 
    l4 = scatter(x,y, 'filled', 'r'); y4_ls = polyval(polyfit(x,y,1),x); y4_ls = plot(x, y4_ls, 'Color', 'r');
    [r4, p4] = corrcoef(x, y); 
    legend([y2_ls, y4_ls], {['YA Beta: r = ', num2str(round(r2(2),2)), ', p = ' num2str(round(p2(2),3))],...
        ['OA Beta: r = ', num2str(round(r4(2),2)), ', p = ' num2str(round(p4(2),3))]}); legend('boxoff');
    xlabel('Power (log10)'); ylabel('Entropy');
    title('Central Beta')
set(findall(gcf,'-property','FontSize'),'FontSize',18)

%% correlate PSD with MSE in eyes closed condition

betaCluster = max(stat{4,5}.mask(:,23:27),[],2);
alphaCluster = max(stat{4,5}.mask(:,15:20),[],2);

BetaPSD_YA = squeeze(nanmean(nanmedian(log10(grandavg_EC.powspctrm(idx_YA_fft,betaCluster,grandavg_EC.freq > 12 & grandavg_EO.freq<18)),3),2));
AlphaPSD_YA = squeeze(nanmean(nanmedian(log10(grandavg_EC.powspctrm(idx_YA_fft,alphaCluster,grandavg_EC.freq > 8 & grandavg_EO.freq<12)),3),2));

BetaMSE_YA = squeeze(nanmean(nanmedian(mseMerged{1,1}.MSEbp(:,betaCluster,mseMerged{1,1}.freq> 12 & mseMerged{1,1}.freq< 18),3),2));
AlphaMSE_YA = squeeze(nanmean(nanmedian(mseMerged{1,1}.MSEbp(:,alphaCluster,mseMerged{1,1}.freq> 10 & mseMerged{1,1}.freq< 12),3),2));

BetaPSD_OA = squeeze(nanmean(nanmedian(log10(grandavg_EC.powspctrm(idx_OA_fft,betaCluster,grandavg_EO.freq > 12 & grandavg_EO.freq<18)),3),2));
AlphaPSD_OA = squeeze(nanmean(nanmedian(log10(grandavg_EC.powspctrm(idx_OA_fft,alphaCluster,grandavg_EO.freq > 8 & grandavg_EO.freq<12)),3),2));

BetaMSE_OA = squeeze(nanmean(nanmedian(mseMerged{2,1}.MSEbp(:,betaCluster,mseMerged{1,1}.freq> 12 & mseMerged{1,1}.freq< 18),3),2));
AlphaMSE_OA = squeeze(nanmean(nanmedian(mseMerged{2,1}.MSEbp(:,alphaCluster,mseMerged{1,1}.freq> 8 & mseMerged{1,1}.freq< 12),3),2));

h = figure('units','normalized','position',[.1 .1 .7 .4]);
subplot(1,2,1); cla; hold on;
    x = AlphaPSD_YA; y = AlphaMSE_YA; 
    l1 = scatter(x,y, 'filled', 'k'); y1_ls = polyval(polyfit(x,y,1),x); y1_ls = plot(x, y1_ls, 'Color', 'k');
    [r1, p1] = corrcoef(x, y);
    x = AlphaPSD_OA; y = AlphaMSE_OA; 
    l3 = scatter(x,y, 'filled', 'r'); y3_ls = polyval(polyfit(x,y,1),x); y3_ls = plot(x, y3_ls, 'Color', 'r');
    [r3, p3] = corrcoef(x, y);
    legend([y1_ls, y3_ls], {['YA Alpha: r = ', num2str(round(r1(2),2)), ', p = ' num2str(round(p1(2),3))], ...
        ['OA Alpha: r = ', num2str(round(r3(2),2)), ', p = ' num2str(round(p3(2),3))]}); legend('boxoff');
    xlabel('Power (log10)'); ylabel('Entropy');
    title('Parietooccipital Alpha')
subplot(1,2,2); cla; hold on;
    x = BetaPSD_YA; y = BetaMSE_YA; 
    l2 = scatter(x,y, 'filled', 'k'); y2_ls = polyval(polyfit(x,y,1),x); y2_ls = plot(x, y2_ls, 'Color', 'k');
    [r2, p2] = corrcoef(x, y); 
    x = BetaPSD_OA; y = BetaMSE_OA; 
    l4 = scatter(x,y, 'filled', 'r'); y4_ls = polyval(polyfit(x,y,1),x); y4_ls = plot(x, y4_ls, 'Color', 'r');
    [r4, p4] = corrcoef(x, y); 
    legend([y2_ls, y4_ls], {['YA Beta: r = ', num2str(round(r2(2),2)), ', p = ' num2str(round(p2(2),3))],...
        ['OA Beta: r = ', num2str(round(r4(2),2)), ', p = ' num2str(round(p4(2),3))]}); legend('boxoff');
    xlabel('Power (log10)'); ylabel('Entropy');
    title('Central Beta')
set(findall(gcf,'-property','FontSize'),'FontSize',18)
