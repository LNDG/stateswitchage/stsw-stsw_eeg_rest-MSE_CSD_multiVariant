% CBPA of MSE output (Let's start with vanilla)

addpath('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/A_MSE_CSD_multiVariant/T_tools/fieldtrip-20170904'); ft_defaults;

pn.dataPath = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/A_MSE_CSD_multiVariant/B_data/C_MSE_Output_v1/';

IDs{1} = {'1117';'1118';'1120';'1124';'1126';'1131';'1132';'1135';'1136';'1138';...
    '1144';'1151';'1158';'1160';'1163';'1164';'1167';'1169';'1172';'1173';...
    '1178';'1182';'1215';'1216';'1219';'1221';'1223';'1227';'1228';...
    '1233';'1234';'1237';'1239';'1240';'1243';'1245';'1247';'1250';'1252';...
    '1257';'1261';'1265';'1266';'1268';'1270';'1276';'1281'};

IDs{2} = {'2104';'2107';'2108';'2112';'2118';'2120';'2121';'2123';'2125';'2129';...
    '2130';'2131';'2132';'2133';'2134';'2135';'2139';'2140';'2145';'2147';...
    '2149';'2157';'2160';'2202';'2203';'2205';'2206';'2209';'2210';...
    '2211';'2213';'2214';'2215';'2216';'2217';'2219';'2222';'2224';'2226';...
    '2227';'2236';'2237';'2238';'2241';'2244';'2246';'2248';'2250';'2251';...
    '2252';'2258';'2261'};

conds = {'EC'; 'EO'};

mseMerged = cell(1,1);
for indGroup = 1:2
    for indID = 1:numel(IDs{indGroup})
        for indCond = 1:2
            try
            curID = IDs{indGroup}{indID};
            % vanilla
            load([pn.dataPath, curID, '_',conds{indCond},'_MSE_OUT_vanilla.mat']);
            mseMerged{indGroup,indCond}.MSEVanilla(indID,:,:) = mse.sampen(1:60,:);
            mseMerged{indGroup,indCond}.Rlog10Vanilla(indID,:,:) = log10(squeeze(mse.r(:,:,:,1:60)))';
            % pointavg
            load([pn.dataPath, curID, '_',conds{indCond},'_MSE_OUT_pointavg.mat']);
            mseMerged{indGroup,indCond}.MSEPointavg(indID,:,:) = mse.sampen(1:60,:);
            mseMerged{indGroup,indCond}.Rlog10Pointavg(indID,:,:) = log10(squeeze(mse.r(:,:,:,1:60)))';
            % filtskip (LP)
            load([pn.dataPath, curID, '_',conds{indCond},'_MSE_OUT_filtskip.mat']);
            mseMerged{indGroup,indCond}.MSEfiltskip(indID,:,:) = mse.sampen(1:60,:);
            mseMerged{indGroup,indCond}.Rlog10filtskip(indID,:,:) = log10(squeeze(nanmean(mse.r(:,:,:,1:60),3)))';
            % filtskip (HP)
            load([pn.dataPath, curID, '_',conds{indCond},'_MSE_OUT_hp.mat']);
            mseMerged{indGroup,indCond}.MSEhp(indID,:,:) = mse.sampen(1:60,:);
            mseMerged{indGroup,indCond}.Rlog10hp(indID,:,:) = log10(squeeze(nanmean(mse.r(:,:,:,1:60),3)))';
            % filtskip (BP)
            load([pn.dataPath, curID, '_',conds{indCond},'_MSE_OUT_bp.mat']);
            mseMerged{indGroup,indCond}.MSEbp(indID,:,:) = mse.sampen(1:60,:);
            mseMerged{indGroup,indCond}.Rlog10bp(indID,:,:) = log10(squeeze(nanmean(mse.r(:,:,:,1:60),3)))';
            % add info
            mseMerged{indGroup,indCond}.dimord = 'subj_chan_freq';
            mseMerged{indGroup,indCond}.label = mse.label(1:60);
            mseMerged{indGroup,indCond}.timescales = mse.timescales;
            mseMerged{indGroup,indCond}.freq = (1./[1:41])*250;
            mseMerged{indGroup,indCond}.time = mse.time;
            catch
                disp([curID, ' ',conds{indCond}, ' skipped']);
                continue
            end
        end
    end
end

methodLabels = {'MSEVanilla', 'MSEPointavg', 'MSEfiltskip', 'MSEhp', 'MSEbp', ...
    'Rlog10Vanilla', 'Rlog10Pointavg', 'Rlog10filtskip', 'Rlog10hp', 'Rlog10bp'};

%% CBPA: within-group: EC vs. EO

% prepare_neighbours determines what sensors may form clusters
cfg_neighb.method       = 'template';
cfg_neighb.template     = 'elec1010_neighb.mat';
cfg_neighb.channel      = mseMerged{1,1}.label;

cfgStat = [];
cfgStat.channel          = 'all';
cfgStat.method           = 'montecarlo';
cfgStat.avgoverchan      = 'yes';
cfgStat.statistic        = 'ft_statfun_depsamplesT';
cfgStat.correctm         = 'cluster';
cfgStat.clusteralpha     = 0.05;
cfgStat.clusterstatistic = 'maxsum';
%cfgStat.minnbchan        = 2;
cfgStat.tail             = 0;
cfgStat.clustertail      = 0;
cfgStat.alpha            = 0.025;
cfgStat.numrandomization = 500;
cfgStat.neighbours       = [];

subj = size(mseMerged{1,1}.MSEVanilla,1);
conds = 2;
design = zeros(2,conds*subj);
for indCond = 1:conds
for i = 1:subj
    design(1,(indCond-1)*subj+i) = indCond;
    design(2,(indCond-1)*subj+i) = i;
end
end
cfgStat.design   = design;
cfgStat.ivar     = 1;
cfgStat.uvar     = 2;

for indMethod = 1:numel(methodLabels)
    cfgStat.parameter = methodLabels{indMethod};
    [stat{1,indMethod}] = ft_freqstatistics(cfgStat, mseMerged{1,1}, mseMerged{1,2});
end

subj = size(mseMerged{2,1}.MSEVanilla,1);
conds = 2;
design = zeros(2,conds*subj);
for indCond = 1:conds
for i = 1:subj
    design(1,(indCond-1)*subj+i) = indCond;
    design(2,(indCond-1)*subj+i) = i;
end
end
cfgStat.design   = design;
cfgStat.ivar     = 1;
cfgStat.uvar     = 2;

for indMethod = 1:numel(methodLabels)
    cfgStat.parameter = methodLabels{indMethod};
    [stat{2,indMethod}] = ft_freqstatistics(cfgStat, mseMerged{2,1}, mseMerged{2,2});
end

%% CBPA: between-group: EC: YA vs. OA; EO: YA vs. OA

cfgStat = [];
cfgStat.channel          = 'all';
cfgStat.method           = 'montecarlo';
cfgStat.avgoverchan      = 'yes';
cfgStat.statistic        = 'ft_statfun_indepsamplesT';
cfgStat.correctm         = 'cluster';
cfgStat.clusteralpha     = 0.05;
cfgStat.clusterstatistic = 'maxsum';
%cfgStat.minnbchan        = 2;
cfgStat.tail             = 0;
cfgStat.clustertail      = 0;
cfgStat.alpha            = 0.025;
cfgStat.numrandomization = 500;
cfgStat.neighbours       = [];

N_1 = size(mseMerged{1,1}.MSEVanilla,1);
N_2 = size(mseMerged{2,1}.MSEVanilla,1);
cfgStat.design = zeros(1,N_1+N_2);
cfgStat.design(1,1:N_1) = 1;
cfgStat.design(1,N_1+1:end) = 2;
    
for indMethod = 1:numel(methodLabels)
    cfgStat.parameter = methodLabels{indMethod};
    [stat{3,indMethod}] = ft_freqstatistics(cfgStat, mseMerged{1,1}, mseMerged{2,1});
    [stat{4,indMethod}] = ft_freqstatistics(cfgStat, mseMerged{1,2}, mseMerged{2,2});
end

save(['/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/A_MSE_CSD_multiVariant/B_data/E2_CBPA_Age_Condition_chanAvg.mat'], 'stat', 'methodLabels')