datahigh = load('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/A_MSE_CSD_multiVariant/B_data/B_MSE_Segmented_Dim_Input/1243_EO_MSE_IN.mat');
datalow = load('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/A_MSE_CSD_multiVariant/B_data/B_MSE_Segmented_Dim_Input/2209_EO_MSE_IN.mat');

datahigh = datahigh.data;
datalow = datalow.data;

%% filter to low-frequency power 


% figure; imagesc(datahigh.trial{1})
% figure; imagesc(datahigh.trial{2})

addpath('/Users/kosciessa/Desktop/mntTardis/STSWD_Rest/WIP_eeg/A_MSE_CSD_multiVariant/T_tools/fieldtrip-20170904/'); ft_defaults;

% filter

data = datahigh;
sc = 17; s = 1;

fs = 256;
nyquist = (fs/2);
fcLowPass = (1/sc)*nyquist;
fcHighPass = (1/(sc+1))*nyquist;
[B,A] = butter(6,fcLowPass/nyquist);            % define low-pass filter: https://de.mathworks.com/help/signal/ref/butter.html
[D,C] = butter(6,fcHighPass/nyquist, 'high');   % define high-pass filter

padlength = ceil(size(data.trial{1},2)./2); 
x_pad = cellfun(@(a) ft_preproc_padding(a, 'mean', padlength), data.trial, 'UniformOutput', false );    % add padding
x_pad = cellfun(@transpose, x_pad, 'UniformOutput', false);                                                 % transpose for filtfilt: time x chan
resamp_x_pad = cellfun(@(x_pad) filtfilt(B,A,x_pad), x_pad, 'UniformOutput', false );                       % low-pass filter data
resamp_x_pad = cellfun(@(resamp_x_pad) filtfilt(D,C,resamp_x_pad), resamp_x_pad, 'UniformOutput', false );  % high-pass filter data
resamp_x_pad = cellfun(@transpose, resamp_x_pad, 'UniformOutput', false);                                   % transpose back : chan x time again
resamp_x = cellfun(@(resamp_x_pad) ft_preproc_padding(resamp_x_pad, 'remove', padlength), ...                % remove padding
    resamp_x_pad, 'UniformOutput', false );
% create data_filt structure 
data_filt = data;
data_filt.trial = resamp_x;
clear resamp_* x_pad;

data_filt_high = data_filt;

%% timelock events

cfg = [];
cfg.keeptrials = 'yes';
cfg.vartrllength = 2;
filt_timeLock_High = ft_timelockanalysis(cfg, data_filt_high);

%%

data = datalow;
sc = 17; s = 1;

fs = 256;
nyquist = (fs/2);
fcLowPass = (1/sc)*nyquist;
fcHighPass = (1/(sc+1))*nyquist;
[B,A] = butter(6,fcLowPass/nyquist);            % define low-pass filter: https://de.mathworks.com/help/signal/ref/butter.html
[D,C] = butter(6,fcHighPass/nyquist, 'high');   % define high-pass filter

padlength = ceil(size(data.trial{1},2)./2); 
x_pad = cellfun(@(a) ft_preproc_padding(a, 'mean', padlength), data.trial, 'UniformOutput', false );    % add padding
x_pad = cellfun(@transpose, x_pad, 'UniformOutput', false);                                                 % transpose for filtfilt: time x chan
resamp_x_pad = cellfun(@(x_pad) filtfilt(B,A,x_pad), x_pad, 'UniformOutput', false );                       % low-pass filter data
resamp_x_pad = cellfun(@(resamp_x_pad) filtfilt(D,C,resamp_x_pad), resamp_x_pad, 'UniformOutput', false );  % high-pass filter data
resamp_x_pad = cellfun(@transpose, resamp_x_pad, 'UniformOutput', false);                                   % transpose back : chan x time again
resamp_x = cellfun(@(resamp_x_pad) ft_preproc_padding(resamp_x_pad, 'remove', padlength), ...                % remove padding
    resamp_x_pad, 'UniformOutput', false );
% create data_filt structure 
data_filt = data;
data_filt.trial = resamp_x;
clear resamp_* x_pad;

data_filt_low = data_filt;

%% timelock events

cfg = [];
cfg.keeptrials = 'yes';
cfg.vartrllength = 2;
filt_timeLock_Low = ft_timelockanalysis(cfg, data_filt_low);

idxChan = 30;

h = figure('units','normalized','position',[.1 .1 .5 .7]);
subplot(1,2,1); 
    imagesc(filt_timeLock_High.time,[],squeeze(filt_timeLock_High.trial(:,idxChan,:)))
    title('High MSE subject'); xlabel('Time (s)'); ylabel('Trials');
subplot(1,2,2); 
    imagesc(filt_timeLock_Low.time,[],squeeze(filt_timeLock_Low.trial(:,idxChan,:)))
    title('Low MSE subject'); xlabel('Time (s)'); ylabel('Trials');
set(findall(gcf,'-property','FontSize'),'FontSize',18)


%% ERPs

h = figure('units','normalized','position',[.1 .1 .5 .4]);
subplot(1,2,1);  hold on;
    plot(filt_timeLock_High.time,squeeze(nanmean(nanmean(filt_timeLock_High.trial(:,[30],:),2),1)), 'LineWidth', 2)
    title('High change subject'); xlabel('Time (s)'); ylabel('Trials (stacked by condition)');
    legend({'conservative', 'liberal'}); legend('boxoff');
    ylim([-1.2.*10^-4, 1.2.*10^-4]); xlim([-1.5 2]);
subplot(1,2,2); hold on;
    plot(filt_timeLock_Low.time,squeeze(nanmean(nanmean(filt_timeLock_Low.trial(:,[30],:),2),1)), 'LineWidth', 2)
    title('Low change subject'); xlabel('Time (s)'); ylabel('Trials (stacked by condition)');
    legend({'conservative', 'liberal'}); legend('boxoff');
    ylim([-1.2.*10^-4, 1.2.*10^-4]); xlim([-1.5 2]);
    suptitle('FP1, FP2')
set(findall(gcf,'-property','FontSize'),'FontSize',18)


%% get instantaneous phase

cfg = [];
cfg.hilbert = 'angle';
[filt_timeLock_Low_angle] = ft_preprocessing(cfg, data_filt_low);

cfg = [];
cfg.keeptrials = 'yes';
cfg.vartrllength = 2;
filt_timeLock_Low_angle = ft_timelockanalysis(cfg, filt_timeLock_Low_angle);

cfg = [];
cfg.hilbert = 'angle';
[filt_timeLock_High_angle] = ft_preprocessing(cfg, data_filt_high);

cfg = [];
cfg.keeptrials = 'yes';
cfg.vartrllength = 2;
filt_timeLock_High_angle = ft_timelockanalysis(cfg, filt_timeLock_High_angle);


timeIdx = find(filt_timeLock_Low_angle.time>=0 & filt_timeLock_Low_angle.time<=1.5);
time = filt_timeLock_Low_angle.time(timeIdx);

    Low_cons = squeeze(filt_timeLock_Low_angle.trial(:,30,timeIdx));

figure; imagesc(squeeze(filt_timeLock_Low_angle.trial(:,30,timeIdx)))
figure; imagesc(squeeze(filt_timeLock_High_angle.trial(:,30,timeIdx)))

figure; imagesc(squeeze(filt_timeLock_Low_angle.trial(:,30,:)))
figure; imagesc(squeeze(filt_timeLock_High_angle.trial(:,30,:)))

for indTime = 1:size(Low_cons,2)
    k = Low_cons(:,indTime);
    ITPC(indTime) = abs(mean(exp(1i*k)));
end

%% sort timeseries by phase at zero


figure;
timeIdx = find(filt_timeLock_Low_angle.time==0);
subplot(2,1,1);
    tmpPhase = squeeze(filt_timeLock_Low_angle.trial(:,30,timeIdx));
    [~, sortInd] = sort(tmpPhase, 'descend');
    imagesc(filt_timeLock_Low.time,[],squeeze(nanmean(zscore(filt_timeLock_Low.trial(sortInd,30,:),[],3),2)),[-.003 .003])
    title('Low entropy subject'); xlabel('Time (s)'); ylabel('Trial (sorted by phase @t=0)')
subplot(2,1,2);
    tmpPhase = squeeze(filt_timeLock_High_angle.trial(:,30,timeIdx));
    [~, sortInd] = sort(tmpPhase, 'descend');
    imagesc(filt_timeLock_High.time,[],squeeze(nanmean(zscore(filt_timeLock_High.trial(sortInd,30,:),[],3),2)),[-.003 .003])
    title('High entropy subject'); xlabel('Time (s)'); ylabel('Trial (sorted by phase @t=0)')

colormap('parula')
set(findall(gcf,'-property','FontSize'),'FontSize',18)

%% plot for different channels

figure;
for idxChan = 1:48
    subplot(2,2,1);
        tmpPhase = squeeze(filt_timeLock_Low_angle.trial(:,idxChan,timeIdx));
        [~, sortInd] = sort(tmpPhase, 'descend');
        imagesc(filt_timeLock_Low.time,[],squeeze(nanmean(filt_timeLock_Low.trial(sortInd,idxChan,:),2)),[-.003 .003])
        title('Low diff subject, conservative'); xlabel('Time (s)'); ylabel('Trial (sorted by phase @t=0)')
    
    subplot(2,2,3);
        tmpPhase = squeeze(filt_timeLock_High_angle.trial(:,idxChan,timeIdx));
        [~, sortInd] = sort(tmpPhase, 'descend');
        imagesc(filt_timeLock_High.time,[],squeeze(nanmean(filt_timeLock_High.trial(sortInd,idxChan,:),2)),[-.003 .003])
        title('High diff subject, conservative'); xlabel('Time (s)'); ylabel('Trial (sorted by phase @t=0)')
   
    colormap('parula')
    set(findall(gcf,'-property','FontSize'),'FontSize',18)
    suptitle('POz')
    pause(1)
end

