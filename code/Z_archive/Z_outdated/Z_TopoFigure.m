%% PSD topographies

% load stat output

pn.root     = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/';
pn.dataIn   = [pn.root, 'B_analyses/B_FFT/B_data/A_spectra/'];
pn.dataOut  = [pn.root, 'B_analyses/B_FFT/B_data/'];
pn.tools    = [pn.root, 'B_analyses/A_MSE_CSD_multiVariant/T_tools/'];
addpath([pn.tools, 'fieldtrip-20170904/']);
ft_defaults

load('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/B_FFT/B_data/C_stat.mat')

% topographies: 8-12 Hz; 32-64 Hz

pn.root         = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/';
pn.tools        = [pn.root, 'B_analyses/S2_TFR/T_tools/']; addpath(pn.tools);
addpath([pn.tools, 'fieldtrip-20170904/']); ft_defaults;

cfg = [];
cfg.layout = 'acticap-64ch-standard2.mat';
cfg.parameter = 'powspctrm';
cfg.comment = 'no';
cfg.colorbar = 'SouthOutside';
cfg.zlim = [-3 3];

h = figure('units','normalized','position',[.1 .1 .7 .4]);
subplot(1,2,1)
    plotData = [];
    plotData.label = stat{1,1}.label; % {1 x N}
    plotData.dimord = 'chan';
    idxFreqs = stat{4,1}.freq >= 8 & stat{4,1}.freq <= 15;
    cfg.highlight = 'on';
    cfg.highlightchannel = stat{4,1}.label(max(stat{4,1}.mask(:,idxFreqs),[],2));
    cfg.highlightcolor = [1 0 0];
    plotData.powspctrm = squeeze(nanmean(stat{4,1}.stat(:,idxFreqs),2));
    ft_topoplotER(cfg,plotData);
    title('8-15 Hz OA > YA')
subplot(1,2,2)
    plotData = [];
    plotData.label = stat{1,1}.label; % {1 x N}
    plotData.dimord = 'chan';
    idxFreqs = stat{4,1}.freq >= 32 & stat{4,1}.freq <= 64;
    cfg.highlight = 'on';
    cfg.highlightchannel = stat{4,1}.label(max(stat{4,1}.mask(:,idxFreqs),[],2));
    cfg.highlightcolor = [1 0 0];
    plotData.powspctrm = squeeze(nanmean(stat{4,1}.stat(:,idxFreqs),2));
    ft_topoplotER(cfg,plotData);
    title('32-64 Hz OA > YA')
set(findall(gcf,'-property','FontSize'),'FontSize',18)
pn.plotFolder = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/B_FFT/C_figures/';
figureName = 'C2_toposEO_YAvsOA';

saveas(h, [pn.plotFolder, figureName], 'fig');
saveas(h, [pn.plotFolder, figureName], 'epsc');
saveas(h, [pn.plotFolder, figureName], 'png');
