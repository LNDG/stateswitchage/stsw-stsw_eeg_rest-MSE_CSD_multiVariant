% find optimized band-pass filter settings for alpha rhythmicity 

cfg                     = [];
cfg.toi                 = 4;
cfg.timwin              = 8;
cfg.timescales          = 1:42;
cfg.coarsegrainmethod   = 'filtskip';
cfg.filtmethod          = 'bp';
cfg.m                   = 2;
cfg.r                   = 0.5;
cfg.recompute_r         = 'perscale_toi_sp';

for indFreq = 4
    simData.trial = squeeze(simDataOrig.trial(:,indFreq,:))';
    mse{indFreq} = ft_entropyanalysis(cfg, simData);
end

for s = 1:40
    sc = timescales(s);
    fs = data.fsample;
    nyquist = fs/2;
    fcLowPass = (1/(sc))*nyquist;
    if fcLowPass == nyquist
        fcLowPass = fcLowPass-1;
    end
    if s == numel(timescales)
        fcHighPass = 0.5;
    else
        fcHighPass = (1/(timescales(s+1)))*nyquist;
    end
    [B,A] = butter(6,fcLowPass/nyquist);            % define low-pass filter: https://de.mathworks.com/help/signal/ref/butter.html
    [D,C] = butter(6,fcHighPass/nyquist, 'high');   % define high-pass filter
    cfg.freq(1,s) = fcLowPass;
    cfg.freq(2,s) = fcHighPass;

    padlength = ceil(size(data.trial{1},2)./2); % use half the length of trial 1 as padding (JQK)
    x_pad = cellfun(@(a) ft_preproc_padding(a, 'mean', padlength), data.trial, 'UniformOutput', false );    % add padding
    x_pad = cellfun(@transpose, x_pad, 'UniformOutput', false);                                                 % transpose for filtfilt: time x chan
    if sc == 1 % only HPF
       resamp_x_pad = cellfun(@(x_pad) filtfilt(D,C,x_pad), x_pad, 'UniformOutput', false );  % high-pass filter data
    else
        resamp_x_pad = cellfun(@(x_pad) filtfilt(B,A,x_pad), x_pad, 'UniformOutput', false );                       % low-pass filter data
        resamp_x_pad = cellfun(@(resamp_x_pad) filtfilt(D,C,resamp_x_pad), resamp_x_pad, 'UniformOutput', false );  % high-pass filter data
    end
    resamp_x_pad = cellfun(@transpose, resamp_x_pad, 'UniformOutput', false);                                   % transpose back : chan x time again
    resamp_x = cellfun(@(resamp_x_pad) ft_preproc_padding(resamp_x_pad, 'remove', padlength), ...                % remove padding
        resamp_x_pad, 'UniformOutput', false );
    %figure; hold on; plot(resamp_x{1}(1,:)); plot(data.trial{1}(1,:))
    % create data_filt structure
    data_filt = data;
    data_filt.trial = resamp_x;
    clear resamp_* x_pad;

    ExampleSeries(sc,:) = data_filt.trial{1}(15,:);

    ExampleSeriesOrig(sc,:) = data_filt.trial{1}(1,:);
end

figure; imagesc(zscore(ExampleSeries,[],2))
figure; imagesc(zscore(ExampleSeriesOrig,[],2))

figure; imagesc(ExampleSeries)

figure; imagesc(zscore(ExampleSeriesOrig-ExampleSeries,[],2))


figure; plot(log10(cfg.freq(1,:)), std(ExampleSeries,[],2))


figure; plot(ExampleSeries(5,:))

figure; hold on;
plot(ExampleSeries(8,:))
plot(ExampleSeriesOrig(8,:))

figure; hold on;
for indScale = 1:40
    cla;
    plot(ExampleSeries(indScale,:))
    plot(ExampleSeriesOrig(indScale,:))
    pause(.5)
end

cfg.freq(1,8)
cfg.freq(1,9)
cfg.freq(1,10)

figure; hold on;
plot(ExampleSeries(11,:))
plot(ExampleSeriesOrig(11,:))
