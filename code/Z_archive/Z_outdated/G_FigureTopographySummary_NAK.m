% create overview figure of entropy, power and PSD slope age differences

%% add NAK colormap

load('/Users/kosciessa/Downloads/colormap_jetlightgray.mat')

cmap = colormap('parula');

%% plot topographies of MSE age differences

pn.root = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/A_MSE_CSD_multiVariant/';
addpath([pn.root, 'T_tools/fieldtrip-20170904']); ft_defaults;
load([pn.root, 'B_data/E2_CBPA_Age_Condition.mat'], 'stat', 'methodLabels')
contrastLabels = {'YA: EC vs. EO'; 'OA: EC vs. EO'; 'EC: OA vs. YA'; 'EO: OA vs. YA'};

cfg = [];
cfg.layout = 'acticap-64ch-standard2.mat';
cfg.parameter = 'powspctrm';
cfg.comment = 'no';
cfg.colorbar = 'EastOutside';
cfg.style = 'both';
cfg.colormap = cmap;

% Note that the internal FieldTrip cluster statistics reorder the entropy
% timescales according to increasing frequency.

h = figure('units','normalized','position',[.1 .1 .45 .7]);
subplot(3,2,1)
    cfg.highlight = 'on';
    cfg.highlightchannel = stat{1,1}.label(max(stat{4,1}.mask(:,41),[],2));
    cfg.highlightcolor = [0 0 0];
    cfg.highlightsymbol = '*';
    cfg.zlim = [-5 5];
    plotData = [];
    plotData.label = stat{1,1}.label; % {1 x N}
    plotData.dimord = 'chan';
    plotData.powspctrm = squeeze(nanmean(stat{4,1}.stat(:,41),2));
    ft_topoplotER(cfg,plotData);
    cb = colorbar; set(get(cb,'title'),'string','t values');
    title('Fast scale entropy OA vs. YA')
subplot(3,2,2)
    cfg.highlight = 'on';
    cfg.highlightchannel = stat{1,1}.label(max(stat{4,1}.mask(:,1:5),[],2));
    cfg.highlightcolor = [0 0 0];
    cfg.zlim = [-5 5];
    plotData = [];
    plotData.label = stat{1,1}.label; % {1 x N}
    plotData.dimord = 'chan';
    plotData.powspctrm = squeeze(nanmean(stat{4,1}.stat(:,1:5),2));
    ft_topoplotER(cfg,plotData);
    cb = colorbar; set(get(cb,'title'),'string','t values');
    title('Coarse (slow) scale entropy OA vs. YA')

%% plot topographies of spectral power age differences

load('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/B_FFT/B_data/C_stat.mat')

subplot(3,2,3)
    plotData = [];
    plotData.label = stat{1,1}.label; % {1 x N}
    plotData.dimord = 'chan';
    cfg.zlim = [-3 3];
    idxFreqs = stat{4,1}.freq >= 8 & stat{4,1}.freq <= 15;
    cfg.highlight = 'on';
    cfg.highlightchannel = stat{4,1}.label(max(stat{4,1}.mask(:,idxFreqs),[],2));
    cfg.highlightcolor = [0 0 0];
    plotData.powspctrm = squeeze(nanmean(stat{4,1}.stat(:,idxFreqs),2));
    ft_topoplotER(cfg,plotData);
    cb = colorbar; set(get(cb,'title'),'string','t values');
    title('8-15 Hz Power OA vs. YA')
subplot(3,2,4)
    plotData = [];
    plotData.label = stat{1,1}.label; % {1 x N}
    plotData.dimord = 'chan';
    cfg.zlim = [-3 3];
    idxFreqs = stat{4,1}.freq >= 32 & stat{4,1}.freq <= 64;
    cfg.highlight = 'on';
    cfg.highlightchannel = stat{4,1}.label(max(stat{4,1}.mask(:,idxFreqs),[],2));
    cfg.highlightcolor = [0 0 0];
    plotData.powspctrm = squeeze(nanmean(stat{4,1}.stat(:,idxFreqs),2));
    ft_topoplotER(cfg,plotData);
    cb = colorbar; set(get(cb,'title'),'string','t values');
    title('32-64 Hz Power OA vs. YA')

%% plot topographies of PSD slope differences

load('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/B_FFT/B_data/D_PSDslopes.mat')
load('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/B_FFT/B_data/D2_CBPASlopeAge.mat')

subplot(3,2,5);
    cfg.highlight = 'yes';
    cfg.highlightchannel =  grandavg_EC.label(stat_zero.mask);
    cfg.highlightcolor = [0 0 0];
    cfg.zlim = [-.03 .03];
    plotData = [];
    plotData.label = grandavg_EC.label; % {1 x N}
    plotData.dimord = 'chan';
    plotData.powspctrm = squeeze(nanmean(linFit_2_30_EO,1))';
    ft_topoplotER(cfg,plotData);
    cb = colorbar; set(get(cb,'title'),'string','slope');
    title('Spectral slopes across age vs. 0')
        
subplot(3,2,6);
    cfg.zlim = [-7 7];
    cfg.highlight = 'yes';
    cfg.highlightchannel =  grandavg_EC.label(stat.mask);
    cfg.highlightcolor = [0 0 0];
    plotData.powspctrm = stat.stat;
    ft_topoplotER(cfg,plotData);
    cb = colorbar; set(get(cb,'title'),'string','t values');
    title('Spectral slopes: OA vs. YA');

set(findall(gcf,'-property','FontSize'),'FontSize',17)

%% save figure

pn.plotFolder = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/A_MSE_CSD_multiVariant/C_figures/';
figureName = 'G_FigureTopographySummary';
saveas(h, [pn.plotFolder, figureName], 'epsc');
saveas(h, [pn.plotFolder, figureName], 'png');