% compare individual bandpass beta MSE and spectral power: eyes closed vs. eyes open

clear all; clc; restoredefaultpath

% add FieldTrip
addpath('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/A_MSE_CSD_multiVariant/T_tools/fieldtrip-20170904'); ft_defaults;

% add convertPtoExponential
addpath('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/A_MSE_CSD_multiVariant/T_tools/')

%% load MSE

load('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/A_MSE_CSD_multiVariant/B_data/E_mseMerged.mat', 'mseMerged', 'IDs')

%% load PSD

load('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/B_FFT/B_data/B_FFT_grandavg_individual.mat')

commonYA = info.IDs_all(ismember(info.IDs_all, IDs{1}));
commonOA = info.IDs_all(ismember(info.IDs_all, IDs{2}));

idx_YA_fft = ismember(info.IDs_all, commonYA);
idx_OA_fft = ismember(info.IDs_all, commonOA);
idx_YA_mse = ismember(IDs{1}, commonYA);
idx_OA_mse = ismember(IDs{2}, commonOA);

%% load stats cluster

pn.root = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/A_MSE_CSD_multiVariant/';
load([pn.root, 'B_data/E2_CBPA_Age_Condition_v2.mat'], 'stat', 'methodLabels')

%% correlate PSD with MSE

betaCluster = max(stat{1,5}.mask(:,23:27),[],2);
alphaCluster = max(stat{1,5}.mask(:,15:20),[],2);

BetaPSD_YA = squeeze(nanmean(nanmedian(log10(grandavg_EC.powspctrm(idx_YA_fft,betaCluster,grandavg_EC.freq > 12 & grandavg_EO.freq<18)),3),2))-...
    squeeze(nanmean(nanmedian(log10(grandavg_EO.powspctrm(idx_YA_fft,betaCluster,grandavg_EC.freq > 12 & grandavg_EO.freq<18)),3),2));
AlphaPSD_YA = squeeze(nanmean(nanmedian(log10(grandavg_EC.powspctrm(idx_YA_fft,alphaCluster,grandavg_EC.freq > 8 & grandavg_EO.freq<12)),3),2))-...
    squeeze(nanmean(nanmedian(log10(grandavg_EO.powspctrm(idx_YA_fft,alphaCluster,grandavg_EC.freq > 8 & grandavg_EO.freq<12)),3),2));

BetaMSE_YA = squeeze(nanmean(nanmedian(mseMerged{1,1}.MSEbp(:,betaCluster,mseMerged{1,1}.freq> 12 & mseMerged{1,1}.freq< 18),3),2))-...
    squeeze(nanmean(nanmedian(mseMerged{1,2}.MSEbp(:,betaCluster,mseMerged{1,1}.freq> 12 & mseMerged{1,1}.freq< 18),3),2));
AlphaMSE_YA = squeeze(nanmean(nanmedian(mseMerged{1,1}.MSEbp(:,alphaCluster,mseMerged{1,1}.freq> 10 & mseMerged{1,1}.freq< 12),3),2))-...
    squeeze(nanmean(nanmedian(mseMerged{1,2}.MSEbp(:,alphaCluster,mseMerged{1,1}.freq> 10 & mseMerged{1,1}.freq< 12),3),2));

BetaMSE_YA_Vanilla = squeeze(nanmean(nanmedian(mseMerged{1,1}.MSEVanilla(:,1,betaCluster,mseMerged{1,1}.freq> 12 & mseMerged{1,1}.freq< 18),4),3))-...
    squeeze(nanmean(nanmedian(mseMerged{1,2}.MSEVanilla(:,1,betaCluster,mseMerged{1,1}.freq> 12 & mseMerged{1,1}.freq< 18),4),3));
AlphaMSE_YA_Vanilla = squeeze(nanmean(nanmedian(mseMerged{1,1}.MSEVanilla(:,1,alphaCluster,mseMerged{1,1}.freq> 10 & mseMerged{1,1}.freq< 12),4),3))-...
    squeeze(nanmean(nanmedian(mseMerged{1,2}.MSEVanilla(:,1,alphaCluster,mseMerged{1,1}.freq> 10 & mseMerged{1,1}.freq< 12),4),3));

BetaPSD_OA = squeeze(nanmean(nanmedian(log10(grandavg_EC.powspctrm(idx_OA_fft,betaCluster,grandavg_EO.freq > 12 & grandavg_EO.freq<18)),3),2))-...
    squeeze(nanmean(nanmedian(log10(grandavg_EO.powspctrm(idx_OA_fft,betaCluster,grandavg_EO.freq > 12 & grandavg_EO.freq<18)),3),2));
AlphaPSD_OA = squeeze(nanmean(nanmedian(log10(grandavg_EC.powspctrm(idx_OA_fft,alphaCluster,grandavg_EO.freq > 8 & grandavg_EO.freq<12)),3),2))-...
    squeeze(nanmean(nanmedian(log10(grandavg_EO.powspctrm(idx_OA_fft,alphaCluster,grandavg_EO.freq > 8 & grandavg_EO.freq<12)),3),2));

BetaMSE_OA = squeeze(nanmean(nanmedian(mseMerged{2,1}.MSEbp(:,betaCluster,mseMerged{1,1}.freq> 12 & mseMerged{1,1}.freq< 18),3),2))-...
    squeeze(nanmean(nanmedian(mseMerged{2,2}.MSEbp(:,betaCluster,mseMerged{1,1}.freq> 12 & mseMerged{1,1}.freq< 18),3),2));
AlphaMSE_OA = squeeze(nanmean(nanmedian(mseMerged{2,1}.MSEbp(:,alphaCluster,mseMerged{1,1}.freq> 8 & mseMerged{1,1}.freq< 12),3),2))-...
    squeeze(nanmean(nanmedian(mseMerged{2,2}.MSEbp(:,alphaCluster,mseMerged{1,1}.freq> 8 & mseMerged{1,1}.freq< 12),3),2));

BetaMSE_OA_Vanilla = squeeze(nanmean(nanmedian(mseMerged{2,1}.MSEVanilla(:,1,betaCluster,mseMerged{1,1}.freq> 12 & mseMerged{1,1}.freq< 18),4),3))-...
    squeeze(nanmean(nanmedian(mseMerged{2,2}.MSEVanilla(:,1,betaCluster,mseMerged{1,1}.freq> 12 & mseMerged{1,1}.freq< 18),4),3));
AlphaMSE_OA_Vanilla = squeeze(nanmean(nanmedian(mseMerged{2,1}.MSEVanilla(:,1,alphaCluster,mseMerged{1,1}.freq> 10 & mseMerged{1,1}.freq< 12),4),3))-...
    squeeze(nanmean(nanmedian(mseMerged{2,2}.MSEVanilla(:,1,alphaCluster,mseMerged{1,1}.freq> 10 & mseMerged{1,1}.freq< 12),4),3));


h = figure('units','normalized','position',[.1 .1 .7 .4]);
subplot(1,2,1); cla; hold on;
    x = AlphaPSD_YA; y = AlphaMSE_YA; 
    l1 = scatter(x,y, 'filled', 'k'); y1_ls = polyval(polyfit(x,y,1),x); y1_ls = plot(x, y1_ls, 'Color', 'k');
    [r1, p1] = corrcoef(x, y);
    x = AlphaPSD_OA; y = AlphaMSE_OA; 
    l3 = scatter(x,y, 'filled', 'r'); y3_ls = polyval(polyfit(x,y,1),x); y3_ls = plot(x, y3_ls, 'Color', 'r');
    [r3, p3] = corrcoef(x, y);
    legend([y1_ls, y3_ls], {['YA Alpha: r = ', num2str(round(r1(2),2)), ', p = ' num2str(round(p1(2),3))], ...
        ['OA Alpha: r = ', num2str(round(r3(2),2)), ', p = ' num2str(round(p3(2),3))]}); legend('boxoff');
    xlabel('Power (log10)'); ylabel('Entropy');
    title('Parietooccipital Alpha')
subplot(1,2,2); cla; hold on;
    x = BetaPSD_YA; y = BetaMSE_YA; 
    l2 = scatter(x,y, 'filled', 'k'); y2_ls = polyval(polyfit(x,y,1),x); y2_ls = plot(x, y2_ls, 'Color', 'k');
    [r2, p2] = corrcoef(x, y); 
    x = BetaPSD_OA; y = BetaMSE_OA; 
    l4 = scatter(x,y, 'filled', 'r'); y4_ls = polyval(polyfit(x,y,1),x); y4_ls = plot(x, y4_ls, 'Color', 'r');
    [r4, p4] = corrcoef(x, y); 
    legend([y2_ls, y4_ls], {['YA Beta: r = ', num2str(round(r2(2),2)), ', p = ' num2str(round(p2(2),3))],...
        ['OA Beta: r = ', num2str(round(r4(2),2)), ', p = ' num2str(round(p4(2),3))]}); legend('boxoff');
    xlabel('Power (log10)'); ylabel('Entropy');
    title('Central Beta')
set(findall(gcf,'-property','FontSize'),'FontSize',18)

h = figure('units','normalized','position',[.1 .1 .7 .4]);
subplot(1,2,1); cla; hold on;
    x = AlphaMSE_YA_Vanilla; y = AlphaMSE_YA; 
    l1 = scatter(x,y, 'filled', 'k'); y1_ls = polyval(polyfit(x,y,1),x); y1_ls = plot(x, y1_ls, 'Color', 'k');
    [r1, p1] = corrcoef(x, y);
    x = AlphaMSE_OA_Vanilla; y = AlphaMSE_OA; 
    l3 = scatter(x,y, 'filled', 'r'); y3_ls = polyval(polyfit(x,y,1),x); y3_ls = plot(x, y3_ls, 'Color', 'r');
    [r3, p3] = corrcoef(x, y);
    legend([y1_ls, y3_ls], {['YA Alpha: r = ', num2str(round(r1(2),2)), ', p = ' num2str(round(p1(2),3))], ...
        ['OA Alpha: r = ', num2str(round(r3(2),2)), ', p = ' num2str(round(p3(2),3))]}); legend('boxoff');
    xlabel('Power (log10)'); ylabel('Entropy');
    title('Parietooccipital Alpha')
subplot(1,2,2); cla; hold on;
    x = BetaMSE_YA_Vanilla; y = BetaMSE_YA; 
    l2 = scatter(x,y, 'filled', 'k'); y2_ls = polyval(polyfit(x,y,1),x); y2_ls = plot(x, y2_ls, 'Color', 'k');
    [r2, p2] = corrcoef(x, y); 
    x = BetaMSE_OA_Vanilla; y = BetaMSE_OA; 
    l4 = scatter(x,y, 'filled', 'r'); y4_ls = polyval(polyfit(x,y,1),x); y4_ls = plot(x, y4_ls, 'Color', 'r');
    [r4, p4] = corrcoef(x, y); 
    legend([y2_ls, y4_ls], {['YA Beta: r = ', num2str(round(r2(2),2)), ', p = ' num2str(round(p2(2),3))],...
        ['OA Beta: r = ', num2str(round(r4(2),2)), ', p = ' num2str(round(p4(2),3))]}); legend('boxoff');
    xlabel('Power (log10)'); ylabel('Entropy');
    title('Central Beta')
set(findall(gcf,'-property','FontSize'),'FontSize',18)


%% correlate PSD with MSE in eyes closed condition

betaCluster = max(stat{4,5}.mask(:,23:27),[],2);
alphaCluster = max(stat{4,5}.mask(:,15:20),[],2);

BetaPSD_YA = squeeze(nanmean(nanmedian(log10(grandavg_EC.powspctrm(idx_YA_fft,betaCluster,grandavg_EC.freq > 12 & grandavg_EO.freq<18)),3),2));
AlphaPSD_YA = squeeze(nanmean(nanmedian(log10(grandavg_EC.powspctrm(idx_YA_fft,alphaCluster,grandavg_EC.freq > 8 & grandavg_EO.freq<12)),3),2));

BetaMSE_YA = squeeze(nanmean(nanmedian(mseMerged{1,1}.MSEbp(:,betaCluster,mseMerged{1,1}.freq> 12 & mseMerged{1,1}.freq< 18),3),2));
AlphaMSE_YA = squeeze(nanmean(nanmedian(mseMerged{1,1}.MSEbp(:,alphaCluster,mseMerged{1,1}.freq> 10 & mseMerged{1,1}.freq< 12),3),2));

BetaPSD_OA = squeeze(nanmean(nanmedian(log10(grandavg_EC.powspctrm(idx_OA_fft,betaCluster,grandavg_EO.freq > 12 & grandavg_EO.freq<18)),3),2));
AlphaPSD_OA = squeeze(nanmean(nanmedian(log10(grandavg_EC.powspctrm(idx_OA_fft,alphaCluster,grandavg_EO.freq > 8 & grandavg_EO.freq<12)),3),2));

BetaMSE_OA = squeeze(nanmean(nanmedian(mseMerged{2,1}.MSEbp(:,betaCluster,mseMerged{1,1}.freq> 12 & mseMerged{1,1}.freq< 18),3),2));
AlphaMSE_OA = squeeze(nanmean(nanmedian(mseMerged{2,1}.MSEbp(:,alphaCluster,mseMerged{1,1}.freq> 8 & mseMerged{1,1}.freq< 12),3),2));

h = figure('units','normalized','position',[.1 .1 .7 .4]);
subplot(1,2,1); cla; hold on;
    x = AlphaPSD_YA; y = AlphaMSE_YA; 
    l1 = scatter(x,y, 'filled', 'k'); y1_ls = polyval(polyfit(x,y,1),x); y1_ls = plot(x, y1_ls, 'Color', 'k');
    [r1, p1] = corrcoef(x, y);
    x = AlphaPSD_OA; y = AlphaMSE_OA; 
    l3 = scatter(x,y, 'filled', 'r'); y3_ls = polyval(polyfit(x,y,1),x); y3_ls = plot(x, y3_ls, 'Color', 'r');
    [r3, p3] = corrcoef(x, y);
    legend([y1_ls, y3_ls], {['YA Alpha: r = ', num2str(round(r1(2),2)), ', p = ' num2str(round(p1(2),3))], ...
        ['OA Alpha: r = ', num2str(round(r3(2),2)), ', p = ' num2str(round(p3(2),3))]}); legend('boxoff');
    xlabel('Power (log10)'); ylabel('Entropy');
    title('Parietooccipital Alpha')
subplot(1,2,2); cla; hold on;
    x = BetaPSD_YA; y = BetaMSE_YA; 
    l2 = scatter(x,y, 'filled', 'k'); y2_ls = polyval(polyfit(x,y,1),x); y2_ls = plot(x, y2_ls, 'Color', 'k');
    [r2, p2] = corrcoef(x, y); 
    x = BetaPSD_OA; y = BetaMSE_OA; 
    l4 = scatter(x,y, 'filled', 'r'); y4_ls = polyval(polyfit(x,y,1),x); y4_ls = plot(x, y4_ls, 'Color', 'r');
    [r4, p4] = corrcoef(x, y); 
    legend([y2_ls, y4_ls], {['YA Beta: r = ', num2str(round(r2(2),2)), ', p = ' num2str(round(p2(2),3))],...
        ['OA Beta: r = ', num2str(round(r4(2),2)), ', p = ' num2str(round(p4(2),3))]}); legend('boxoff');
    xlabel('Power (log10)'); ylabel('Entropy');
    title('Central Beta')
set(findall(gcf,'-property','FontSize'),'FontSize',18)
