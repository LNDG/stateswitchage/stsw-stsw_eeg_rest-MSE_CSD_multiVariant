% Plot results from MSE analysis

% 180129 | JQK adapted function for STSWD study YA

% Note: For study data, channel positions were already rearranged during
% preprocessing.

%% set up paths

pn.root = '/Users/kosciessa/Desktop/mntTardis/STSWD_Rest/WIP_eeg/A_MSE_CSD/';
pn.dataInOUT  = [pn.root, 'B_data/C_MSE_Output_v1/'];
addpath([pn.root, 'T_tools/qsub_tardis']);
addpath([pn.root, 'T_tools/functions_mse_time/']);
addpath([pn.root, 'T_tools/fieldtrip-20170904/']);
addpath([pn.root, 'T_tools/brewermap/']);
addpath([pn.root, 'T_tools/mseb/']);
ft_defaults

%% load data

load([pn.dataInOUT, 'STSWD_rest_mseavg_CSD.mat'], 'mseavg');

%% plot mMSE topography for each scale

scales   = [1:41]; % scale list
freqApp  = 500./[scales];

load([pn.root, 'B_data/B_MSE_Segmented_Dim_Input/1117_EC_MSE_IN.mat'])
layoutInfo.elec = data.elec; clear data;
layoutInfo.elec.chanpos = layoutInfo.elec.chanpos(1:60,:);
layoutInfo.elec.elecpos = layoutInfo.elec.elecpos(1:60,:);
layoutInfo.elec.label = layoutInfo.elec.label(1:60,:);

cfg = [];
cfg.layout = 'elec1010.lay';
cfg.parameter = 'powspctrm';
cfg.comment = 'no';
cfg.colorbar = 'no'; % plot no more colorbars

plotData = [];
plotData.label = layoutInfo.elec.label'; % {1 x N}
plotData.dimord = 'chan';

h = figure('units','normalized','position',[.1 .1 .9 .9]);
for indScale = 1:numel(scales)
    selectScale = scales(indScale);
    dataToPlot = squeeze(nanmean(nanmean(mseavg.dat(:,1,1:60,selectScale,:,1),4),1));
    subplot(6,7,indScale)
    plotData.powspctrm = dataToPlot;
    ft_topoplotER(cfg,plotData);
    title(['Scale ', num2str(scales(indScale)), ' Freq ', num2str(freqApp(indScale)/2)]);
end
pn.plotFolder = [pn.root, 'C_figures/'];
figureName = ['D_MSEtopography_1to41'];
saveas(h, [pn.plotFolder, figureName], 'png');
close(h);
