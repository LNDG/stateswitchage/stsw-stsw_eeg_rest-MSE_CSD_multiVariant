pn.data = '/Users/kosciessa/Desktop/mntTardis/STSWD_Rest/WIP_eeg/D_eBOSC_noDur/B_data/C_RhythmCharacteristics/';
load([pn.data, 'C2_EventsMerged.mat'], 'Events_Merged', 'EventsNonRhythm_Merged', 'EventsRhythm_Merged');

freq = 2.^[1:.125:6];
freq = freq(4:end-3);
 
% load stats cluster of bandpass difference

pn.root = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/A_MSE_CSD_multiVariant/';
load([pn.root, 'B_data/E2_CBPA_Age_Condition.mat'], 'stat', 'methodLabels')

% correlate with individual MSE values

load('/Users/kosciessa/Desktop/mntTardis/STSWD_Rest/WIP_eeg/A_MSE_CSD_multiVariant/B_data/E_mseMerged.mat')

addpath('/Users/kosciessa/Desktop/mntTardis/STSWD_Rest/WIP_eeg/D_eBOSC_noDur/T_tools')

betaCluster = max(stat{4,5}.mask(:,23:27),[],2);
alphaCluster = max(stat{4,5}.mask(:,15:20),[],2);

BetaMSE_YA = squeeze(nanmean(nanmedian(mseMerged{1,2}.MSEbp(:,betaCluster,mseMerged{1,1}.freq> 14 & mseMerged{1,1}.freq< 20),3),2));
BetaMSE_OA = squeeze(nanmean(nanmedian(mseMerged{2,2}.MSEbp(:,betaCluster,mseMerged{1,1}.freq> 14 & mseMerged{1,1}.freq< 20),3),2));

AlphaMSE_YA = squeeze(nanmean(nanmedian(mseMerged{1,2}.MSEbp(:,alphaCluster,mseMerged{1,1}.freq> 8 & mseMerged{1,1}.freq< 12),3),2));
AlphaMSE_OA = squeeze(nanmean(nanmedian(mseMerged{2,2}.MSEbp(:,alphaCluster,mseMerged{1,1}.freq> 8 & mseMerged{1,1}.freq< 12),3),2));

BetaRate_YA = squeeze(nanmean(max(EventsRhythm_Merged(1:47, betaCluster, freq > 14 & freq< 20,2),[],3),2));
BetaRate_OA = squeeze(nanmean(max(EventsRhythm_Merged(48:end, betaCluster, freq > 14 & freq< 20,2),[],3),2));

AlphaRate_YA = squeeze(nanmean(max(EventsRhythm_Merged(1:47, alphaCluster, freq > 8 & freq< 12,2),[],3),2));
AlphaRate_OA = squeeze(nanmean(max(EventsRhythm_Merged(48:end, alphaCluster, freq > 8 & freq< 12,2),[],3),2));

% figure;
% imagesc(corrcoef(squeeze(nanmedian(mseMerged{2,2}.MSEbp(:,betaCluster,:),2))),[-1
% 1]); set(gca, 'YTickLabels', round(mseMerged{1,2}.freq(get(gca,
% 'YTick')),2)); set(gca, 'XTickLabels', round(mseMerged{1,2}.freq(get(gca, 'XTick')),2));

% figure; imagesc(corrcoef(squeeze(nanmedian(mseMerged{1,2}.MSElp(:,betaCluster,:),2))),[-1 1])
% figure; imagesc(corrcoef(squeeze(nanmedian(mseMerged{1,2}.MSEhp(:,betaCluster,:),2))),[-1 1])
% 
% figure; imagesc(corrcoef(squeeze(nanmedian(mseMerged{1,2}.MSEbp(:,betaCluster,:),2))),[-1 1])
% figure; imagesc(corrcoef(squeeze(nanmedian(mseMerged{1,2}.MSEfiltskip(:,betaCluster,:),2))),[-1 1])
% figure; imagesc(corrcoef(squeeze(nanmedian(mseMerged{1,2}.MSEhp(:,betaCluster,:),2))),[-1 1])


figure
subplot(1,4,1); cla; hold on;
    x = AlphaRate_YA; y = AlphaMSE_YA; 
    l1 = scatter(x,y, 'filled', 'k'); y1_ls = polyval(polyfit(x,y,1),x); y1_ls = plot(x, y1_ls, 'Color', 'k');
    [r1, p1] = corrcoef(x, y); p1 = convertPtoExponential(p1(2));
    x = AlphaRate_OA; y = AlphaMSE_OA; 
    l3 = scatter(x,y, 'filled', 'r'); y3_ls = polyval(polyfit(x,y,1),x); y3_ls = plot(x, y3_ls, 'Color', 'r');
    [r3, p3] = corrcoef(x, y); p3 = convertPtoExponential(p3(2));
    legend([y1_ls, y3_ls], {['YA Alpha: r = ', num2str(round(r1(2),2)), ', p = ' p1{1}], ...
        ['OA Alpha: r = ', num2str(round(r3(2),2)), ', p = ' p3{1}]}); legend('boxoff');
    xlabel('Number of Alpha events'); ylabel('Entropy');
    %ylim([.2 .55])
    title('Parietooccipital Alpha')
subplot(1,4,2); cla; hold on;
    x = BetaRate_YA; y = BetaMSE_YA; 
    l2 = scatter(x,y, 'filled', 'k'); y2_ls = polyval(polyfit(x,y,1),x); y2_ls = plot(x, y2_ls, 'Color', 'k');
    [r2, p2] = corrcoef(x, y);  p2 = convertPtoExponential(p2(2));
    x = BetaRate_OA; y = BetaMSE_OA; 
    l4 = scatter(x,y, 'filled', 'r'); y4_ls = polyval(polyfit(x,y,1),x); y4_ls = plot(x, y4_ls, 'Color', 'r');
    [r4, p4] = corrcoef(x, y);  p4 = convertPtoExponential(p4(2));
    legend([y2_ls, y4_ls], {['YA Beta: r = ', num2str(round(r2(2),2)), ', p = ' p2{1}],...
        ['OA Beta: r = ', num2str(round(r4(2),2)), ', p = ' p4{1}]}); legend('boxoff');
    xlabel('Number of Beta events'); ylabel('Entropy');
    %ylim([.3 .5])
    title('Central Beta')
subplot(1,4,3); cla; hold on;
    x = AlphaMSE_YA; y = BetaMSE_YA; 
    l2 = scatter(x,y, 'filled', 'k'); y2_ls = polyval(polyfit(x,y,1),x); y2_ls = plot(x, y2_ls, 'Color', 'k');
    [r2, p2] = corrcoef(x, y);  p2 = convertPtoExponential(p2(2));
    x = AlphaMSE_OA; y = BetaMSE_OA; 
    l4 = scatter(x,y, 'filled', 'r'); y4_ls = polyval(polyfit(x,y,1),x); y4_ls = plot(x, y4_ls, 'Color', 'r');
    [r4, p4] = corrcoef(x, y);  p4 = convertPtoExponential(p4(2));
    legend([y2_ls, y4_ls], {['YA Beta: r = ', num2str(round(r2(2),2)), ', p = ' p2{1}],...
        ['OA Beta: r = ', num2str(round(r4(2),2)), ', p = ' p4{1}]}); legend('boxoff');
    xlabel('Alpha MSE'); ylabel('Beta Entropy');
    %ylim([.3 .5])
    title('Beta vs alpha MSE')
subplot(1,4,4); cla; hold on;
    x = AlphaRate_YA; y = BetaRate_YA; 
    l2 = scatter(x,y, 'filled', 'k'); y2_ls = polyval(polyfit(x,y,1),x); y2_ls = plot(x, y2_ls, 'Color', 'k');
    [r2, p2] = corrcoef(x, y);  p2 = convertPtoExponential(p2(2));
    x = AlphaRate_OA; y = BetaRate_OA; 
    l4 = scatter(x,y, 'filled', 'r'); y4_ls = polyval(polyfit(x,y,1),x); y4_ls = plot(x, y4_ls, 'Color', 'r');
    [r4, p4] = corrcoef(x, y);  p4 = convertPtoExponential(p4(2));
    legend([y2_ls, y4_ls], {['YA Beta: r = ', num2str(round(r2(2),2)), ', p = ' p2{1}],...
        ['OA Beta: r = ', num2str(round(r4(2),2)), ', p = ' p4{1}]}); legend('boxoff');
    xlabel('Alpha Rate'); ylabel('Beta Rate');
    %ylim([.3 .5])
    title('Beta vs alpha MSE')
set(findall(gcf,'-property','FontSize'),'FontSize',18)

% get partial correlations

[r,p] = partialcorr(AlphaRate_YA, AlphaMSE_YA, [BetaRate_YA, BetaMSE_YA])
[r,p] = partialcorr(AlphaRate_OA, AlphaMSE_OA, [BetaRate_OA, BetaMSE_OA])
[r,p] = partialcorr(BetaRate_YA, BetaMSE_YA, [AlphaRate_YA, AlphaMSE_YA])
[r,p] = partialcorr(BetaRate_OA, BetaMSE_OA, [AlphaRate_OA, AlphaMSE_OA])

for indFreq = 1:41
    BetaMSE_YA = squeeze(nanmean(nanmedian(mseMerged{1,2}.MSEbp(:,betaCluster,indFreq),3),2));
    BetaMSE_OA = squeeze(nanmean(nanmedian(mseMerged{2,2}.MSEbp(:,betaCluster,indFreq),3),2));

    AlphaMSE_YA = squeeze(nanmean(nanmedian(mseMerged{1,2}.MSEbp(:,alphaCluster,indFreq),3),2));
    AlphaMSE_OA = squeeze(nanmean(nanmedian(mseMerged{2,2}.MSEbp(:,alphaCluster,indFreq),3),2));

    BetaRate_YA = squeeze(nanmean(max(EventsRhythm_Merged(1:47, betaCluster, freq > 14 & freq< 20,2),[],3),2));
    BetaRate_OA = squeeze(nanmean(max(EventsRhythm_Merged(48:end, betaCluster, freq > 14 & freq< 20,2),[],3),2));

    AlphaRate_YA = squeeze(nanmean(max(EventsRhythm_Merged(1:47, alphaCluster, freq > 8 & freq< 12,2),[],3),2));
    AlphaRate_OA = squeeze(nanmean(max(EventsRhythm_Merged(48:end, alphaCluster, freq > 8 & freq< 12,2),[],3),2));

    x = AlphaMSE_YA; y = AlphaRate_YA; [r, p] = corrcoef(x, y); RMat(1,1,indFreq) = r(2);
    x = AlphaMSE_OA; y = AlphaRate_OA; [r, p] = corrcoef(x, y); RMat(2,1,indFreq) = r(2);
    x = BetaMSE_YA; y = BetaRate_YA; [r, p] = corrcoef(x, y); RMat(1,2,indFreq) = r(2);
    x = BetaMSE_OA; y = BetaRate_OA; [r, p] = corrcoef(x, y); RMat(2,2,indFreq) = r(2);
end

figure; 
subplot(1,2,1); hold on; plot(squeeze(RMat(1,1,:))); plot(squeeze(RMat(2,1,:))); xlim([1,41]); title('AlphaRate')
set(gca, 'XTickLabels', round(mseMerged{1,2}.freq(get(gca,'XTick')),2));
subplot(1,2,2); hold on; plot(squeeze(RMat(1,2,:))); plot(squeeze(RMat(2,2,:))); xlim([1,41]); title('BetaRate')
set(gca, 'XTickLabels', round(mseMerged{1,2}.freq(get(gca,'XTick')),2));


%% low-pass settings

BetaMSE_YA = squeeze(nanmean(nanmedian(mseMerged{1,2}.MSEfiltskip(:,betaCluster,mseMerged{1,1}.freq> 14 & mseMerged{1,1}.freq< 20),3),2));
BetaMSE_OA = squeeze(nanmean(nanmedian(mseMerged{2,2}.MSEfiltskip(:,betaCluster,mseMerged{1,1}.freq> 14 & mseMerged{1,1}.freq< 20),3),2));

AlphaMSE_YA = squeeze(nanmean(nanmedian(mseMerged{1,2}.MSEfiltskip(:,alphaCluster,mseMerged{1,1}.freq> 8 & mseMerged{1,1}.freq< 12),3),2));
AlphaMSE_OA = squeeze(nanmean(nanmedian(mseMerged{2,2}.MSEfiltskip(:,alphaCluster,mseMerged{1,1}.freq> 8 & mseMerged{1,1}.freq< 12),3),2));

BetaRate_YA = squeeze(nanmean(max(EventsRhythm_Merged(1:47, betaCluster, freq > 14 & freq< 20,2),[],3),2));
BetaRate_OA = squeeze(nanmean(max(EventsRhythm_Merged(48:end, betaCluster, freq > 14 & freq< 20,2),[],3),2));

AlphaRate_YA = squeeze(nanmean(max(EventsRhythm_Merged(1:47, alphaCluster, freq > 8 & freq< 12,2),[],3),2));
AlphaRate_OA = squeeze(nanmean(max(EventsRhythm_Merged(48:end, alphaCluster, freq > 8 & freq< 12,2),[],3),2));

figure
subplot(1,4,1); cla; hold on;
    x = AlphaRate_YA; y = AlphaMSE_YA; 
    l1 = scatter(x,y, 'filled', 'k'); y1_ls = polyval(polyfit(x,y,1),x); y1_ls = plot(x, y1_ls, 'Color', 'k');
    [r1, p1] = corrcoef(x, y); p1 = convertPtoExponential(p1(2));
    x = AlphaRate_OA; y = AlphaMSE_OA; 
    l3 = scatter(x,y, 'filled', 'r'); y3_ls = polyval(polyfit(x,y,1),x); y3_ls = plot(x, y3_ls, 'Color', 'r');
    [r3, p3] = corrcoef(x, y); p3 = convertPtoExponential(p3(2));
    legend([y1_ls, y3_ls], {['YA Alpha: r = ', num2str(round(r1(2),2)), ', p = ' p1{1}], ...
        ['OA Alpha: r = ', num2str(round(r3(2),2)), ', p = ' p3{1}]}); legend('boxoff');
    xlabel('Number of Alpha events'); ylabel('Entropy');
    %ylim([.2 .55])
    title('Parietooccipital Alpha')
subplot(1,4,2); cla; hold on;
    x = BetaRate_YA; y = BetaMSE_YA; 
    l2 = scatter(x,y, 'filled', 'k'); y2_ls = polyval(polyfit(x,y,1),x); y2_ls = plot(x, y2_ls, 'Color', 'k');
    [r2, p2] = corrcoef(x, y);  p2 = convertPtoExponential(p2(2));
    x = BetaRate_OA; y = BetaMSE_OA; 
    l4 = scatter(x,y, 'filled', 'r'); y4_ls = polyval(polyfit(x,y,1),x); y4_ls = plot(x, y4_ls, 'Color', 'r');
    [r4, p4] = corrcoef(x, y);  p4 = convertPtoExponential(p4(2));
    legend([y2_ls, y4_ls], {['YA Beta: r = ', num2str(round(r2(2),2)), ', p = ' p2{1}],...
        ['OA Beta: r = ', num2str(round(r4(2),2)), ', p = ' p4{1}]}); legend('boxoff');
    xlabel('Number of Beta events'); ylabel('Entropy');
    %ylim([.3 .5])
    title('Central Beta')
subplot(1,4,3); cla; hold on;
    x = AlphaMSE_YA; y = BetaMSE_YA; 
    l2 = scatter(x,y, 'filled', 'k'); y2_ls = polyval(polyfit(x,y,1),x); y2_ls = plot(x, y2_ls, 'Color', 'k');
    [r2, p2] = corrcoef(x, y);  p2 = convertPtoExponential(p2(2));
    x = AlphaMSE_OA; y = BetaMSE_OA; 
    l4 = scatter(x,y, 'filled', 'r'); y4_ls = polyval(polyfit(x,y,1),x); y4_ls = plot(x, y4_ls, 'Color', 'r');
    [r4, p4] = corrcoef(x, y);  p4 = convertPtoExponential(p4(2));
    legend([y2_ls, y4_ls], {['YA Beta: r = ', num2str(round(r2(2),2)), ', p = ' p2{1}],...
        ['OA Beta: r = ', num2str(round(r4(2),2)), ', p = ' p4{1}]}); legend('boxoff');
    xlabel('Alpha MSE'); ylabel('Beta Entropy');
    %ylim([.3 .5])
    title('Beta vs alpha MSE')
subplot(1,4,4); cla; hold on;
    x = AlphaRate_YA; y = BetaRate_YA; 
    l2 = scatter(x,y, 'filled', 'k'); y2_ls = polyval(polyfit(x,y,1),x); y2_ls = plot(x, y2_ls, 'Color', 'k');
    [r2, p2] = corrcoef(x, y);  p2 = convertPtoExponential(p2(2));
    x = AlphaRate_OA; y = BetaRate_OA; 
    l4 = scatter(x,y, 'filled', 'r'); y4_ls = polyval(polyfit(x,y,1),x); y4_ls = plot(x, y4_ls, 'Color', 'r');
    [r4, p4] = corrcoef(x, y);  p4 = convertPtoExponential(p4(2));
    legend([y2_ls, y4_ls], {['YA Beta: r = ', num2str(round(r2(2),2)), ', p = ' p2{1}],...
        ['OA Beta: r = ', num2str(round(r4(2),2)), ', p = ' p4{1}]}); legend('boxoff');
    xlabel('Alpha Rate'); ylabel('Beta Rate');
    %ylim([.3 .5])
    title('Beta vs alpha MSE')
set(findall(gcf,'-property','FontSize'),'FontSize',18)

for indFreq = 1:41
    BetaMSE_YA = squeeze(nanmean(nanmedian(mseMerged{1,2}.MSEfiltskip(:,betaCluster,indFreq),3),2));
    BetaMSE_OA = squeeze(nanmean(nanmedian(mseMerged{2,2}.MSEfiltskip(:,betaCluster,indFreq),3),2));

    AlphaMSE_YA = squeeze(nanmean(nanmedian(mseMerged{1,2}.MSEfiltskip(:,alphaCluster,indFreq),3),2));
    AlphaMSE_OA = squeeze(nanmean(nanmedian(mseMerged{2,2}.MSEfiltskip(:,alphaCluster,indFreq),3),2));

    BetaRate_YA = squeeze(nanmean(max(EventsRhythm_Merged(1:47, betaCluster, freq > 14 & freq< 20,2),[],3),2));
    BetaRate_OA = squeeze(nanmean(max(EventsRhythm_Merged(48:end, betaCluster, freq > 14 & freq< 20,2),[],3),2));

    AlphaRate_YA = squeeze(nanmean(max(EventsRhythm_Merged(1:47, alphaCluster, freq > 8 & freq< 12,2),[],3),2));
    AlphaRate_OA = squeeze(nanmean(max(EventsRhythm_Merged(48:end, alphaCluster, freq > 8 & freq< 12,2),[],3),2));

    x = AlphaMSE_YA; y = AlphaRate_YA; [r, p] = corrcoef(x, y); RMat(1,1,indFreq) = r(2);
    x = AlphaMSE_OA; y = AlphaRate_OA; [r, p] = corrcoef(x, y); RMat(2,1,indFreq) = r(2);
    x = BetaMSE_YA; y = BetaRate_YA; [r, p] = corrcoef(x, y); RMat(1,2,indFreq) = r(2);
    x = BetaMSE_OA; y = BetaRate_OA; [r, p] = corrcoef(x, y); RMat(2,2,indFreq) = r(2);
end

figure; 
subplot(1,2,1); hold on; plot(squeeze(RMat(1,1,:))); plot(squeeze(RMat(2,1,:))); xlim([1,41]); title('AlphaRate')
set(gca, 'XTickLabels', round(mseMerged{1,2}.freq(get(gca,'XTick')),2));
subplot(1,2,2); hold on; plot(squeeze(RMat(1,2,:))); plot(squeeze(RMat(2,2,:))); xlim([1,41]); title('BetaRate')
set(gca, 'XTickLabels', round(mseMerged{1,2}.freq(get(gca,'XTick')),2));
    

%% high-pass settings

BetaMSE_YA = squeeze(nanmean(nanmedian(mseMerged{1,2}.MSEhp(:,betaCluster,mseMerged{1,1}.freq> 14 & mseMerged{1,1}.freq< 20),3),2));
BetaMSE_OA = squeeze(nanmean(nanmedian(mseMerged{2,2}.MSEhp(:,betaCluster,mseMerged{1,1}.freq> 14 & mseMerged{1,1}.freq< 20),3),2));

AlphaMSE_YA = squeeze(nanmean(nanmedian(mseMerged{1,2}.MSEhp(:,alphaCluster,mseMerged{1,1}.freq> 8 & mseMerged{1,1}.freq< 12),3),2));
AlphaMSE_OA = squeeze(nanmean(nanmedian(mseMerged{2,2}.MSEhp(:,alphaCluster,mseMerged{1,1}.freq> 8 & mseMerged{1,1}.freq< 12),3),2));

BetaRate_YA = squeeze(nanmean(max(EventsRhythm_Merged(1:47, betaCluster, freq > 14 & freq< 20,2),[],3),2));
BetaRate_OA = squeeze(nanmean(max(EventsRhythm_Merged(48:end, betaCluster, freq > 14 & freq< 20,2),[],3),2));

AlphaRate_YA = squeeze(nanmean(max(EventsRhythm_Merged(1:47, alphaCluster, freq > 8 & freq< 12,2),[],3),2));
AlphaRate_OA = squeeze(nanmean(max(EventsRhythm_Merged(48:end, alphaCluster, freq > 8 & freq< 12,2),[],3),2));

figure
subplot(1,4,1); cla; hold on;
    x = AlphaRate_YA; y = AlphaMSE_YA; 
    l1 = scatter(x,y, 'filled', 'k'); y1_ls = polyval(polyfit(x,y,1),x); y1_ls = plot(x, y1_ls, 'Color', 'k');
    [r1, p1] = corrcoef(x, y); p1 = convertPtoExponential(p1(2));
    x = AlphaRate_OA; y = AlphaMSE_OA; 
    l3 = scatter(x,y, 'filled', 'r'); y3_ls = polyval(polyfit(x,y,1),x); y3_ls = plot(x, y3_ls, 'Color', 'r');
    [r3, p3] = corrcoef(x, y); p3 = convertPtoExponential(p3(2));
    legend([y1_ls, y3_ls], {['YA Alpha: r = ', num2str(round(r1(2),2)), ', p = ' p1{1}], ...
        ['OA Alpha: r = ', num2str(round(r3(2),2)), ', p = ' p3{1}]}); legend('boxoff');
    xlabel('Number of Alpha events'); ylabel('Entropy');
    %ylim([.2 .55])
    title('Parietooccipital Alpha')
subplot(1,4,2); cla; hold on;
    x = BetaRate_YA; y = BetaMSE_YA; 
    l2 = scatter(x,y, 'filled', 'k'); y2_ls = polyval(polyfit(x,y,1),x); y2_ls = plot(x, y2_ls, 'Color', 'k');
    [r2, p2] = corrcoef(x, y);  p2 = convertPtoExponential(p2(2));
    x = BetaRate_OA; y = BetaMSE_OA; 
    l4 = scatter(x,y, 'filled', 'r'); y4_ls = polyval(polyfit(x,y,1),x); y4_ls = plot(x, y4_ls, 'Color', 'r');
    [r4, p4] = corrcoef(x, y);  p4 = convertPtoExponential(p4(2));
    legend([y2_ls, y4_ls], {['YA Beta: r = ', num2str(round(r2(2),2)), ', p = ' p2{1}],...
        ['OA Beta: r = ', num2str(round(r4(2),2)), ', p = ' p4{1}]}); legend('boxoff');
    xlabel('Number of Beta events'); ylabel('Entropy');
    %ylim([.3 .5])
    title('Central Beta')
subplot(1,4,3); cla; hold on;
    x = AlphaMSE_YA; y = BetaMSE_YA; 
    l2 = scatter(x,y, 'filled', 'k'); y2_ls = polyval(polyfit(x,y,1),x); y2_ls = plot(x, y2_ls, 'Color', 'k');
    [r2, p2] = corrcoef(x, y);  p2 = convertPtoExponential(p2(2));
    x = AlphaMSE_OA; y = BetaMSE_OA; 
    l4 = scatter(x,y, 'filled', 'r'); y4_ls = polyval(polyfit(x,y,1),x); y4_ls = plot(x, y4_ls, 'Color', 'r');
    [r4, p4] = corrcoef(x, y);  p4 = convertPtoExponential(p4(2));
    legend([y2_ls, y4_ls], {['YA Beta: r = ', num2str(round(r2(2),2)), ', p = ' p2{1}],...
        ['OA Beta: r = ', num2str(round(r4(2),2)), ', p = ' p4{1}]}); legend('boxoff');
    xlabel('Alpha MSE'); ylabel('Beta Entropy');
    %ylim([.3 .5])
    title('Beta vs alpha MSE')
subplot(1,4,4); cla; hold on;
    x = AlphaRate_YA; y = BetaRate_YA; 
    l2 = scatter(x,y, 'filled', 'k'); y2_ls = polyval(polyfit(x,y,1),x); y2_ls = plot(x, y2_ls, 'Color', 'k');
    [r2, p2] = corrcoef(x, y);  p2 = convertPtoExponential(p2(2));
    x = AlphaRate_OA; y = BetaRate_OA; 
    l4 = scatter(x,y, 'filled', 'r'); y4_ls = polyval(polyfit(x,y,1),x); y4_ls = plot(x, y4_ls, 'Color', 'r');
    [r4, p4] = corrcoef(x, y);  p4 = convertPtoExponential(p4(2));
    legend([y2_ls, y4_ls], {['YA Beta: r = ', num2str(round(r2(2),2)), ', p = ' p2{1}],...
        ['OA Beta: r = ', num2str(round(r4(2),2)), ', p = ' p4{1}]}); legend('boxoff');
    xlabel('Alpha Rate'); ylabel('Beta Rate');
    %ylim([.3 .5])
    title('Beta vs alpha MSE')

set(findall(gcf,'-property','FontSize'),'FontSize',18)

for indFreq = 1:41
    BetaMSE_YA = squeeze(nanmean(nanmedian(mseMerged{1,2}.MSEhp(:,betaCluster,indFreq),3),2));
    BetaMSE_OA = squeeze(nanmean(nanmedian(mseMerged{2,2}.MSEhp(:,betaCluster,indFreq),3),2));

    AlphaMSE_YA = squeeze(nanmean(nanmedian(mseMerged{1,2}.MSEhp(:,alphaCluster,indFreq),3),2));
    AlphaMSE_OA = squeeze(nanmean(nanmedian(mseMerged{2,2}.MSEhp(:,alphaCluster,indFreq),3),2));

    BetaRate_YA = squeeze(nanmean(max(EventsRhythm_Merged(1:47, betaCluster, freq > 14 & freq< 20,2),[],3),2));
    BetaRate_OA = squeeze(nanmean(max(EventsRhythm_Merged(48:end, betaCluster, freq > 14 & freq< 20,2),[],3),2));

    AlphaRate_YA = squeeze(nanmean(max(EventsRhythm_Merged(1:47, alphaCluster, freq > 8 & freq< 12,2),[],3),2));
    AlphaRate_OA = squeeze(nanmean(max(EventsRhythm_Merged(48:end, alphaCluster, freq > 8 & freq< 12,2),[],3),2));

    x = AlphaMSE_YA; y = AlphaRate_YA; [r, p] = corrcoef(x, y); RMat(1,1,indFreq) = r(2);
    x = AlphaMSE_OA; y = AlphaRate_OA; [r, p] = corrcoef(x, y); RMat(2,1,indFreq) = r(2);
    x = BetaMSE_YA; y = BetaRate_YA; [r, p] = corrcoef(x, y); RMat(1,2,indFreq) = r(2);
    x = BetaMSE_OA; y = BetaRate_OA; [r, p] = corrcoef(x, y); RMat(2,2,indFreq) = r(2);
end

figure; 
subplot(1,2,1); hold on; plot(squeeze(RMat(1,1,:))); plot(squeeze(RMat(2,1,:))); xlim([1,42]); title('AlphaRate')
set(gca, 'XTickLabels', round(mseMerged{1,2}.freq(get(gca,'XTick')),2));
subplot(1,2,2); hold on; plot(squeeze(RMat(1,2,:))); plot(squeeze(RMat(2,2,:))); xlim([1,42]); title('BetaRate')
set(gca, 'XTickLabels', round(mseMerged{1,2}.freq(get(gca,'XTick')),2));