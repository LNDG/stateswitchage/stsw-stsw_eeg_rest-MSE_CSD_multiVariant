% plot results of MSE CBPA

pn.root = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/A_MSE_CSD_multiVariant/';

addpath([pn.root, 'T_tools/fieldtrip-20170904']); ft_defaults;

load([pn.root, 'B_data/E2_CBPA_Age_Condition_v3.mat'], 'stat', 'methodLabels')

contrastLabels = {'YA: EC vs. EO'; 'OA: EC vs. EO'; 'EC: OA vs. YA'; 'EO: OA vs. YA'};

% 1 - YA: EC vs. EO
% 2 - OA: EC vs. EO
% 3 - EC: YA vs. OA
% 4 - EO: YA vs. OA

% X, 1... - method in methodLabels

h = figure('units','normalized','position',[0 0 1 1]);
for indContrast = 1:4
    for indMethod = 1:10
        subplot(10,4,(indMethod-1)*4+indContrast);
        imagesc(stat{indContrast,indMethod}.stat, 'AlphaData', .5); 
        caxis([-6 6]) 
        hold on;
        imagesc(stat{indContrast,indMethod}.stat, 'AlphaData', stat{indContrast,indMethod}.mask); 
        set(gca, 'XTick', 1:4:41); set(gca, 'XTickLabel', round(stat{indContrast,indMethod}.freq(1:4:41))); xlabel('Scale [Hz]'); ylabel('Channel');
        title([contrastLabels{indContrast}, ';', methodLabels{indMethod}])
    end
end
addpath('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/A_MSE_CSD_multiVariant/T_tools/brewermap')
cBrew = brewermap(500,'RdBu');
cBrew = flipud(cBrew);
colormap(cBrew)
pn.plotFolder = [pn.root, 'C_figures/'];
figureName = 'E3_statsWithMasking';
% saveas(h, [pn.plotFolder, figureName], 'fig');
% saveas(h, [pn.plotFolder, figureName], 'epsc');
saveas(h, [pn.plotFolder, figureName], 'png');

%% plot only EO: OA vs. YA

indContrast = 4;
labels = {'MSE: Vanilla'; 'MSE: Vanilla + scale-wise R'; 'MSE: Lowpass + scale-wise R'; 'MSE: Highpass + scale-wise R'; 'MSE: Bandpass + scale-wise R';...
    'r: Vanilla'; 'r: Vanilla + scale-wise R'; 'r: Lowpass + scale-wise R'; 'r: Highpass + scale-wise R'; 'r: Bandpass + scale-wise R'};

addpath('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/A_MSE_CSD_multiVariant/T_tools/altmany-export_fig-4282d74')

h = figure('units','normalized','position',[0 0 1 .4]);
for indMethod = 1:10
    subplot(2,5,indMethod);
    imagesc(stat{indContrast,indMethod}.stat, 'AlphaData', .5); 
    caxis([-6 6]) 
    hold on;
    imagesc(stat{indContrast,indMethod}.stat, 'AlphaData', stat{indContrast,indMethod}.mask); 
    set(gca, 'XTick', [1:7:42, 42]);
    set(gca, 'XTickLabels', round(stat{1,1}.freq(get(gca, 'XTick')),0));
    set(gca, 'XDir', 'reverse')
    xlabel('Frequency (Hz)'); ylabel('Channel');
    cb = colorbar; set(get(cb,'label'),'string','t values');
    title(labels(indMethod))
end
set(findall(gcf,'-property','FontSize'),'FontSize',15)
addpath('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/A_MSE_CSD_multiVariant/T_tools/brewermap')
cBrew = brewermap(500,'RdBu');
cBrew = flipud(cBrew);
colormap(cBrew)
pn.plotFolder = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/A_MSE_CSD_multiVariant/C_figures/';
figureName = 'E3_CBPA_EO_OAvsYA';
export_fig([pn.plotFolder, figureName, '.pdf'], '-transparent')
export_fig([pn.plotFolder, figureName, '.eps'], '-transparent')

saveas(h, [pn.plotFolder, figureName], 'fig');
saveas(h, [pn.plotFolder, figureName], 'png');

%% topography

pn.root         = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/';
pn.tools        = [pn.root, 'B_analyses/S2_TFR/T_tools/']; addpath(pn.tools);
addpath([pn.tools, 'fieldtrip-20170904/']); ft_defaults;

cfg = [];
cfg.layout = 'acticap-64ch-standard2.mat';
cfg.parameter = 'powspctrm';
cfg.comment = 'no';
cfg.colorbar = 'SouthOutside';
cfg.zlim = [-5 5];

h = figure('units','normalized','position',[.1 .1 .7 .4]);
subplot(1,2,1)
    cfg.highlight = 'on';
    cfg.highlightchannel = stat{1,1}.label(max(stat{4,1}.mask(:,1:5),[],2));
    plotData = [];
    plotData.label = stat{1,1}.label; % {1 x N}
    plotData.dimord = 'chan';
    plotData.powspctrm = squeeze(nanmean(stat{4,1}.stat(:,1:5),2));
    ft_topoplotER(cfg,plotData);
    title('Low-frequency YA>OA vanilla entropy')
subplot(1,2,2)
    cfg.highlight = 'on';
    cfg.highlightchannel = stat{1,1}.label(max(stat{4,1}.mask(:,41),[],2));
    plotData = [];
    plotData.label = stat{1,1}.label; % {1 x N}
    plotData.dimord = 'chan';
    plotData.powspctrm = squeeze(nanmean(stat{4,1}.stat(:,41),2));
    ft_topoplotER(cfg,plotData);
    title('High-frequency YA>OA vanilla entropy')
set(findall(gcf,'-property','FontSize'),'FontSize',18)
pn.plotFolder = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/A_MSE_CSD_multiVariant/C_figures/';
figureName = 'E3_vanillaAgeDifferences';
saveas(h, [pn.plotFolder, figureName], 'fig');
saveas(h, [pn.plotFolder, figureName], 'epsc');
saveas(h, [pn.plotFolder, figureName], 'png');

    
figure;
subplot(1,2,1)
    cfg.highlight = 'on';
    cfg.highlightchannel = stat{1,1}.label(max(stat{4,3}.mask(:,1:5),[],2));
    plotData = [];
    plotData.label = stat{1,1}.label; % {1 x N}
    plotData.dimord = 'chan';
    plotData.powspctrm = squeeze(nanmean(stat{4,3}.stat(:,1:5),2));
    ft_topoplotER(cfg,plotData);
    title('Low-frequency age difference low-pass entropy')
subplot(1,2,2)
    cfg.highlight = 'on';
    cfg.highlightchannel = stat{1,1}.label(max(stat{4,3}.mask(:,41),[],2));
    plotData = [];
    plotData.label = stat{1,1}.label; % {1 x N}
    plotData.dimord = 'chan';
    plotData.powspctrm = squeeze(nanmean(stat{4,3}.stat(:,41),2));
    ft_topoplotER(cfg,plotData);
    title('High-frequency age difference low-pass entropy')

figure;
subplot(1,2,1)
    cfg.highlight = 'on';
    cfg.highlightchannel = stat{1,1}.label(max(stat{4,4}.mask(:,1:5),[],2));
    plotData = [];
    plotData.label = stat{1,1}.label; % {1 x N}
    plotData.dimord = 'chan';
    plotData.powspctrm = squeeze(nanmean(stat{4,4}.stat(:,1:5),2));
    ft_topoplotER(cfg,plotData);
    title('Low-frequency age difference high-pass entropy')
subplot(1,2,2)
    cfg.highlight = 'on';
    cfg.highlightchannel = stat{1,1}.label(max(stat{4,4}.mask(:,41),[],2));
    plotData = [];
    plotData.label = stat{1,1}.label; % {1 x N}
    plotData.dimord = 'chan';
    plotData.powspctrm = squeeze(nanmean(stat{4,4}.stat(:,41),2));
    ft_topoplotER(cfg,plotData);
    title('High-frequency age difference high-pass entropy')
    
    
figure;
plotData = [];
plotData.label = stat{1,1}.label; % {1 x N}
plotData.dimord = 'chan';
plotData.powspctrm = squeeze(nanmean(stat{4,4}.stat(:,5),2));
ft_topoplotER(cfg,plotData);

figure;
plotData = [];
plotData.label = stat{1,1}.label; % {1 x N}
plotData.dimord = 'chan';
plotData.powspctrm = squeeze(nanmean(stat{4,3}.stat(:,5),2));
ft_topoplotER(cfg,plotData);



figure;
plotData = [];
plotData.label = stat{1,1}.label; % {1 x N}
plotData.dimord = 'chan';
plotData.powspctrm = squeeze(nanmean(stat{4,1}.stat(:,41),2));
ft_topoplotER(cfg,plotData);
title('High-frequency age difference vanilla entropy')


%% topography of bandpass MSE differences

addpath('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/A_MSE_CSD_multiVariant/T_tools/brewermap')

cBrew = brewermap(500,'RdBu');
cBrew = flipud(cBrew);

cfg = [];
cfg.layout = 'acticap-64ch-standard2.mat';
cfg.parameter = 'powspctrm';
cfg.comment = 'no';
cfg.colorbar = 'SouthOutside';
cfg.style = 'both';
cfg.colormap = cBrew;
cfg.zlim = [-5 5];

h = figure('units','normalized','position',[.1 .1 .7 .7]);
subplot(2,2,1)
    cfg.highlight = 'on';
    cfg.highlightchannel = stat{1,1}.label(max(stat{4,5}.mask(:,15:20),[],2));
    plotData = [];
    plotData.label = stat{1,1}.label; % {1 x N}
    plotData.dimord = 'chan';
    plotData.powspctrm = squeeze(nanmedian(stat{4,5}.stat(:,15:20),2));
    ft_topoplotER(cfg,plotData);
    title('Alpha entropy OA vs. YA')
subplot(2,2,2)
    cfg.highlight = 'on';
    cfg.highlightchannel = stat{1,1}.label(max(stat{4,5}.mask(:,23:27),[],2));
    plotData = [];
    plotData.label = stat{1,1}.label; % {1 x N}
    plotData.dimord = 'chan';
    plotData.powspctrm = squeeze(nanmedian(stat{4,5}.stat(:,23:27),2));
    ft_topoplotER(cfg,plotData);
    title('Beta entropy OA vs. YA')
subplot(2,2,3)
    cfg.highlight = 'on';
    cfg.highlightchannel = stat{1,1}.label(max(stat{4,10}.mask(:,15:20),[],2));
    plotData = [];
    plotData.label = stat{1,1}.label; % {1 x N}
    plotData.dimord = 'chan';
    plotData.powspctrm = squeeze(nanmedian(stat{4,10}.stat(:,15:20),2));
    ft_topoplotER(cfg,plotData);
    title('Alpha similarity criterion OA vs. YA')
subplot(2,2,4)
    cfg.highlight = 'on';
    cfg.highlightchannel = stat{1,1}.label(max(stat{4,10}.mask(:,23:27),[],2));
    plotData = [];
    plotData.label = stat{1,1}.label; % {1 x N}
    plotData.dimord = 'chan';
    plotData.powspctrm = squeeze(nanmedian(stat{4,10}.stat(:,23:27),2));
    ft_topoplotER(cfg,plotData);
    title('Beta similarity criterion OA vs. YA')
set(findall(gcf,'-property','FontSize'),'FontSize',18)
pn.plotFolder = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/A_MSE_CSD_multiVariant/C_figures/';
figureName = 'E4_bandpassMSEdifferences';

saveas(h, [pn.plotFolder, figureName], 'fig');
saveas(h, [pn.plotFolder, figureName], 'epsc');
saveas(h, [pn.plotFolder, figureName], 'png');
