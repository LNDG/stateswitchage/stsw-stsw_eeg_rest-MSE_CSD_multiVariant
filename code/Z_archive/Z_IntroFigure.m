%% Create a summary figure of the age-dependent spectra and their reflection in 'Original' MSE scales

load('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/A_MSE_CSD_multiVariant/B_data/E_mseMerged.mat', 'mseMerged', 'IDs')

% add convertPtoExponential
addpath('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/A_MSE_CSD_multiVariant/T_tools/')

pn.shadedError = ['/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/A_MSE_CSD_multiVariant/T_tools/shadedErrorBar']; addpath(pn.shadedError);

indCond = 2; channels = 1:60;

%% Plot PSD spectra including slope fits

load('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/B_FFT/B_data/B_FFT_grandavg_individual.mat')

% figure; 
% subplot(1,2,1); imagesc(squeeze(nanmean(grandavg_EC.powspctrm,2))); title('Eyes Closed');
% subplot(1,2,2); imagesc(squeeze(nanmean(grandavg_EO.powspctrm,2))); title('Eyes Open');

commonYA = info.IDs_all(ismember(info.IDs_all, IDs{1}));
commonOA = info.IDs_all(ismember(info.IDs_all, IDs{2}));

idx_YA_fft = ismember(info.IDs_all, commonYA);
idx_OA_fft = ismember(info.IDs_all, commonOA);
idx_YA_mse = ismember(IDs{1}, commonYA);
idx_OA_mse = ismember(IDs{2}, commonOA);

pn.shadedError = ['/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/A_MSE_CSD_multiVariant/T_tools/shadedErrorBar']; addpath(pn.shadedError);

curAverage = nanmean(log10(grandavg_EO.powspctrm(idx_YA_fft,:,:)),2);

h = figure('units','normalized','position',[.1 .1 .25 .15]);
subplot(1,2,1); cla; hold on;
    % plot area under spectrum
    x = log10(grandavg_EO.freq);
    y1=squeeze(nanmean(curAverage,1));
    H1=area(x,y1,-9.5, 'FaceColor',[175/255, 36/255, 24/255]);
    hold on
    idx=grandavg_EO.freq<= 8;
    H=area(x(idx),y1(idx),-9.5, 'FaceColor',[48/255, 111/255, 186/255]);
    H.FaceAlpha = 0.5;
    H.EdgeAlpha = 0;
    H1.EdgeAlpha = 1;
    H1.EdgeColor = [175/255, 36/255, 24/255];
    % plot actual spectrum

    l1 = plot(log10(grandavg_EO.freq),squeeze(nanmean(curAverage,1)), 'Color', 'k', 'LineWidth', 5);
    %set(gca, 'XScale', 'log')
    xlim([.3, 1.8])
    set(gca, 'XTick',log10(grandavg_EC.freq(3:8:end)));    
    set(gca, 'XTickLabels',round(grandavg_EC.freq(3:8:end)));
    %set(gca, 'XTickLabels',[]);
    xlabel('Frequency [Hz, log-scaled]'); ylabel({'Power ~ variance';'[log-scaled]'})
    set(gca, 'YTick', []);
    set(gca, 'XAxisLocation', 'bottom')
    set(gca, 'YAxisLocation', 'left')
    set(gca, 'XDir','reverse')
    %set(gca,'XColor','none');
    %set(gca,'YColor','none');
    
%% Plot cumulative power spectrum

subplot(1,2,2); cla; hold on;
    x = squeeze(nanmean(nanmean(mseMerged{1,indCond}.Rvanilla(:,1:60,:),2),1)); l1 = plot(x, 'Color', 'k', 'LineStyle', ':', 'LineWidth', 5);
    x = squeeze(nanmean(nanmean(mseMerged{1,indCond}.Rpointavg(:,1:60,:),2),1)); l2 = plot(x, 'Color', 'k', 'LineStyle', '-', 'LineWidth', 5);
    ylabel({'Cumulative variance'; '(~ similarity bounds)'}); xlabel('Time scale [Hz/scale-factor; log-scaled]')
    set(gca, 'XScale', 'log')
    scales = 1:41;
    set(gca, 'XTick',round(logspace(log10(1), log10(30), 4)));
    tmp_meas = [round(mseMerged{1,indCond}.freq(get(gca, 'XTick')),0); round(scales(get(gca, 'XTick')),0)];
    xLabels = [];
    for indScale = 1:size(tmp_meas,2)
        xLabels{indScale} = [num2str(tmp_meas(1,indScale)), '/',num2str(tmp_meas(2,indScale))];
    end; clear tmp_meas;
    set(gca, 'XTickLabels', xLabels);
    set(gca, 'YAxisLocation', 'right')
    xlim([1 41]); ylim([5.5 8.5]*10^-4);
    set(gca, 'YTick', []);
    set(findall(gcf,'-property','FontSize'),'FontSize',15);
    
%% save Figure
    
pn.plotFolder = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/A_MSE_CSD_multiVariant/C_figures/';
figureName = 'Z_IntroSchematic';

saveas(h, [pn.plotFolder, figureName], 'fig');
saveas(h, [pn.plotFolder, figureName], 'epsc');
saveas(h, [pn.plotFolder, figureName], 'png');