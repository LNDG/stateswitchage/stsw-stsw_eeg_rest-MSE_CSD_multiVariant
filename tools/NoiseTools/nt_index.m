function [status,p]=nt_index(name,p,forceUpdate);
%[status,p]=nt_index(name,p,forceUpdate) - index data files & directories
%
%  status: 1: needed indexing, 0: didn't, -1: failed
%  p: parameter structure
%
%  name: name(s) of file(s) or directory to index
%  p: parameters
%  forceUpdate: if true force indexing [default: false]
%
% NoiseTools
nt_greetings;

if nargin<3 || isempty(forceUpdate); forceUpdate=0; end 
if nargin<2||isempty(p); % set default parameters
    p=[];
    p.scale=100;
    if nargin >= 1; p.name=name; end    
end
if nargin<1 || isempty(name);
    p.name=[];
    status=-1;
    return;  % just return default parameters
end

status=-1; % failed by default
updateFlag=0;   % don't update unless necessary
if forceUpdate; updateFlag=1; end

% check 'name'
if ~ischar(name); error('name should be a string'); end
avoid=['[',1:31, 127,']'];
if regexp(name,avoid); 
    disp('bad character in file name, skip:'); disp(['   >',name,'<']); 
    return; 
end
if name=='.'; name=pwd; end
if name(end)=='/'; name=name(1:end-1); end % remove trailing slash
[PATHSTR,NAME,EXT]=fileparts(name);
if strcmp(EXT,'idx'); 
    disp(['warning: ', name, ' might be index file']); 
end
if isempty(PATHSTR); % interpret relative to current directory
    name=[pwd,filesep,name]; % full path, safe to use 'exist'
end
[PATHSTR,NAME,EXT]=fileparts(name); 
if 2==exist(name) 
    d=dir(name);
    filename=d.name;            % match case to file system
    PATHSTR=cd(cd(PATHSTR));    % match case to file system
    name=[PATHSTR,filesep,filename];
elseif 7==exist(name)
    name=cd(cd(name));          % match case to file system
    [PATHSTR,NAME,EXT]=fileparts(name); 
else
    disp(name);
    error('...is neither file nor directory');
end


hhh=[]; % structure with info about this file or directory
iii=[]; % structure with data indices
hhh.name=name;
hhh.time_indexed=now;

% file or directory?
if 2==exist(name) 
    hhh.isdir=0;
elseif 7==exist(name)
    hhh.isdir=1;
else
    disp(name);
    error('...is neither file nor directory');
end

% intercept CTF data (directory name ends with '.ds')
if numel(name>=3) && strcmp(name(end-2:end), '.ds');
    hhh.isdir=0;
end
        
hhh.failed=0;
hhh.isdata=0;

% index directory 
idxDir=[PATHSTR,filesep,'nt_idx'];
if 7 ~= exist(idxDir); 
    disp(['creating index directory ', idxDir]);
    mkdir (idxDir);
    updateFlag=1;
end

% index file
idxName=[idxDir,filesep,NAME,EXT,'.idx'];
hhh.idxName=idxName;
if ~2==exist(idxName); updateFlag=1; end

% is my index older than me?
if exist(idxName) && (dateModified(idxName) < dateModified(name))
    updateFlag=1;
end

if hhh.isdir % directory (else file)
        
    disp([name,filesep']);
    
    % check that 'name' is consistent with name in parent directory
    d=dir(PATHSTR);
    OKflag=0;
    for iFile=1:numel(d)
        if strcmp(d(iFile).name,[NAME,EXT]);
            OKflag=1;
        end
    end
    if ~OKflag; error(['''', NAME,EXT, ''' does not match real file name']); end
    

    % list my files/directories
    d=dir(name);
    iGood=ones(numel(d),1);
    nskip=0;
    for k=1:numel(iGood);       % weed out irrelevant/bad files
        if strcmp(d(k).name,'.') || strcmp(d(k).name,'..')  % me & parent dirs
            iGood(k)=0; 
        elseif d(k).name(1)=='.';                               % files starting with '.'
            iGood(k)=0; nskip = nskip+1; 
            disp(['skip, starts with ''.'': ',name,filesep,d(k).name]);
        end;                              
        if strcmp(d(k).name,'nt_idx');                      % index directory
            iGood(k)=0; nskip=nskip+1; 
        end      
        if any(d(k).name<33) || any(d(k).name==127) ;       % files with bad chars in names
            iGood(k)=0; nskip=nskip+1;
            disp(['skip, bad char in name: ',name,filesep,d(k).name]);
        end 
        if isempty(d(k).date);                              % invalid file (soft link?)
            iGood(k)=0; nskip=nskip+1; 
            disp(['skip, invalid file: ',name,filesep,d(k).name]);
        end 
    end
    d=d(iGood~=0);
    nfiles=numel(d);
    
    % check / index all my files 
    for iFile=1:nfiles
        if d(iFile).name(end)=='/';
            disp(d)
        end
        if 1==nt_index([name,filesep,d(iFile).name],p,forceUpdate);
            updateFlag=1; % one of my files updated, update me too
        end        
    end   
    
    % purge my index directory of orphan files
    dd=dir(idxDir);
    iGood2=ones(numel(dd),1);
    for k=1:numel(iGood2);      % weed out irrelevant stuff
        if dd(k).name(1)=='.'; iGood2(k)=0; end;            % files starting with '.'
        if strcmp(dd(k).name,'nt_idx'); iGood2(k)=0; end    % index directory
    end
    dd=dd(iGood2~=0);
    iGood3=ones(numel(dd),1);
    for iFile=1:numel(iGood3)
        [~,NAME2,EXT2]=fileparts(dd(iFile).name);           % name of index file
        theFile=[PATHSTR,filesep,NAME2];                    % file pointed by index
        if 2~=exist(theFile) ...    % neither file...
                && 7~=exist(theFile) % nor directory...
            disp(['>',dd(iFile).name,'<'])
            delete([idxDir,filesep,dd(iFile).name]);
            disp([theFile, ' not found, ']);
            disp(['deleting orphan file ',[idxDir,filesep,dd(iFile).name]]);
            iGood3(iFile)=0;
        end
    end
    dd=dd(iGood3~=0);
    
    % all my files are now checked/updated
        
    % other checks?
    
    % merge info about me & my files into my index
    if updateFlag
        
        % info about me
        hhh.dir=d;              % my directory excluding bad files
                
        % compile info about my files/directories 
        hhh.myfiles.bytes=zeros(nfiles,1,'uint64');       % bytes (file) or total bytes (directory)
        hhh.myfiles.isdir=nan(nfiles,1);                  % directory?
        hhh.myfiles.nfiles=zeros(nfiles,1,'uint64');      % number of files (including files in subdirectories)
        hhh.nfiles=uint64(1);       % me
        hhh.ndirs=uint64(1);        % me
        hhh.bytes=uint64(0);
        hhh.nskip=nskip;  
        hhh.ndata=0; 
        hhh.nbad=0;
        hhh.ntypes=[];
        hhh.depth=1;
        for iFile=1:nfiles
            load('-mat',[name,filesep,'nt_idx',filesep,d(iFile).name,'.idx']);  % loads hh, ii
            % aggregate info for me & subdirectories
            hhh.bytes=hhh.bytes+hh.bytes;
            hhh.ndirs=hhh.ndirs+hh.ndirs;
            hhh.ndata=hhh.ndata+hh.ndata;
            hhh.nbad=hhh.nbad+hh.nbad;
            hhh.nfiles=hhh.nfiles+hh.nfiles;   
            hhh.bytes=hhh.bytes+hh.bytes;
            hhh.nskip=hhh.nskip+hh.nskip;
            % higher resolution data for my files
            hhh.myfiles.bytes(iFile)=hh.bytes;
            hhh.myfiles.isdir(iFile)=hh.isdir;
            hhh.myfiles.nfiles(iFile)=hh.nfiles;
            % aggregate counts of file types
            types=myfieldnamesr(hh.ntypes);
            for iType=1:numel(types)
                if isfield(hhh.ntypes,types(iType));
                    eval(['hhh.ntypes.',types{iType},'=hhh.ntypes.',types{iType},'+hh.ntypes.',types{iType},';']);
                else
                    eval(['hhh.ntypes.',types{iType},'=hh.ntypes.',types{iType},';']);
                end
            end
            hhh.depth=max(hhh.depth,1+hh.depth);
           % TBD: compile data info (ii)
        end
                
        hhh.date=dateModified(name);
        
    end % if updateflag
    
        
else  % file
    
    disp(name)
    
    if numel(name>=3) && strcmp(name(end-2:end), '.ds'); % intercept CFT data
        [a,b,c] = fileparts(name);
        name=[name,filesep,b,'.meg4'];
    end
        
    if updateFlag
        
        hhh.nfiles=uint64(1); % just me
        d=dir(name);
        hhh.bytes=uint64(d.bytes);
        hhh.date=d.date;
        hhh.sr=[];
        hhh.depth=0;
        % needed for dirs:
        hhh.ndirs=uint64(0); 
        hhh.nbad=0; 
        hhh.ndata=0;
        hhh.nskip=0;
        if d.bytes==0; % empty, don't bother
            hhh.isdata=0;
            hhh.type='empty';
            hhh.ntypes.empty=1;
        else

            % check file type
            hhh.ext=EXT;
            [isdata,type]=filetype(name);
            
            type=strrep(type,':','___'); % biosig uses ':' in type names

            hhh.type=type;
            try
                eval(['hhh.ntypes.',type,'=1;']);
            catch
                disp(['hhh.ntypes.',type,'=1;']);
                disp(name);
                warning('eval failed');
            end
            
            hhh.isdata=isdata;

            hhh.size=[];   
            hhh.originalsize=[]; % before reshape/transpose

            if hhh.isdata
                hhh.ndata=1;
                [a,b,c]=fileparts(type);
                x=[];
                if strcmp(b,'matlab');
                    x=readmatlab(name,c(2:end));
                elseif strcmp(type,'unknown') || strcmp(type,'matlab_non_numeric')
                    error('!');
                else
                    try
                        h=sopen(name);
                        hhh.sr=h.SampleRate;
                    catch ME
                        hhh.failed=1;
                        disp(name);
                        warning('...sopen failed');
                        disp(ME);
                    end
                    try
                        x=sread(h);
                    catch ME
                        hhh.failed=1;
                        disp(name)
                        disp(ME);
                        warning('...sread failed');
                    end
                    sclose(h);
                end
                hhh.originalsize=size(x);
                if ndims(x)>2;
                    sizes=size(x);
                    x=reshape(x,prod(sizes(1:end-1)),sizes(end));
                    disp(['reshape -->', num2str(size(x))]);
                end
                if size(x,1)<size(x,2); 
                    x=x'; 
                    disp(['transpose --> ',num2str(size(x))]);
                end
                hhh.size=size(x);
                nt_whoss;

                if ~isempty(x)
                    % calculate index
                    if ndims(x)>2; 
                        disp(name); 
                        disp(size(x));
                        error('!'); 

                    end
                    iii=index(x,p);
                end % else iii==[]
            end
        end
    end
end   

if updateFlag
    status=1;
    hh=hhh; ii=iii;
    save(idxName, 'hh','ii');
else 
    status=0;
end


function ii=index(x,p)
% index data
if ndims(x)>2; error('!'); end
[ii.nsamples,ii.nchans]=size(x);
ii.scale=p.scale;
ii.p=p;
npairs=floor(ii.nsamples/p.scale);
x_extra=x(npairs*p.scale+1:end,:);
x=x(1:npairs*p.scale,:);
x=reshape(x,[p.scale,npairs,ii.nchans]);
ii.min=squeeze(min(x,[],1));
ii.max=squeeze(max(x,[],1));
if ~isempty(x_extra);
    ii.min=[ii.min;min(x_extra,[],1)];
    ii.max=[ii.max;max(x_extra,[],1)];
end

function date=dateModified(name)
% modification date of file or directory
[PATHSTR,NAME,EXT]=fileparts(name);
if isempty(PATHSTR); error('!'); end
date=[];
if 2==exist(name); % I'm a file, I own my date.
    d=dir(name); % get directly from file
    date=d.datenum;
elseif 7==exist(name); % I'm a directory, my parent own's my date
    d=dir(PATHSTR);
    for iFile=1:numel(d)
        %disp(d(iFile).name)
        if strcmp(d(iFile).name,[NAME,EXT]);
            date=d(iFile).datenum; % get indirectly from parent directory
            break
        end
    end
else
    disp(name)
    error('!');
end
if isempty(date); 
    disp(['>',name,'<']);
    error('!'); 
end
%date=datenum(date);
        

function [isdata,type,readwith]=filetype(name)
% try to guess type and whether it's data
EXTENSIONS_TO_SKIP={'.idx', '.zip','.txt','.pdf','.doc','.docx','pptx','.xls','.html','.rtf',...
    '.jpg', '.tif','.tiff','.js', '.md', '.m', '.py', '.rar', '.wav'};
[PATHSTR,NAME,EXT]=fileparts(name);
isdata=0; type='unknown'; transpose=0; % default
if ~isempty(EXT) && any(strcmp(lower(EXT),EXTENSIONS_TO_SKIP))
    isdata=0; type=lower(EXT); type=type(2:end); % intercept common types
    disp(['skip (extension): ',name])
else
    fid=fopen(name);
    firstbytes=fread(fid,8,'uchar');
    fclose(fid);
    if ~isempty(EXT) && strcmp(EXT,'.mat') || (numel(firstbytes)>=4 && all(firstbytes(1:4)'=='MATL')) % matlab file
        try
            s=whos('-file',name);
        catch ME
            disp('name');
            disp('... whos failed');
            disp(ME)
            type=[]; return
        end
        % find which variables are numeric
        numerics={'double','single','int64','int32','int16','int8'};
        matrix=strcmp(repmat({s.class},numel(numerics),1), ...
            repmat(numerics',1,numel(s))); 
        idx=find(any(matrix));
        if isempty(idx);
            isdata=0; type='matlab_non_numeric';
        else                    % multiple variables
            sizes=zeros(numel(s),1);
            for iVar=1:numel(s);
                sizes(iVar)=prod(s(iVar).size);
            end
            [~,biggest]=max(prod(sizes));
            isdata=1; type=['matlab.',s(biggest).name];
            disp(['mat file, multiple numeric variables, chosing: ''', s(biggest).name, ''', size:',num2str(size(s(biggest).name))]);
        end
%     elseif strcmp(char(firstbytes(2:8))','BIOSEMI')
%         isdata=1; type='biosemi_bdf';
    else    % hand over to biosig
        try
            h=sopen(name);
            type=h.TYPE;
            sclose(h);
        catch ME
            disp(name);
            warning('... sopen failed');
            disp(ME);
            type='unknown'; return
        end                
        if strcmp(type,'unknown'); 
            isdata=0; 
        else 
            isdata=1;
        end
    end
end
    
function x=readmatlab(name,varname)
% read varname from matlab file
load('-mat',name,varname);
eval(['x=',varname, ';']);


function s=myfieldnamesr(x)
if ~isstruct(x); s=[]; return; end
fields=fieldnames(x);
s={};
for iField=1:numel(fields);
    xx=getfield(x,fields{iField});
    if isa(xx,'struct');
        subfields=myfieldnamesr(xx);
        for iSubfield=1:numel(subfields);
            s=[s,[char(fields(iField)),'.',char(subfields(iSubfield))]];
        end
    else
        s=[s,fields{iField}];
    end
end

        
        
        
    
           
        
        
            
   