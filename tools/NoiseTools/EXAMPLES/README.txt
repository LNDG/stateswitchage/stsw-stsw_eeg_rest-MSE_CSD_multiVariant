
Example scripts for NoiseTools. Currently they cover only part of the functionality.
Some functions in NoiseTools include test code that may be useful as examples.

See also the scripts in ./deCheveigne_Arzounian_2017/, example_spm_eeg.m and 
example_spm_meg.m.

